package com.shopnearby;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.shopnearby.preference.App_Preference;

public class SplashScreen extends AppCompatActivity {
    Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (App_Preference.newInstance(this).getUserData() != null)
            mIntent = new Intent(SplashScreen.this, HomeActivity.class);
        else if (App_Preference.newInstance(this).getShowTutorial())
            mIntent = new Intent(SplashScreen.this, TutorialActivity.class);
        else
            mIntent = new Intent(SplashScreen.this, LoginActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(mIntent);
                finish();
            }
        }, 3000);
    }

}
