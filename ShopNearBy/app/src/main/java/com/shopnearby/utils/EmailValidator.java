package com.shopnearby.utils;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ankit on 14/5/16.
 */
public class EmailValidator {

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    public static boolean ValidateEmail(String email)
    {
        Matcher mMatcher;
        mMatcher = EMAIL_ADDRESS_PATTERN.matcher(email);
        Log.d("Ankit", "email mMatcher.matches() result:" + mMatcher.matches());
        return mMatcher.matches();
    }
}
