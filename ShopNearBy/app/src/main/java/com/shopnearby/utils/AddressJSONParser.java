package com.shopnearby.utils;


import com.shopnearby.modal.Address_Modal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ankit on 11/05/16.
 */
public class AddressJSONParser {

    private String Str_Types = "";
    private String Str_Place_name = "";
    private String Str_Address = "";
    private String Str_street = "";
    private String Str_street_number = "";
    private String Str_postcode = "";
    private boolean isStreet = true;

    public Address_Modal parse(String result) {

        Address_Modal mModal = null;
        JSONObject mLocationObj = null;
        JSONArray mTypesArray = null;
        JSONObject jPlacesResult = null;

        try {
            JSONObject jObject = new JSONObject(result);
            if (jObject.optString("status").equalsIgnoreCase("OK")) {
                mModal = new Address_Modal();
                jPlacesResult = jObject.getJSONObject("result");
                mLocationObj = jPlacesResult.optJSONObject("geometry");
                if (mLocationObj == null) {
                    return null;
                }

                JSONObject mLocObj = mLocationObj.getJSONObject("location");
                mModal.setStr_lat(mLocObj.optString("lat"));
                mModal.setStr_lng(mLocObj.optString("lng"));
                mTypesArray = jPlacesResult.optJSONArray("types");

                if (mTypesArray != null && mTypesArray.length() > 0) {
                    for (int i = 0; i < mTypesArray.length(); i++) {
                        Str_Types = mTypesArray.getString(i);
                        if (Str_Types.equalsIgnoreCase("establishment")) {
                            isStreet = false;
                        }
                    }
                }

                if (isStreet) {
                    Str_Address = jPlacesResult.getString("formatted_address");
                    JSONArray mAddressArray = jPlacesResult.getJSONArray("address_components");

                    if (mAddressArray != null && mAddressArray.length() > 0) {

                        JSONObject mObj_first = mAddressArray.getJSONObject(0);
                        JSONArray Arr_Types = mObj_first.getJSONArray("types");
                        if (Arr_Types != null && Arr_Types.length() > 0) {
                            for (int j = 0; j < Arr_Types.length(); j++) {
                                if (Arr_Types.getString(j).equalsIgnoreCase("street_number")) {
                                    mModal.setStr_Msg("0");
                                } else {
                                    mModal.setStr_Msg("1");
                                }

                            }
                        }

                        for (int i = 0; i < mAddressArray.length(); i++) {
                            JSONObject mObj = mAddressArray.getJSONObject(i);
                            JSONArray mARR = mObj.getJSONArray("types");

                            if (mARR != null && mARR.length() > 0) {
                                if (mARR.getString(0).equalsIgnoreCase("street_number")) {
                                    Str_street_number = mAddressArray.getJSONObject(0).optString("long_name");
                                }
                                for (int j = 0; j < mARR.length(); j++) {

                                    if (mARR.getString(j).equalsIgnoreCase("street_number")) {
                                        Str_street_number = mObj.getString("short_name");
                                    }

                                    if (mARR.getString(j).equalsIgnoreCase("route")) {
                                        Str_street = mObj.getString("short_name");
                                    }
                                    if (mARR.getString(j).equalsIgnoreCase("postal_code")) {
                                        Str_postcode = mObj.getString("short_name");
                                    }
                                    if (mARR.getString(j).equalsIgnoreCase("country")) {
                                        mModal.setStr_CountryCode(mObj.getString("short_name"));
                                    }
                                }
                                mModal.setStr_add_text(Str_Address);
                                mModal.setStr_add_number(Str_street_number);
                                mModal.setStr_add_street(Str_street);
                                mModal.setStr_add_postalcode(Str_postcode);
                                mModal.setStr_length("" + i);

                            }

                        }
                    }
                    return mModal;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
