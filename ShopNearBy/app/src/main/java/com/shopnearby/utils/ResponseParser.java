package com.shopnearby.utils;

import android.util.Log;

import com.shopnearby.HomeActivity;
import com.shopnearby.fragment.CategoryFilterFragment;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.ItemDetailsModal;
import com.shopnearby.modal.ItemFrontModel;
import com.shopnearby.modal.ItemModal;
import com.shopnearby.modal.ReviewModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.modal.SpecificationModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.modal.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;

/**
 * Created by ankit on 12/5/16.
 */
public class ResponseParser {
    //private String TAG = "ResponseParser";
    public static boolean getResponseStatus(String ApiResult) {
        try {
            JSONObject mObject = new JSONObject(ApiResult);
            return mObject.optBoolean("success");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getResponseMessage(String ApiResult) {
        try {
            JSONObject mObject = new JSONObject(ApiResult);
            return mObject.optString("msg");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static User getUserData(String ApiResult) {
        User user = new User();
        try {
            JSONObject mObject = new JSONObject(ApiResult);
            JSONObject mDataObject = mObject.optJSONObject("data");
            user.setStrUserId(mDataObject.optString("_id"));
            user.setStrUserEmail(mDataObject.optString("email"));
            user.setStrUserFName(mDataObject.optString("fname"));
            user.setStrUserLName(mDataObject.optString("lname"));
            user.setStrUserState(mDataObject.optString("state"));
            user.setStrUserToken(mDataObject.optString("user_token"));
            user.setStrDoB(mDataObject.optString("dob"));
            user.setStrGender(mDataObject.optString("gender"));
            user.setStrUserPassword(mDataObject.optBoolean("password"));
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getDataStatus(String ApiResult) {
        try {
            JSONObject mObject = new JSONObject(ApiResult);
            return mObject.optBoolean("data");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getTotalResults(String ApiResult) {
        try {
            JSONObject mObject = new JSONObject(ApiResult);
            return mObject.optString("total");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }


    public static ArrayList<ItemModal> getItemsList(String ApiResult) {
        try {
            ArrayList<ItemModal> mItemList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mItemArray = mObject.optJSONArray("data");
            if (mItemArray != null && mItemArray.length() > 0) {
                for (int i = 0; i < mItemArray.length(); i++) {
                    JSONObject mChildObj = mItemArray.optJSONObject(i);
                    ItemModal mModal = new ItemModal();
                    mModal.setStrId(mChildObj.optString("_id"));
                    mModal.setStrImage(mChildObj.optString("image"));
                    mModal.setStrCustomerReviewCount(mChildObj.optString("customerReviewCount"));
                    mModal.setStrMaxPrice(trimPriceToTwoDigits(mChildObj.optDouble("maxPrice")));
                    mModal.setStrMinPrice(trimPriceToTwoDigits(mChildObj.optDouble("minPrice")));
                    mModal.setmStoreCount(mChildObj.optInt("store_count"));
                    mModal.setStrTitle(mChildObj.optString("title"));
                    mModal.setStrReviewAverage(mChildObj.optString("customerReviewAverage"));
                    mModal.setInStoreAvailability(mChildObj.optBoolean("inStoreAvailability"));
                    mItemList.add(mModal);
                }
                return mItemList;
            }else{
                return  mItemList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ItemDetailsModal getItemDetails(String ApiResult, String day) {
        try {
            ArrayList<StoreModal> mStoreList = new ArrayList<>();
            ArrayList<SpecificationModal> mDetailsList = new ArrayList<>();
            ItemDetailsModal mDetailModal = new ItemDetailsModal();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONObject mDataObject = mObject.optJSONObject("data");
            mDetailModal.setStrId(mDataObject.optString("_id"));
            mDetailModal.setStrTitle(mDataObject.optString("title"));
            mDetailModal.setStrImage(mDataObject.optString("image"));
            mDetailModal.setStrReviewCount(mDataObject.optString("customerReviewCount"));
            mDetailModal.setStrReviewAverage(mDataObject.optString("customerReviewAverage"));
            mDetailModal.setReviewAverage(Float.parseFloat(mDataObject.optString("customerReviewAverage")));
            mDetailModal.setStrBrand(mDataObject.optString("brand"));
            mDetailModal.setStrDescription(mDataObject.optString("description"));
            mDetailModal.setStrLongDesc(mDataObject.optString("longDescription"));

            if (!mDataObject.optString("mpn").equalsIgnoreCase("")) {
                SpecificationModal modal1 = new SpecificationModal();
                modal1.setName("Model number");
                modal1.setValue(mDataObject.optString("mpn"));
                mDetailsList.add(modal1);
            }
            JSONArray mDetailsArray = mDataObject.optJSONArray("details");
            if (mDetailsArray != null && mDetailsArray.length() > 0) {
                for (int i = 0; i < mDetailsArray.length(); i++) {
                    SpecificationModal detailModal = new SpecificationModal();
                    JSONObject mDetailObj = mDetailsArray.optJSONObject(i);
                    detailModal.setName(mDetailObj.optString("name"));
                    detailModal.setValue(mDetailObj.optString("value"));
                    mDetailsList.add(detailModal);
                }
            }
            JSONArray mStoreArray = mDataObject.optJSONArray("stores");
            if (mStoreArray != null && mStoreArray.length() > 0) {
                for (int i = 0; i < mStoreArray.length(); i++) {
                    StoreModal mModal = new StoreModal();
                    JSONObject mStoreObj = mStoreArray.optJSONObject(i);
                    JSONObject mLocationObj = mStoreObj.optJSONObject("location");
                    JSONObject mHourObj = mStoreObj.optJSONObject("hours");
                    JSONObject mDayObj = mHourObj.optJSONObject(day);
                    Log.d("ResponseParser", "day "+day);
                    Log.d("ResponseParser", "day JSON "+mDayObj.toString());
                    mModal.setStrId(mStoreObj.optString("_id"));
                    mModal.setRetailerId(mStoreObj.optString("retailer"));
                    mModal.setStoreId(mStoreObj.optString("storeId"));
                    mModal.setStoreName(mStoreObj.optString("storeName"));
                    mModal.setAddress(mStoreObj.optString("address"));
                    mModal.setCity(mStoreObj.optString("city"));
                    mModal.setState(mStoreObj.optString("state"));
                    mModal.setCountry(mStoreObj.optString("country"));
                    mModal.setPostalCode(mStoreObj.optString("postalCode"));
                    mModal.setPhoneNumber(mStoreObj.optString("phone"));
                    mModal.setRetailerName(mStoreObj.optString("retailerName"));
                    mModal.setPrice(trimPriceToTwoDigits(mStoreObj.optDouble("price")));
                    mModal.setInStoreAvailability(mStoreObj.optBoolean("inStoreAvailability"));
                    mModal.setAffilateUrl(mStoreObj.optString("affiliateUrl"));
                    mModal.setStoreLat(mLocationObj.optDouble("lat"));
                    mModal.setStoreLng(mLocationObj.optDouble("lon"));

                    mModal.setOpenTime(changeTimeFormat(mDayObj.optString("open")));
                    mModal.setCloseTime(changeTimeFormat(mDayObj.optString("close")));
                    mModal.setUpdateDate(mStoreObj.optString("itemUpdateDate"));
                    mStoreList.add(mModal);

                    if(i==2)
                        mModal.setPrice("23.44");
                }
            }

            SpecificationModal modal2 = new SpecificationModal();
            modal2.setName("Sku");
            modal2.setValue(mDataObject.optString("_id"));
            mDetailsList.add(modal2);

            mDetailModal.setmStoreList(mStoreList);
            mDetailModal.setmDetailList(mDetailsList);
            return mDetailModal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ReviewModal> getReviewsList(String ApiResult) {
        try {
            ArrayList<ReviewModal> mReviewsList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mReviewArray = mObject.optJSONArray("data");
            if (mReviewArray != null && mReviewArray.length() > 0) {
                for (int i = 0; i < mReviewArray.length(); i++) {
                    JSONObject mChildObj = mReviewArray.optJSONObject(i);
                    ReviewModal mModal = new ReviewModal();
                    mModal.setStrId(mChildObj.optString("_id"));
                    mModal.setStrName(mChildObj.optString("reviewer"));
                    mModal.setStrRating(mChildObj.optString("rating"));
                    mModal.setStrReviewTitle(mChildObj.optString("title"));
                    mModal.setStrReviewComment(mChildObj.optString("comment"));
                    mModal.setStrReviewDate(ChangeDateFormat(mChildObj.optString("time"), "MM/dd/yyyy"));
                    mReviewsList.add(mModal);
                }
                return mReviewsList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String ChangeDateFormat(String apiDate, String format) {
        String ConvertedDate = "";
        try {
            apiDate = apiDate.replace("T", " ");
            SimpleDateFormat sourceFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date parsed = sourceFormat.parse(apiDate);
            sourceFormat.setTimeZone(TimeZone.getDefault());
//            ConvertedDate = sourceFormat.format(parsed);
            SimpleDateFormat destFormat = new SimpleDateFormat(format);
            ConvertedDate = destFormat.format(parsed);

        } catch (Exception e) {
            e.printStackTrace();
            return apiDate;
        }
        return ConvertedDate;
    }

    public static String changeTimeFormat(String time) {
        String ConvertedTime = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(time);
            ConvertedTime = new SimpleDateFormat("hh:mm aa").format(dateObj);
        } catch (final Exception e) {
            e.printStackTrace();
            return time;
        }
//        Log.d("Ankit", "ConvertedTime: " + ConvertedTime);
        return ConvertedTime;
    }

    public static List<BrandModal> getBrandNameList(String ApiResult) {
        try {
            List<BrandModal> mBrandList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mBrandArray = mObject.optJSONArray("data");
            if (mBrandArray != null && mBrandArray.length() > 0) {
                for (int i = 0; i < mBrandArray.length(); i++) {
                    BrandModal modal = new BrandModal();
                    JSONObject mChildObj = mBrandArray.optJSONObject(i);
                    modal.setBrandName(mChildObj.optString("name"));
                    modal.setSelected(false);
                    mBrandList.add(modal);
                    BRANDNAME_LIST.add(modal);
                }
                return mBrandList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<SearchCategoryModal> getCategoryList(String ApiResult) {
        try {
            List<SearchCategoryModal> mCategoryList = new ArrayList<>();
            List<SearchCategoryModal> mMainList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mCategoryArray = mObject.optJSONArray("data");
            if (mCategoryArray != null && mCategoryArray.length() > 0) {
                for (int i = 0; i < mCategoryArray.length(); i++) {
                    SearchCategoryModal modal = new SearchCategoryModal();
                    JSONObject mChildObj = mCategoryArray.optJSONObject(i);
                    modal.setName(mChildObj.optString("name"));
                    modal.setId(mChildObj.optString("_id"));
                    modal.setParent(mChildObj.optString("parent"));
                    modal.setSelected(false);

                    mMainList.add(modal);
                }
                Log.d("ResponseParser", "Size of Main  List "+mMainList.size());
                for (int j=0;j<mMainList.size();j++){
                    if(mMainList.get(j).getParent().equalsIgnoreCase("0")){
                        SearchCategoryModal modal = new SearchCategoryModal();
                        modal.setName(mMainList.get(j).getName());
                        modal.setId(mMainList.get(j).getId());
                        modal.setParent(mMainList.get(j).getParent());
                        modal.setSelected(false);
                        mCategoryList.add(modal);
                    }
                }
                Log.d("ResponseParser", "Size of Parent  List "+mCategoryList.size());


                for (int j=0;j<mMainList.size();j++){
                    Log.d("ResponseParser", "Main llist parent "+mMainList.get(j).getParent());
                    if(!(mMainList.get(j).getParent().equalsIgnoreCase("0"))){
                        Log.d("ResponseParser", "MainList not Parent "+mMainList.get(j).getParent());
                        boolean matched=false;
                        int matchPosition=-1;
                        SearchCategoryModal parentModel=new SearchCategoryModal();
                        for(int k=0;k<mCategoryList.size();k++){
                            Log.d("ResponseParser", "Parent List Id  "+mCategoryList.get(k).getId());

                            if(mMainList.get(j).getParent().equalsIgnoreCase(mCategoryList.get(k).getId())){
                                Log.d("ResponseParser", "Parent List Id matched with Main "+mCategoryList.get(k).getId()+" Main "+mMainList.get(j).getParent());
                                ArrayList<SearchCategoryModal> ChildArray=new ArrayList<SearchCategoryModal>();
                                SearchCategoryModal childModel = new SearchCategoryModal();

                                childModel.setName(mMainList.get(j).getName());
                                childModel.setId(mMainList.get(j).getId());
                                childModel.setParent(mMainList.get(j).getParent());
                                childModel.setSelected(false);
                                matched=true;
                                ChildArray=mCategoryList.get(k).getChildArray();
                                Log.d("ResponseParser", "Parent ChildArray size "+ChildArray.size());
                                ChildArray.add(childModel);
                                matchPosition=k;
                                parentModel=mCategoryList.get(k);
                                parentModel.setChildArray(ChildArray);
                                break;
                                //mCategoryList.add(modal);
                            }


                        }

                        if(matched==true && matchPosition!=-1)
                            mCategoryList.set(matchPosition,parentModel);

                    }
                }
                return mCategoryList;
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("ResponseParser", "Exception in pareser");
        }
        return null;
    }

    public static List<String> getSearchedTermList(String ApiResult) {
        try {
            List<String> mTermsList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONObject mDataObject = mObject.optJSONObject("data");
            JSONArray mTermsArray = mDataObject.optJSONArray("searchTerms");
            if (mTermsArray != null && mTermsArray.length() > 0) {
                for (int i = 0; i < mTermsArray.length(); i++) {
                    mTermsList.add(mTermsArray.getString(i));
                }
                return mTermsList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<SearchCategoryModal> getSearchCategoryList(String ApiResult) {
        try {
            List<SearchCategoryModal> mCatList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONObject mDataObject = mObject.optJSONObject("data");
            JSONArray categoryArray = mDataObject.optJSONArray("categories");
            if (categoryArray != null && categoryArray.length() > 0) {
                for (int i = 0; i < categoryArray.length(); i++) {
                    SearchCategoryModal modal = new SearchCategoryModal();
                    JSONObject mChildObj = categoryArray.optJSONObject(i);
                    modal.setId(mChildObj.optString("id"));
                    modal.setName(mChildObj.optString("name"));
                    modal.setText(mChildObj.optString("text"));
                    mCatList.add(modal);
                }
                return mCatList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String trimPriceToTwoDigits(double price) {
        String strPrice = "" + price;
        try {
            strPrice = String.format("%.2f", price);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strPrice;
    }

    public static ArrayList<ItemFrontModel> getFrontItemsList(String ApiResult) {
        try {
            ArrayList<ItemFrontModel> mItemList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mItemArray = mObject.optJSONArray("data");
            if (mItemArray != null && mItemArray.length() > 0) {
                for (int i = 0; i < mItemArray.length(); i++) {
                    JSONObject mChildObj = mItemArray.optJSONObject(i);
                    ItemFrontModel mModal = new ItemFrontModel();
                    if(mChildObj.has("category"))
                    mModal.setStrCategory(mChildObj.getString("category"));
                    if(mChildObj.has("image"))
                    mModal.setStrimage(mChildObj.getString("image"));
                    if(mChildObj.has("name"))
                    mModal.setStrName(mChildObj.getString("name"));
                    if(mChildObj.has("order"))
                    mModal.setStrOrder(mChildObj.getString("order"));
                    if(mChildObj.has("pageType"))
                    mModal.setStrPageType(mChildObj.getString("pageType"));
                    mItemList.add(mModal);
                }
                return mItemList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<StoreModal> getFilterstoreList(String ApiResult) {
        try {
            ArrayList<StoreModal> mItemList = new ArrayList<>();
            JSONObject mObject = new JSONObject(ApiResult);
            JSONArray mItemArray = mObject.optJSONArray("data");
            if (mItemArray != null && mItemArray.length() > 0) {
                for (int i = 0; i < mItemArray.length(); i++) {
                    JSONObject mChildObj = mItemArray.optJSONObject(i);
                    StoreModal mModal = new StoreModal();
                    if(mChildObj.has("_id"))
                        mModal.setStoreId(mChildObj.getString("_id"));
                    if(mChildObj.has("name"))
                        mModal.setStoreName(mChildObj.getString("name"));

                    mModal.setSelected(false);
                    mItemList.add(mModal);
                }
                return mItemList;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
