package com.shopnearby.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.shopnearby.R;


public class Progress_HUD extends Dialog {

    public static Progress_HUD dialog;
    private static TextView txt_Message;

    public Progress_HUD(Context context) {
        super(context);
    }

    public Progress_HUD(Context context, int theme) {
        super(context, theme);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
//		ImageView imageView = (ImageView) findViewById(R.id.spinnerImageView);
//		AnimationDrawable spinner = (AnimationDrawable) imageView
//				.getBackground();
//		spinner.start();
    }

    public static Progress_HUD show(Activity mActivity,String message
    /* ,OnCancelListener cancelListener */) {

        if (dialog != null) {
            if (dialog.isShowing()) {
                hideProgressDialog(mActivity);
            }
        }

        dialog = new Progress_HUD(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.progress_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        txt_Message = (TextView) dialog.findViewById(R.id.txt_progress);
        txt_Message.setText(message);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        dialog.getWindow().setAttributes(lp);

        dialog.show();
        return dialog;
    }

    public static void hideProgressDialog(Activity mActivity) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        } else {
            dialog = new Progress_HUD(mActivity);
            dialog.dismiss();
        }
    }
}