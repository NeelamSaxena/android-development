package com.shopnearby.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.shopnearby.R;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.SearchCategoryModal;

import java.util.List;

import static com.shopnearby.HomeActivity.CATEGORYNAME_LIST;
import static com.shopnearby.constant.App_Constant.currentSortFilter;

/**
 * Created by shailendra on 10/6/16.
 */

public class CategoryAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<SearchCategoryModal> categoryList;


    public CategoryAdapter(Context context, List<SearchCategoryModal> list) {
        this.context = context;
        this.categoryList = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }
    //
    public List<SearchCategoryModal> getSelectedCategory()
    {
        return categoryList;
    }

    @Override
    public SearchCategoryModal getItem(int i) {
        return categoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_category, null);
            holder.brandCheckBox = (CheckBox) convertView.findViewById(R.id.cb_brandName);
            holder.MainRow = (LinearLayout) convertView.findViewById(R.id.mainlayout);
            holder.childRow = (LinearLayout) convertView.findViewById(R.id.childRow);
            holder.childRow.setVisibility(View.GONE);

            holder.MainRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(categoryList.get(i).getChildArray().size()>=0){

                        if(holder.childRow.getVisibility()==View.VISIBLE)
                        holder.childRow.setVisibility(View.GONE);
                        else
                            holder.childRow.setVisibility(View.VISIBLE);
                    }else{
                        holder.childRow.setVisibility(View.GONE);
                    }
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.brandCheckBox.setText(getItem(i).getName());

        holder.brandCheckBox.setChecked(getItem(i).isSelected());

        holder.brandCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.brandCheckBox.isChecked()) {
                    SetAllUnchecked();

                    categoryList.get(i).setSelected(true);


                } else {
                    categoryList.get(i).setSelected(false);
                }
            }
        });
        if(categoryList.get(i).getChildArray().size()>=0){
            holder.childRow.removeAllViews();
          // holder.childRow.setVisibility(View.VISIBLE);
            for (int j=0;j<categoryList.get(i).getChildArray().size();j++){
                LinearLayout view_Dynamic = (LinearLayout) inflater.inflate(
                        R.layout.child_row_category, null);
                final CheckBox checkBox=(CheckBox)view_Dynamic.findViewById(R.id.cb_child);
                final int posChild=j;
                checkBox.setText(categoryList.get(i).getChildArray().get(j).getName());
                checkBox.setChecked(categoryList.get(i).getChildArray().get(posChild).isSelected());
                holder.childRow.addView(view_Dynamic);

                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkBox.isChecked()) {

                            SetAllUnchecked();

                            categoryList.get(i).getChildArray().get(posChild).setSelected(true);
                            Log.d("Neelam", "Child Array: " +  categoryList.get(i).getChildArray());
                        } else {
                            categoryList.get(i).getChildArray().get(posChild).setSelected(false);
                        }
                    }
                });

            }


        }else{
            holder.childRow.setVisibility(View.GONE);
        }


        return convertView;
    }

    public void SetAllUnchecked(){
        for (int i = 0; i < categoryList.size(); i++) {
            categoryList.get(i).setSelected(false);
            for (int k=0;k<categoryList.get(i).getChildArray().size();k++){
                categoryList.get(i).getChildArray().get(k).setSelected(false);
            }
        }
        this.notifyDataSetChanged();
    }

    public class ViewHolder {
        CheckBox brandCheckBox;
        LinearLayout childRow;
        LinearLayout MainRow;
    }
}
