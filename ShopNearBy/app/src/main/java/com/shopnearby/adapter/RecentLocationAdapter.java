package com.shopnearby.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.shopnearby.R;
import com.shopnearby.modal.Address_Modal;

import java.util.List;

/**
 * Created by neelam on 14/6/16.
 */

public class RecentLocationAdapter extends RecyclerView.Adapter<RecentLocationAdapter.MyViewHolder> {
    List<Address_Modal> mSearchList;
    Context mcontext;
    @Override
    public RecentLocationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_row_category, parent, false);
        return new RecentLocationAdapter.MyViewHolder(itemView);
    }

    public RecentLocationAdapter(List<Address_Modal> mList) {
        this.mSearchList = mList;
    }


    @Override
    public void onBindViewHolder(RecentLocationAdapter.MyViewHolder holder, int position) {
        holder.txt_SearchText.setChecked(mSearchList.get(position).isSelected());
        holder.txt_SearchText.setText(mSearchList.get(position).getStr_Street_text());
    }

    @Override
    public int getItemCount() {
        return mSearchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox txt_SearchText;

        public MyViewHolder(View view) {
            super(view);
            txt_SearchText = (CheckBox) view.findViewById(R.id.cb_child);
        }
    }
}
