package com.shopnearby.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.shopnearby.R;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.StoreModal;

import java.util.List;

/**
 * Created by neelam on 20/6/16.
 */

public class StoreFilter_Adapter extends BaseAdapter {


    private Context context;
    private LayoutInflater inflater;
    private List<StoreModal> Original_Data;

    public StoreFilter_Adapter(Context context,List<StoreModal> data) {
        this.context = context;
        this.Original_Data = data;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return Original_Data.size();
    }

    @Override
    public StoreModal getItem(int position) {
        return Original_Data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final StoreFilter_Adapter.ViewHolder holder;
        if (convertView == null) {
            holder = new StoreFilter_Adapter.ViewHolder();
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_brand, null);
            holder.brandCheckBox = (CheckBox) convertView.findViewById(R.id.cb_brandName);
            convertView.setTag(holder);
        } else {
            holder = (StoreFilter_Adapter.ViewHolder) convertView.getTag();
        }

        holder.brandCheckBox.setText(getItem(position).getStoreName());
        holder.brandCheckBox.setChecked(getItem(position).isSelected());

        holder.brandCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.brandCheckBox.isChecked()) {
                    Original_Data.get(position).setSelected(true);
                } else {
                    Original_Data.get(position).setSelected(false);
                }
            }
        });
        return convertView;
    }

    public class ViewHolder {
        CheckBox brandCheckBox;
    }

    public List<StoreModal> getSelectedSrore(){
        return Original_Data;
    }
}
