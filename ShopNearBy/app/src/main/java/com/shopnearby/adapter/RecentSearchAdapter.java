package com.shopnearby.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shopnearby.R;

import java.util.List;

/**
 * Created by ankit on 3/6/16.
 */
public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.MyViewHolder> {
    List<String> mSearchList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent_search, parent, false);
        return new MyViewHolder(itemView);
    }

    public RecentSearchAdapter(List<String> mList) {
        this.mSearchList = mList;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txt_SearchText.setText(mSearchList.get(position));
    }

    @Override
    public int getItemCount() {
        return mSearchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_SearchText;

        public MyViewHolder(View view) {
            super(view);
            txt_SearchText = (TextView) view.findViewById(R.id.tv_searchText);
        }
    }
}
