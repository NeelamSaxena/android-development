package com.shopnearby.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.shopnearby.R;
import com.shopnearby.modal.SearchCategoryModal;

import java.util.HashMap;
import java.util.List;

/**
 * Created by neelam on 16/6/16.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private String TAG = "ExpandableListAdapter";
    private Context _context;
    // child data in format of header title, child title
    private List<SearchCategoryModal> _listDataHeader;
    private HashMap<String, List<SearchCategoryModal>> _listDataChild;

    public ExpandableListAdapter(Context context,  List<SearchCategoryModal>listDataHeader,
                                 HashMap<String, List<SearchCategoryModal>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public SearchCategoryModal getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    // Inflate child view
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition).getName().toString();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_row_category, null);
        }
        Log.d(TAG, "childText"+childText);
        CheckBox childCheckBox = (CheckBox) convertView.findViewById(R.id.cb_child);

        childCheckBox.setText(childText);
        return convertView;
    }
    // return number of headers in list
    @Override
    public int getChildrenCount(int groupPosition) {
        if(this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size()>0){
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }
        else
        return 0;
    }

    @Override
    public SearchCategoryModal getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition).getName().toString();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_brand, null);
        }

        CheckBox brandCheckBox = (CheckBox) convertView.findViewById(R.id.cb_brandName);
        Log.d(TAG, "headerTitle"+headerTitle);
        brandCheckBox.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
