package com.shopnearby.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.shopnearby.R;
import com.shopnearby.modal.ItemModal;

import java.util.ArrayList;

/**
 * Created by ankit on 19/5/16.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.BindingHolder> {
    ArrayList<ItemModal> mItemList;

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public ItemsAdapter(ArrayList<ItemModal> mList) {
        this.mItemList = mList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_products, parent, false);
        return new BindingHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final ItemModal item = mItemList.get(position);

        holder.getBinding().setVariable(BR.itemVM, item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }


}

