package com.shopnearby.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.databinding.library.baseAdapters.BR;
import com.shopnearby.R;
import com.shopnearby.modal.ReviewModal;

import java.util.ArrayList;


/**
 * Created by ankit on 2/6/16.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.BindingHolder> {
    ArrayList<ReviewModal> mReviewsList;

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public ReviewsAdapter(ArrayList<ReviewModal> mList) {
        this.mReviewsList = mList;
    }

    @Override
    public ReviewsAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reviews, parent, false);
        return new ReviewsAdapter.BindingHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.BindingHolder holder, int position) {
        final ReviewModal item = mReviewsList.get(position);
        holder.getBinding().setVariable(BR.reviewVM, item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mReviewsList.size();
    }


}