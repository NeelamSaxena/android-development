package com.shopnearby.adapter;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.shopnearby.R;

public class CustomBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {

        Glide.with(imageView.getContext()).load(url)
                .error(R.drawable.placeholder_a) // will be displayed if the image cannot be loaded
                .crossFade(2000).into(imageView);
    }

}
