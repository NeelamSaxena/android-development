package com.shopnearby.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shopnearby.R;
import com.shopnearby.modal.StoreModal;

import java.util.ArrayList;

/**
 * Created by ankit on 17/5/16.
 */
public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {
    ArrayList<StoreModal> mStoreList;
    Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.closest_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    public StoreAdapter(Context context, ArrayList<StoreModal> mList) {
        this.mStoreList = mList;
        this.context = context;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        StoreModal modal = mStoreList.get(position);
        holder.txtTitle.setText((position + 1) + ". " + modal.getRetailerName() + " " + modal.getStoreName());
        holder.txtPrice.setText("$" + modal.getPrice() + " online price");
        if (modal.isInStoreAvailability()) {
            holder.txt_Availability.setText("Buy & pickup in store");
        } else {
            holder.txt_Availability.setText("In-store only");
        }
        holder.txtTiming.setText("Open, " + modal.getOpenTime() + " - " + modal.getCloseTime());

    }

    @Override
    public int getItemCount() {
        return mStoreList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle, txtPrice, txt_Availability, txtTiming, txtDistance, txtUpdateTime;

        public MyViewHolder(View view) {
            super(view);
            txtTitle = (TextView) view.findViewById(R.id.tv_title);
            txtPrice = (TextView) view.findViewById(R.id.tv_price);
            txt_Availability = (TextView) view.findViewById(R.id.tv_availability);
            txtTiming = (TextView) view.findViewById(R.id.tv_timing);
            txtDistance = (TextView) view.findViewById(R.id.tv_distance);
            txtUpdateTime = (TextView) view.findViewById(R.id.tv_UpdateTime);

        }
    }
}

