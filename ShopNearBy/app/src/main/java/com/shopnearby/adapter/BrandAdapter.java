package com.shopnearby.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;

import com.shopnearby.R;
import com.shopnearby.modal.BrandModal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit on 8/6/16.
 */

public class BrandAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private LayoutInflater inflater;
    //private List<BrandModal> brandList;
    private List<BrandModal> Original_Data;
    private List<BrandModal> Filterable_Data;
    private ItemFilter mFilter = new ItemFilter();
    public BrandAdapter(Context context,List<BrandModal> data) {
        this.context = context;
        this.Filterable_Data = data;
        this.Original_Data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
//        if(Filterable_Data!=null)
//        return Filterable_Data.size();
//        else return 0;
        return Filterable_Data.size();
    }
//
    public List<BrandModal> getSelectedBrands()
    {
        return Original_Data;
    }

    @Override
    public BrandModal getItem(int i) {

        return Filterable_Data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_brand, null);
            holder.brandCheckBox = (CheckBox) convertView.findViewById(R.id.cb_brandName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.brandCheckBox.setText(getItem(i).getBrandName());
        holder.brandCheckBox.setChecked(getItem(i).isSelected());

        holder.brandCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.brandCheckBox.isChecked()) {
                    Filterable_Data.get(i).setSelected(true);
                } else {
                    Filterable_Data.get(i).setSelected(false);
                }
            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder {
        CheckBox brandCheckBox;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<BrandModal> list = Original_Data;

            int count = list.size();
            final ArrayList<BrandModal> nlist = new ArrayList<BrandModal>(
                    count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getBrandName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
          //  Filterable_Data = (List<BrandModal>) results.values;
            Filterable_Data = (ArrayList<BrandModal>) results.values;
            notifyDataSetChanged();
        }

    }
}
