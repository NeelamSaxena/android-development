package com.shopnearby.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.shopnearby.R;
import com.shopnearby.modal.SpecificationModal;

import java.util.ArrayList;

/**
 * Created by ankit on 3/6/16.
 */
public class DetailSpecAdapter extends RecyclerView.Adapter<DetailSpecAdapter.BindingHolder> {
    ArrayList<SpecificationModal> mDetailsList;

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public DetailSpecAdapter(ArrayList<SpecificationModal> mList) {
        this.mDetailsList = mList;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_specifications, parent, false);
        return new BindingHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final SpecificationModal item = mDetailsList.get(position);
        holder.getBinding().setVariable(BR.specificationVM, item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mDetailsList.size();
    }


}
