package com.shopnearby.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.shopnearby.R;
import com.shopnearby.modal.ItemFrontModel;
import com.shopnearby.modal.ItemModal;

import java.util.ArrayList;

/**
 * Created by neelam on 17/6/16.
 */

public class ItemFront_Adapter extends RecyclerView.Adapter<ItemFront_Adapter.BindingHolder> {

    ArrayList<ItemFrontModel> mItemList;

    public ItemFront_Adapter(ArrayList<ItemFrontModel> mList) {
        this.mItemList = mList;
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }


    @Override
    public ItemFront_Adapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_front_row, parent, false);
        return new ItemFront_Adapter.BindingHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemFront_Adapter.BindingHolder holder, int position) {
        final ItemFrontModel item = mItemList.get(position);

        holder.getBinding().setVariable(BR.itemFrontVM, item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
