package com.shopnearby;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Array_Constant;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.fragment.BrandFilterFragment;
import com.shopnearby.fragment.CategoryFilterFragment;
import com.shopnearby.fragment.DetailSpecificationsFragment;
import com.shopnearby.fragment.DistanceFilterFragment;
import com.shopnearby.fragment.FilterFragment;
import com.shopnearby.fragment.ItemDetailFragment;
import com.shopnearby.fragment.ItemFragment;
import com.shopnearby.fragment.Item_FrontFragment;
import com.shopnearby.fragment.LocationFilterFragment;
import com.shopnearby.fragment.MapView_Fragment;
import com.shopnearby.fragment.PriceFragment;
import com.shopnearby.fragment.ReviewFragment;
import com.shopnearby.fragment.SearchFragment;
import com.shopnearby.fragment.SortFilterFragment;
import com.shopnearby.fragment.Store_Filter;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.modal.User;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.Progress_HUD;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.constant.App_Constant.FilterRadius;
import static com.shopnearby.constant.App_Constant.FilterSort;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentSearchCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentSortFilter;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnChangeFragment, LocationListener {
    private String TAG = "HomeActivity";
    private LocationManager locationManager;
    public Location mCurrentLocation;
    /* GPS Constant Permission */
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;
    /* Position */
    private static final int MINIMUM_TIME = 10000;  // 10s
    private static final int MINIMUM_DISTANCE = 50; // 50m

    /* GPS */
    private String mProviderName;
    private static final long MIN_TIME_BW_UPDATES = 150000; // 1 minute

    public static int sortPosition = 0;
    public static int sortPositionSearch = 1;
    public static int distancePosition = 1;



    private int width = 0;
    //    private Toolbar mToolbar;
//    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Drawable arrowDrawable;
    private Menu myMenu;
    private TextView userNameTextView;
    private TextView userEmailTextView;
    public static List<BrandModal> BRANDNAME_LIST;
    public static List<SearchCategoryModal> CATEGORYNAME_LIST;
    public static List<StoreModal> STORE_LIST;

    public static List<BrandModal> BRANDNAME_LISTSearch;
    public static List<SearchCategoryModal> CATEGORYNAME_LISTSearch;
    public static List<StoreModal> STORE_LISTSearch;


    public static int FromSearch = 1;
    public static int mPageCount = 1;
    public static int mPageSize = 40;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @InjectView(R.id.ll_filter)
    public LinearLayout LL_FilterBar;

    @InjectView(R.id.tv_appliedFilters)
    TextView appliedFiltersTextview;
    private SNB_Application mApplication;

    @OnClick(R.id.ll_filter)
    public void openFilterView() {
        mApplication.TempAddress = "";
        mApplication.TempLat = "";
        mApplication.TempLong = "";
        mApplication.TempsortPosition = -1;
        mApplication.TempdistancePosition = -1;
        mApplication.TempMinPrice = "";
        mApplication.TempMaxPrice = "";
        App_Array_Constant.Temp_BRANDNAME_LIST.clear();
        App_Array_Constant.Temp_Category_LIST.clear();
        App_Array_Constant.Temp_STORE_LIST.clear();
        Log.d("Ankit", "AppConstant Value of Distance " + App_Constant.currentRadius);
        Log.d("Ankit", "AppConstant Value of Distance Pos" + distancePosition);
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(mFragment instanceof ItemFragment)
        {
            FromSearch=1;
        }else  if(mFragment instanceof SearchFragment){
            FromSearch=2;
        }


        onReplaceFragment(FilterFragment.newInstance(), "FILTER");
        // onReplaceFragment(SearchFragment.newInstance(), "SearchFragment");
        //  mDrawerLayout.openDrawer(GravityCompat.END);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mint.initAndStartSession(HomeActivity.this, "d87b18fa");
        setContentView(R.layout.activity_home);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        updateLocation();
        mApplication = SNB_Application.getApplicationInstance(this);


        BRANDNAME_LISTSearch=new ArrayList<>();
        CATEGORYNAME_LISTSearch=new ArrayList<>();
        STORE_LISTSearch=new ArrayList<>();


        InitializeAllFilter();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        setUpNavDrawer();
        //   onReplaceFilterFragment(FilterFragment.newInstance(), "FILTER");
        // replaceFragment(ItemFragment.newInstance());
        replaceFragment(Item_FrontFragment.newInstance());
        userNameTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_user_name);
        userEmailTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_user_email);
        if (App_Preference.newInstance(this).getUserData() != null) {
            User userModal = App_Preference.newInstance(this).getUserData();
            userNameTextView.setText(userModal.getStrUserFName() + " " + userModal.getStrUserLName());
            userEmailTextView.setText(userModal.getStrUserEmail());
        }


        // App_Preference.newInstance(this).setLocation(String.valueOf(34.0500), String.valueOf(-118.2500), "Log Angles, USA");
        //App_Constant.currentSortFilter=String.
        Log.d(TAG, "App_Preference.newInstance(this).getCurrentSort() " + App_Preference.newInstance(this).getCurrentSort());

        if(!App_Preference.newInstance(this).getCurrentSort().equalsIgnoreCase(""))
        {
            App_Constant.currentSortFilter=App_Preference.newInstance(this).getCurrentSort();
            Log.d(TAG, "App_Constant.currentSortFilter" + App_Constant.currentSortFilter);

        }else{
            App_Constant.currentSortFilter = "&ratingSort=desc";
        }

        if(!App_Preference.newInstance(this).getFilter().equalsIgnoreCase(""))
        {
            App_Constant.FilterSort=App_Preference.newInstance(this).getFilter();

        }else{
                App_Constant.FilterSort="Best Rated";
        }

        if(App_Preference.newInstance(this).getSortPos()!=-1)
        {
            sortPosition=App_Preference.newInstance(this).getSortPos();

        }else{
            sortPosition=0;
        }

        if(!App_Preference.newInstance(this).getcurrentRadius().equalsIgnoreCase(""))
        {
            App_Constant.currentRadius=App_Preference.newInstance(this).getcurrentRadius();

        }else{
            App_Constant.currentRadius = "20mi";
        }

        if(!App_Preference.newInstance(this).getFilterRadius().equalsIgnoreCase(""))
        {
            App_Constant.FilterRadius=App_Preference.newInstance(this).getFilterRadius();

        }else{
            App_Constant.FilterRadius = "20 miles or less";
        }

        if(App_Preference.newInstance(this).getDistancePos()!=-1)
        {
            distancePosition=App_Preference.newInstance(this).getDistancePos();

        }else{
            distancePosition = 1;
        }


        App_Constant.sCurrentLat = Double.parseDouble(App_Preference.newInstance(this).getLatitude());
        App_Constant.sCurrentLng = Double.parseDouble(App_Preference.newInstance(this).getLongitude());
        App_Constant.sCurrentAddress = App_Preference.newInstance(this).getAddress();

        updateFilterText(false);
    }

    private void setUpNavDrawer() {
        arrowDrawable = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);

        if (mToolbar != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    mToolbar, R.string.Tag_Ok, R.string.Tag_Cancel) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    getSupportFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();

            final View.OnClickListener originalToolbarListener = mDrawerToggle.getToolbarNavigationClickListener();

            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    Log.d("Ankit", "Entry Count: " + getSupportFragmentManager().getBackStackEntryCount());
                    final Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

                    if (mFragment instanceof SearchFragment
                            || mFragment instanceof SortFilterFragment
                            || mFragment instanceof LocationFilterFragment
                            || mFragment instanceof DistanceFilterFragment
                            || mFragment instanceof BrandFilterFragment
                            || mFragment instanceof CategoryFilterFragment
                            || mFragment instanceof PriceFragment
                            || mFragment instanceof Store_Filter
                            || mFragment instanceof FilterFragment
                          ) {
                        getSupportActionBar().hide();
                    } else {
                        getSupportActionBar().show();
                    }
                    if (mFragment instanceof ItemFragment) {
                        LL_FilterBar.setVisibility(View.VISIBLE);
                    } else {
                        LL_FilterBar.setVisibility(View.GONE);
                    }

                    if (mFragment instanceof Item_FrontFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        mDrawerToggle.setDrawerIndicatorEnabled(true);
                        mToolbar.setTitle("Items");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);
                            myMenu.findItem(R.id.action_search).setVisible(true);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                        }

                        mDrawerToggle.setToolbarNavigationClickListener(originalToolbarListener);

                    } else if (mFragment instanceof ItemFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setTitle(App_Constant.currentItemHeader);
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                            myMenu.findItem(R.id.action_search).setVisible(true);
                        }

                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });

                    } else if (mFragment instanceof MapView_Fragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(arrowDrawable);
                        mToolbar.setTitle("Stores");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);

                        }
                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });

                    } else if (mFragment instanceof ItemDetailFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(arrowDrawable);
                        mToolbar.setTitle("Item Details");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(true);
                            myMenu.findItem(R.id.action_search).setVisible(true);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);
                        }
                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });
                    } else if (mFragment instanceof ReviewFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(arrowDrawable);
                        mToolbar.setTitle("Reviews");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_search).setVisible(false);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);
                        }
                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });
                    } else if (mFragment instanceof DetailSpecificationsFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(arrowDrawable);
                        mToolbar.setTitle("Item Specifications");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_search).setVisible(false);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(false);
                        }
                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });
                    } else if (mFragment instanceof FilterFragment) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mDrawerToggle.setDrawerIndicatorEnabled(false);
                        mToolbar.setNavigationIcon(arrowDrawable);
                        mToolbar.setTitle("Filters");
                        if (myMenu != null) {
                            myMenu.findItem(R.id.action_share).setVisible(false);
                            myMenu.findItem(R.id.action_favourite).setVisible(false);
                            myMenu.findItem(R.id.action_search).setVisible(false);
                            myMenu.findItem(R.id.action_cart).setVisible(false);
                            myMenu.findItem(R.id.action_apply).setVisible(true);
                        }
                        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getSupportFragmentManager().popBackStack();
                            }
                        });
                    }
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        //  Fragment mFilterFragment = getSupportFragmentManager().findFragmentById(R.id.right_drawer);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
//        else if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
//            if (mFilterFragment instanceof FilterFragment) {
//                mDrawerLayout.closeDrawer(GravityCompat.END);
//            } else {
//                getSupportFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            }
//        }
        else if (mFragment instanceof Item_FrontFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        myMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if (id == R.id.action_cart) {

            return true;
        } else if (id == R.id.action_search) {
          //  updateFilterTextSearch();
            Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if(mFragment instanceof ItemFragment){
               currentSearchCateogryIDFilter=currentCateogryIDFilter;
            } else if(mFragment instanceof Item_FrontFragment){
                currentSearchCateogryIDFilter="";
            }

            onReplaceFragment(SearchFragment.newInstance(), "SearchFragment");
            return true;
        } else if (id == R.id.action_favourite) {
            return true;
        } else if (id == R.id.action_share) {
            return true;
        } else if (id == R.id.action_apply) {
            if (mApplication.TempLat != null && !mApplication.TempLat.equalsIgnoreCase("")) {
                App_Constant.sCurrentLat = Double.valueOf(mApplication.TempLat);
                App_Constant.sCurrentLng = Double.valueOf(mApplication.TempLong);
                App_Constant.sCurrentAddress = mApplication.TempAddress;
                App_Preference.newInstance(this).setLocation(String.valueOf(App_Constant.sCurrentLat), String.valueOf(App_Constant.sCurrentLng), App_Constant.sCurrentAddress);
                mApplication.TempLat = "";
                mApplication.TempLong = "";
                mApplication.TempAddress = "";


            }
            if (mApplication.TempsortPosition != -1) {

                switch (mApplication.TempsortPosition) {
                    case 0:
                       FilterSort="Best Rated";
                        App_Constant.currentSortFilter = "&ratingSort=desc";
                        break;
                    case 1:
                        FilterSort="Lowest Price";
                        App_Constant.currentSortFilter = "&priceSort=asc";
                        break;
                    case 2:
                        FilterSort="Highest Price";
                        App_Constant.currentSortFilter = "&priceSort=desc";
                        break;
                    case 3:
                        FilterSort="A to Z";
                        App_Constant.currentSortFilter = "&titleSort=asc";
                        break;
                    case 4:
                        FilterSort="Z to A";
                        App_Constant.currentSortFilter = "&titleSort=desc";
                        break;
                }



                sortPosition = mApplication.TempsortPosition;
                mApplication.TempsortPosition = -1;
            }
//            else  if(App_Constant.currentSortFilter.equalsIgnoreCase("")){
//                sortPosition = 0;
//                App_Constant.currentSortFilter = "&ratingSort=desc";
//            }

            App_Preference.newInstance(this).setSort(App_Constant.currentSortFilter,FilterSort,sortPosition);
            if (mApplication.TempdistancePosition != -1) {


                switch (mApplication.TempdistancePosition) {
                    case 0:
                        distancePosition=0;
                        FilterRadius=getResources().getString(R.string.Tag_5miles);
                        App_Constant.currentRadius = "5mi";
                        break;
                    case 1:
                        distancePosition=1;
                        FilterRadius=getResources().getString(R.string.Tag_20miles);
                        App_Constant.currentRadius = "20mi";
                        break;
                    case 2:
                        distancePosition=2;
                        FilterRadius=getResources().getString(R.string.Tag_50miles);
                        App_Constant.currentRadius = "50mi";
                        break;
                    case 3:
                        distancePosition=3;
                        FilterRadius=getResources().getString(R.string.Tag_100miles);
                        App_Constant.currentRadius = "100mi";
                        break;
                }

                mApplication.TempdistancePosition = -1;
                mApplication.TempcurrentD = "";
            }


            App_Preference.newInstance(this).setDistanceSort(App_Constant.currentRadius,FilterRadius,distancePosition);

            if (mApplication.TempMaxPrice != null && !mApplication.TempMaxPrice.equalsIgnoreCase("")) {
                App_Constant.max_price = mApplication.TempMaxPrice;
                mApplication.TempMaxPrice = "";
            }
            if (mApplication.TempMinPrice != null && !mApplication.TempMinPrice.equalsIgnoreCase("")) {
                App_Constant.min_price = mApplication.TempMinPrice;
                mApplication.TempMinPrice = "";
            } else {
                App_Constant.min_price = "";
            }

            getSupportFragmentManager().popBackStackImmediate("FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
         //   reloadItemFragment();
            Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            Log.d(TAG, "Update Item" +mFragment.getTag() );
            if(mFragment instanceof ItemFragment){
                Log.d(TAG, "Update Item"  );
                ((ItemFragment) mFragment).callProductListApi(true);
            }
            updateFilterText(true);
            return true;
        }

        invalidateOptionsMenu();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_items) {
           // reloadItemFragment();
            replaceFragment(Item_FrontFragment.newInstance());
        } else if (id == R.id.nav_store) {
        } else if (id == R.id.nav_cart) {
        } else if (id == R.id.nav_logout) {
            App_Preference.newInstance(this).clearData();
            finish();
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void reloadItemFragment() {
        getSupportFragmentManager().popBackStackImmediate("FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(mFragment instanceof ItemFragment){
            Log.d(TAG, "Update Item"  );
            ((ItemFragment) mFragment).callProductListApi(true);
        }else  if(mFragment instanceof SearchFragment){
            Log.d(TAG, "Update Item not Item Fragment and is Search Fragment"  );
            ((SearchFragment) mFragment).clearList=true;
            ((SearchFragment) mFragment).mPageCount=1;
            ((SearchFragment) mFragment).callProductSearchByCategoryApi(true);
        }
    }

    @Override
    public void onReplaceFragment(Fragment fragment, String tag) {
        Log.d("Ankit", "currentSortFilter: tag " + tag);
        if (tag.equalsIgnoreCase("")) {
            replaceFragment(fragment);
        } else {
            replaceFragment(fragment, tag);
        }
    }

    @Override
    public void onReplaceFilterFragment(Fragment fragment, String tag) {
        // getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, fragment, tag).addToBackStack(tag).commit();
    }


    public void replaceFragment(Fragment mFragment, String Tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mFragment, Tag).addToBackStack(Tag).commit();
    }

    public void replaceFragment(Fragment mFragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mFragment).addToBackStack(null).commit();
    }



    public void updateFilterText(boolean reload) {




        String FilterStr="Filters: " + App_Constant.FilterSort + ", " + App_Constant.sCurrentAddress + ", " + App_Constant.FilterRadius ;
        if(!App_Constant.currentCateogryFilter.equalsIgnoreCase(""))
                    FilterStr=FilterStr+App_Constant.currentCateogryFilter;


        if(!App_Constant.currentBrandsFilter.equalsIgnoreCase(""))
            FilterStr=FilterStr+App_Constant.currentBrandsFilter;
        if(!App_Constant.currentCateogryFilter.equalsIgnoreCase(""))
            FilterStr=FilterStr+App_Constant.currentCateogryFilter;
        if(!App_Constant.min_price.equalsIgnoreCase("")||!App_Constant.max_price.equalsIgnoreCase(""))
            FilterStr=FilterStr+ ", $"+App_Constant.min_price+"-$"+App_Constant.max_price;

        Log.d(TAG, "App_Constant.FilterSort  " + App_Constant.FilterSort );
        Log.d(TAG, "App_Constant.Sort Position  " + sortPosition );
        Log.d(TAG, "App_Constant.Current " + App_Constant.currentSortFilter );
        appliedFiltersTextview.setText(FilterStr);
//        Fragment mFilterFragment = getSupportFragmentManager().findFragmentById(R.id.right_drawer);
//        if (mFilterFragment instanceof FilterFragment) {
//            ((FilterFragment) mFilterFragment).updateResultCount();
//        }
         if(reload==true)
         reloadItemFragment();
    }

    public void updateFilterTextSearch( boolean reload) {




        String FilterStr="Filters: " + App_Constant.FilterSearchSort + ", " + App_Constant.sCurrentAddress + ", " + App_Constant.FilterRadius ;
        if(!App_Constant.currentSearchCateogryFilter.equalsIgnoreCase(""))
            FilterStr=FilterStr+App_Constant.currentSearchCateogryFilter;


        if(!App_Constant.currentSearchBrandsFilter.equalsIgnoreCase(""))
            FilterStr=FilterStr+App_Constant.currentSearchBrandsFilter;
        if(!App_Constant.currentSearchCategory.equalsIgnoreCase(""))
            FilterStr=FilterStr+App_Constant.currentSearchCategory;
        if(!App_Constant.min_priceSearch.equalsIgnoreCase("")||!App_Constant.max_priceSearch.equalsIgnoreCase(""))
            FilterStr=FilterStr+ ", $"+App_Constant.min_priceSearch+" - $"+App_Constant.max_priceSearch;

        Log.d(TAG, "App_Constant.FilterSort  " + App_Constant.FilterSearchSort );
        Log.d(TAG, "App_Constant.Sort Position  " + sortPosition );
        Log.d(TAG, "App_Constant.Current " + App_Constant.currentSearchSortFilter );
        appliedFiltersTextview.setText(FilterStr);
        if(reload==true)
            reloadItemFragment();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void updateLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // Get the best provider between gps, network and passive
            Criteria criteria = new Criteria();
            mProviderName = locationManager.getBestProvider(criteria, true);
            Log.d(TAG, "mProviderName " + mProviderName);
            // API 23: we have to check if ACCESS_FINE_LOCATION and/or ACCESS_COARSE_LOCATION permission are granted
            if (ContextCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                // No one provider activated: prompt GPS
                Log.d(TAG, " No one provider activated: prompt GPS " + mProviderName);
                if (mProviderName == null || mProviderName.equals("")) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }

                // At least one provider activated. Get the coordinates
                switch (mProviderName) {
                    case "passive":
                        locationManager.requestLocationUpdates(mProviderName, MINIMUM_TIME, MINIMUM_DISTANCE, this);
                        mCurrentLocation = locationManager.getLastKnownLocation(mProviderName);
                        //  mCurrentLocation=location;
                        // Log.d(TAG, "Longitude== " + String.valueOf(mCurrentLocation.getLongitude()));
                        break;

                    case "network":
                        locationManager.requestLocationUpdates(mProviderName, MINIMUM_TIME, MINIMUM_DISTANCE, this);
                        mCurrentLocation = locationManager.getLastKnownLocation(mProviderName);
                        //mCurrentLocation=location;
                        //   Log.d(TAG, "Longitude== " + String.valueOf(mCurrentLocation.getLongitude()));

                        break;

                    case "gps":
                        locationManager.requestLocationUpdates(mProviderName, MINIMUM_TIME, MINIMUM_DISTANCE, this);
                        mCurrentLocation = locationManager.getLastKnownLocation(mProviderName);
                        // mCurrentLocation=location;
                        // Log.d(TAG, "Longitude== " + String.valueOf(mCurrentLocation.getLongitude()));
                        break;


                }
                Log.d(TAG, "Location Update Showing Progress Abv..............");
                if (Progress_HUD.dialog != null && Progress_HUD.dialog.isShowing()) {
                    Log.d(TAG, "Location Update Showing Progress..............");
                    if (mCurrentLocation != null && mCurrentLocation.getLatitude() != 0) {
                        Progress_HUD.hideProgressDialog(HomeActivity.this);
                        Fragment mCFragment = getCurrentFragment();
                        if (mCFragment instanceof LocationFilterFragment) {
                            Log.d(TAG, "Location Update Showing Progress inside ..............");
                            ((LocationFilterFragment) mCFragment).requestLocation();
                        }
                    } else {
                        Log.d(TAG, "When Getting Null..............");
                    }

                } else {
                    Log.d(TAG, "Location Update Showing Progress in else..............");
                }

                // One or both permissions are denied.
            } else {

                // The ACCESS_COARSE_LOCATION is denied, then I request it and manage the result in
                // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
                if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(HomeActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSION_ACCESS_COARSE_LOCATION);
                }
                // The ACCESS_FINE_LOCATION is denied, then I request it and manage the result in
                // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
                if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(HomeActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSION_ACCESS_FINE_LOCATION);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "In Exception of location..............");
        }

    }

    public Fragment getCurrentFragment() {
        Fragment mCurrentFragment = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);

        return mCurrentFragment;
    }

// This Method is called when application is lanuched or user has change the category of item From Front Page
    public void InitializeAllFilter(){

        mPageCount = 1;
        mPageSize = 40;
        BRANDNAME_LIST = new ArrayList<>();
        CATEGORYNAME_LIST = new ArrayList<>();
        STORE_LIST=new ArrayList<>();
        currentBrandsFilter="";
        currentStoreIDFilter="";
        App_Constant.min_price="";
        App_Constant.max_price="";
        App_Constant.currentCateogryIDFilter="";
    }
}
