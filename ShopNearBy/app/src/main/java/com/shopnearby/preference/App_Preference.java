package com.shopnearby.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shopnearby.modal.Address_Modal;
import com.shopnearby.modal.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ankit on 12/5/16.
 */
public class App_Preference {
    private static SharedPreferences UserPref;
    private static SharedPreferences AppPref;

    public App_Preference(Context mContext) {
        UserPref = mContext.getSharedPreferences("User",
                Context.MODE_PRIVATE);
        AppPref = mContext.getSharedPreferences("App",
                Context.MODE_PRIVATE);
    }

    public static App_Preference newInstance(Context mContext) {
        App_Preference mPreference = new App_Preference(mContext);
        UserPref = mContext.getSharedPreferences("User",
                Context.MODE_PRIVATE);
        AppPref = mContext.getSharedPreferences("App",
                Context.MODE_PRIVATE);
        return mPreference;
    }

    public void setUserData(User result) {
        Log.d("Ankit", "setUserData");
        SharedPreferences.Editor edit = UserPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(result);
        edit.putString("user_data", json);
        edit.apply();
    }

    public User getUserData() {
        Gson gson = new Gson();
        String json = UserPref.getString("user_data", "");
        return gson.fromJson(json, User.class);
    }

    public void clearData() {
        SharedPreferences.Editor edit = UserPref.edit();
        edit.clear();
        edit.commit();
    }

    public void setShowTutorial() {
        SharedPreferences.Editor edit = AppPref.edit();
        edit.putBoolean("Tutorial", false);
        edit.apply();
    }

    public boolean getShowTutorial() {
        return AppPref.getBoolean("Tutorial", true);
    }

    public void setShowLocation() {
        SharedPreferences.Editor edit = AppPref.edit();
        edit.putBoolean("Location", false);
        edit.apply();
    }

    public boolean getShowLocation() {
        return AppPref.getBoolean("Location", true);
    }

    public void setLocation(String lat, String lng, String address) {
        SharedPreferences.Editor edit = AppPref.edit();
        edit.putString("Latitude", lat);
        edit.putString("Longitude", lng);
        edit.putString("Address", address);
        edit.apply();
    }

    public void setSort(String CurrentSort, String Filter,int sortPos) {
        SharedPreferences.Editor edit = AppPref.edit();
        edit.putString("currentSortFilter", CurrentSort);
        edit.putString("Filter", Filter);
        edit.putInt("SortPos",sortPos);
        edit.apply();
    }
    public String getCurrentSort() {
        return AppPref.getString("currentSortFilter", "");
    }

    public String getFilter() {
        return AppPref.getString("Filter", "");
    }

    public int getSortPos(){return  AppPref.getInt("SortPos",-1);}

    public void setDistanceSort(String currentRadius, String FilterRadius,int distPos) {
        SharedPreferences.Editor edit = AppPref.edit();
        edit.putString("currentRadius", currentRadius);
        edit.putString("FilterRadius", FilterRadius);
        edit.putInt("distPos",distPos);
        edit.apply();
    }
    public String getcurrentRadius() {
        return AppPref.getString("currentRadius", "");
    }

    public String getFilterRadius() {
        return AppPref.getString("FilterRadius", "");
    }

    public int getDistancePos() {
        return AppPref.getInt("distPos",-1);
    }

    public String getLatitude() {
        return AppPref.getString("Latitude", "");
    }

    public String getLongitude() {
        return AppPref.getString("Longitude", "");
    }

    public String getAddress() {
        return AppPref.getString("Address", "");
    }

    public void saveSearchedItems(List<String> list) {
        SharedPreferences.Editor edit = UserPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        edit.putString("SearchList", json);
        edit.apply();
    }

    public void saveSearchedLocation(List<Address_Modal> list) {
        SharedPreferences.Editor edit = UserPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        edit.putString("SearchLocation", json);
        edit.apply();
    }

    public ArrayList<String> getSaveSearchedItems() {
        Gson gson = new Gson();
        String json = UserPref.getString("SearchList", "");
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public ArrayList<Address_Modal> getSaveSearchedLocation() {
        Gson gson = new Gson();
        String json = UserPref.getString("SearchLocation", "");
        Type type = new TypeToken<ArrayList<Address_Modal>>() {
        }.getType();


        return gson.fromJson(json,type);
    }
}

