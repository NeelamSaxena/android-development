package com.shopnearby;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.fragment.AddLocationFragment;
import com.shopnearby.fragment.LoginOptionFragment;
import com.shopnearby.location.LocationManagerFragmentActivity;
import com.shopnearby.preference.App_Preference;

public class LoginActivity extends LocationManagerFragmentActivity implements OnChangeFragment {

    FragmentManager mFragManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FacebookSdk.sdkInitialize(getApplicationContext());
        mFragManager = getSupportFragmentManager();

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    getPackageName(), PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:",
//                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        }  catch (Exception e) {
//            e.printStackTrace();
//        }
        App_Preference.newInstance(this).setShowTutorial();

        if (App_Preference.newInstance(this).getShowLocation()) {
            Replacefragment(AddLocationFragment.newInstance());
        } else {
            Replacefragment(LoginOptionFragment.newInstance());
        }


    }

    public void Replacefragment(Fragment mFragment, String Tag) {
        mFragManager.beginTransaction().replace(R.id.content_frame, mFragment, Tag).addToBackStack(Tag).commit();
    }

    public void Replacefragment(Fragment mFragment) {
        mFragManager.beginTransaction().replace(R.id.content_frame, mFragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        Fragment mCurrentFragment = getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);
        if (mCurrentFragment instanceof AddLocationFragment || mCurrentFragment instanceof LoginOptionFragment) {
            this.finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onReplaceFragment(Fragment fragment, String tag) {
        if (tag.equalsIgnoreCase("")) {
            Replacefragment(fragment);
        } else {
            Replacefragment(fragment, tag);
        }
    }

    @Override
    public void onReplaceFilterFragment(Fragment fragment, String tag) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case App_Constant.MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Fragment mCurrentFragment = getSupportFragmentManager()
                            .findFragmentById(R.id.content_frame);
                    if (mCurrentFragment instanceof AddLocationFragment)
                    {
                        ((AddLocationFragment) mCurrentFragment).checkForGps();
                    }
                }
            }
        }
    }
}
