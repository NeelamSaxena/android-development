package com.shopnearby.callbacks;

/**
 * Created by ankit on 11/05/16.
 */
public interface GPS_StatusListener {
    public abstract void onGPSStatusChanged(String status);
}
