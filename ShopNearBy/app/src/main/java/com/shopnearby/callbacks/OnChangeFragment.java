package com.shopnearby.callbacks;

import android.support.v4.app.Fragment;

/**
 * Created by ankit on 12/5/16.
 */
public interface OnChangeFragment {

    void onReplaceFragment(Fragment fragment, String tag);
    void onReplaceFilterFragment(Fragment fragment, String tag);
}
