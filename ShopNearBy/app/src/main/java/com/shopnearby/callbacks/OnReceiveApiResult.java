package com.shopnearby.callbacks;

import com.shopnearby.constant.App_Constant;

/**
 * Created by ankit on 30/4/16.
 */
public interface OnReceiveApiResult {

    void onReceiveResult(App_Constant.URL URL_Type, String ApiResult);

    void onErrorResult(App_Constant.URL URL_Type, String Req_Params,
                       String ApiResult);
}
