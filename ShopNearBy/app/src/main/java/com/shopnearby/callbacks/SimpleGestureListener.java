package com.shopnearby.callbacks;

/**
 * Created by ankit on 14/5/16.
 */
public interface SimpleGestureListener {
        void onSwipe(int direction);
        void onDoubleTap();
}
