package com.shopnearby.location;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationManagerFragmentActivity extends FragmentActivity
		implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,LocationListener {

	protected GoogleApiClient mGoogleApiClient;
	static final int MY_PERMISSIONS_REQUEST_LOCATION = 22;
	private LocationRequest mLocationRequest;

	private final String TAG = "Locationclient";

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

//		mLocationClient = new LocationClient(this, this, this);

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! do the
					// calendar task you need to do.
					LocationServices.FusedLocationApi.requestLocationUpdates(
							mGoogleApiClient, mLocationRequest, this);
				} else {
					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
			}

			// other 'switch' lines to check for other
			// permissions this app might request
		}
	}

	/*
         * Called when the Activity is no longer visible at all. Stop updates and
         * disconnect.
         */
	@Override
	public void onStop() {

		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}

		super.onStop();
	}

	/*
     * Called when the Activity is restarted, even before it becomes visible.
     */
	@Override
	public void onStart() {

		super.onStart();

		/*
         * Connect the client. Don't re-start any requests here; instead, wait
		 * for onResume()
		 */


	}

	@Override
	protected void onResume() {
		super.onResume();

		mGoogleApiClient.connect();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
									Intent intent) {

		// Choose what to do based on the request code
		switch (requestCode) {

			// If the request code matches the code sent in onConnectionFailed
			case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:

				switch (resultCode) {
					// If Google Play services resolved the problem
					case Activity.RESULT_OK:

						// Log the result
						Log.d(LocationUtils.APPTAG, "Resolved");

					default:
						// Log the result
						Log.d(LocationUtils.APPTAG, "Not Resolved");

						break;
				}

				// If any other request code was received
			default:

				break;
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 *
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(LocationUtils.APPTAG,
					"play services available");

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(),
						LocationUtils.APPTAG);
			}
			return false;
		}
	}

	@Override
	public void onLocationChanged(Location location) {

	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 *
		 * @param dialog An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
         * This method must return a Dialog to the DialogFragment.
         */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

		/*
         * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {

				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

				/*
                 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */

			} catch (IntentSender.SendIntentException e) {

				// Log the error
				e.printStackTrace();
			}
		} else {

			// If no resolution is available, display a dialog to the user with
			// the error.
			showErrorDialog(connectionResult.getErrorCode());
		}

	}

	/**
	 * Show a dialog returned by Google Play services for the connection error
	 * code
	 *
	 * @param errorCode An error code returned from onConnectionFailed
	 */
	private void showErrorDialog(int errorCode) {

		// Get the error dialog from Google Play services
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
				this, LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

		// If Google Play services can provide an error dialog
		if (errorDialog != null) {

			// Create a new DialogFragment in which to show the error dialog
			ErrorDialogFragment errorFragment = new ErrorDialogFragment();

			// Set the dialog in the DialogFragment
			errorFragment.setDialog(errorDialog);

			// Show the error dialog in the DialogFragment
			errorFragment.show(getSupportFragmentManager(),
					LocationUtils.APPTAG);
		}
	}

	@Override
	public void onConnected(Bundle arg0) {

		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(1000); // Update location every second

		int permissionCheck = ContextCompat.checkSelfPermission(LocationManagerFragmentActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

		if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

			LocationServices.FusedLocationApi.requestLocationUpdates(
					mGoogleApiClient, mLocationRequest, this);
			//Execute location service call if user has explicitly granted ACCESS_FINE_LOCATION..
		}
//		else {
//			ActivityCompat.requestPermissions(LocationManagerFragmentActivity.this,
//					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//					MY_PERMISSIONS_REQUEST_LOCATION);
//		}

//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, this);


	}


	@Override
	public void onConnectionSuspended(int i) {
		Log.i(TAG, "GoogleApiClient connection has been suspend");
	}


	/*@Override
    public void onDisconnected() {
		// TODO Auto-generated method stub

	}*/

	public GoogleApiClient getLocationClient() {
		return mGoogleApiClient;
	}

}