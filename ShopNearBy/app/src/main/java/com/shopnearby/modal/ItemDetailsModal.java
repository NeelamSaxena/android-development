package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ankit on 30/5/16.
 */
public class ItemDetailsModal implements Parcelable {
    public String strId = "";
    public String strTitle = "";
    public String strImage = "";
    public String strReviewCount = "";
    public String strReviewAverage = "";
    public float ReviewAverage;
    public String strBrand = "";
    public String strDescription = "";
    public String strLongDesc = "";
    public String strUpdateTime = "";
    public ArrayList<StoreModal> mStoreList;
    public ArrayList<SpecificationModal> mDetailList;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrReviewCount() {
        return strReviewCount;
    }

    public void setStrReviewCount(String strReviewCount) {
        this.strReviewCount = strReviewCount;
    }

    public String getStrReviewAverage() {
        return strReviewAverage;
    }

    public void setStrReviewAverage(String strReviewAverage) {
        this.strReviewAverage = strReviewAverage;
    }

    public float getReviewAverage() {
        return ReviewAverage;
    }

    public void setReviewAverage(float reviewAverage) {
        ReviewAverage = reviewAverage;
    }

    public String getStrBrand() {
        return strBrand;
    }

    public void setStrBrand(String strBrand) {
        this.strBrand = strBrand;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    public String getStrLongDesc() {
        return strLongDesc;
    }

    public void setStrLongDesc(String strLongDesc) {
        this.strLongDesc = strLongDesc;
    }

    public String getStrUpdateTime() {
        return strUpdateTime;
    }

    public void setStrUpdateTime(String strUpdateTime) {
        this.strUpdateTime = strUpdateTime;
    }

    public ArrayList<StoreModal> getmStoreList() {
        return mStoreList;
    }

    public void setmStoreList(ArrayList<StoreModal> mStoreList) {
        this.mStoreList = mStoreList;
    }

    public ArrayList<SpecificationModal> getmDetailList() {
        return mDetailList;
    }

    public void setmDetailList(ArrayList<SpecificationModal> mDetailList) {
        this.mDetailList = mDetailList;
    }

    public ItemDetailsModal() {

    }

    private ItemDetailsModal(Parcel in) {
        strId = in.readString();
        strTitle = in.readString();
        strImage = in.readString();
        strReviewCount = in.readString();
        strReviewAverage = in.readString();
        ReviewAverage = in.readFloat();
        strBrand = in.readString();
        strDescription = in.readString();
        strLongDesc = in.readString();
        strUpdateTime = in.readString();
        in.readTypedList(mStoreList, StoreModal.CREATOR);
        in.readTypedList(mDetailList, SpecificationModal.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(strId);
        parcel.writeString(strTitle);
        parcel.writeString(strImage);
        parcel.writeString(strReviewCount);
        parcel.writeString(strReviewAverage);
        parcel.writeFloat(ReviewAverage);
        parcel.writeString(strBrand);
        parcel.writeString(strDescription);
        parcel.writeString(strLongDesc);
        parcel.writeString(strUpdateTime);
        parcel.writeTypedList(mStoreList);
        parcel.writeTypedList(mDetailList);

    }


    public static final Creator<ItemDetailsModal> CREATOR = new Creator<ItemDetailsModal>() {
        @Override
        public ItemDetailsModal createFromParcel(Parcel in) {
            return new ItemDetailsModal(in);
        }

        @Override
        public ItemDetailsModal[] newArray(int size) {
            return new ItemDetailsModal[size];
        }
    };

    public String getArraySize() {
        if (mStoreList == null && mStoreList.size() == 0) {
            return String.valueOf("0");
        }
        return String.valueOf(mStoreList.size());
    }

}
