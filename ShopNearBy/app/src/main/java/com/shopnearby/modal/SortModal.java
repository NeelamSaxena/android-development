package com.shopnearby.modal;

/**
 * Created by shailendra on 10/6/16.
 */

public class SortModal {
    public String sortName="";
    public String key="";
    public String value="";
    public boolean isSelected;

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
