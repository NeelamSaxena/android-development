package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankit on 30/5/16.
 */
public class StoreModal implements Parcelable, Cloneable {
    public boolean isSelected;
    int count = 1;
    public String strId = "";
    public String storeId = "";
    public String retailerId = "";
    public String storeName = "";
    public String address = "";
    public String city = "";
    public String state = "";
    public String postalCode = "";
    public String country = "";
    public String phoneNumber = "";
    public double storeLat;
    public double storeLng;
    public double distance;
    public String openTime = "";
    public String closeTime = "";
    public String retailerName = "";
    public String price = "";
    public String affilateUrl = "";
    public String UpdateDate = "";
    public boolean inStoreAvailability;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getStoreLat() {
        return storeLat;
    }

    public void setStoreLat(double storeLat) {
        this.storeLat = storeLat;
    }

    public double getStoreLng() {
        return storeLng;
    }

    public void setStoreLng(double storeLng) {
        this.storeLng = storeLng;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAffilateUrl() {
        return affilateUrl;
    }

    public void setAffilateUrl(String affilateUrl) {
        this.affilateUrl = affilateUrl;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public boolean isInStoreAvailability() {
        return inStoreAvailability;
    }

    public void setInStoreAvailability(boolean inStoreAvailability) {
        this.inStoreAvailability = inStoreAvailability;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public StoreModal() {

    }

    private StoreModal(Parcel in) {
        strId = in.readString();
        storeId = in.readString();
        retailerId = in.readString();
        storeName = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        postalCode = in.readString();
        country = in.readString();
        phoneNumber = in.readString();
        storeLat = in.readDouble();
        storeLng = in.readDouble();
        distance = in.readDouble();
        openTime = in.readString();
        closeTime = in.readString();
        retailerName = in.readString();
        price = in.readString();
        affilateUrl = in.readString();
        UpdateDate = in.readString();
        isSelected = in.readByte() != 0;
        inStoreAvailability = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(strId);
        parcel.writeString(storeId);
        parcel.writeString(retailerId);
        parcel.writeString(storeName);
        parcel.writeString(address);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(postalCode);
        parcel.writeString(country);
        parcel.writeString(phoneNumber);
        parcel.writeDouble(storeLat);
        parcel.writeDouble(storeLng);
        parcel.writeDouble(distance);
        parcel.writeString(openTime);
        parcel.writeString(closeTime);
        parcel.writeString(retailerName);
        parcel.writeString(affilateUrl);
        parcel.writeString(UpdateDate);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
        parcel.writeByte((byte) (inStoreAvailability ? 1 : 0));
    }

    public static final Creator<StoreModal> CREATOR = new Creator<StoreModal>() {
        public StoreModal createFromParcel(Parcel in) {
            return new StoreModal(in);
        }

        public StoreModal[] newArray(int size) {
            return new StoreModal[size];
        }
    };

    public String getStoreTitle() {
        String name = count + ". " + retailerName + " " + storeName;
        count++;
        return name;
    }

    public StoreModal clone() {
        try {
            return (StoreModal) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
