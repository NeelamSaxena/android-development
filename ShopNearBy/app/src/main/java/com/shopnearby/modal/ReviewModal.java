package com.shopnearby.modal;

/**
 * Created by ankit on 2/6/16.
 */
public class ReviewModal {

    public String strId = "";
    public String strReviewTitle = "";
    public String strReviewComment = "";
    public String strRating = "";
    public String strReviewDate = "";
    public String strName = "";

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStrReviewTitle() {
        return strReviewTitle;
    }

    public void setStrReviewTitle(String strReviewTitle) {
        this.strReviewTitle = strReviewTitle;
    }

    public String getStrReviewComment() {
        return strReviewComment;
    }

    public void setStrReviewComment(String strReviewComment) {
        this.strReviewComment = strReviewComment;
    }

    public String getStrRating() {
        return strRating;
    }

    public void setStrRating(String strRating) {
        this.strRating = strRating;
    }

    public String getStrReviewDate() {
        return strReviewDate;
    }

    public void setStrReviewDate(String strReviewDate) {
        this.strReviewDate = strReviewDate;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public float getRating() {
        if (!strRating.equalsIgnoreCase("")) {
            return Float.parseFloat(strRating);
        } else {
            return 0;
        }
    }
}
