package com.shopnearby.modal;

/**
 * Created by shailendra on 10/6/16.
 */

public class BrandModal {
    public String brandName = "";
    public boolean isSelected;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
