package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by neelam on 17/6/16.
 */

public class ItemFrontModel implements Parcelable{


    public String strPageType="";
    public String strName="";
    public String strCategory="";
    public String strOrder="";
    public String strImage="";

    public ItemFrontModel(){

    }

    protected ItemFrontModel(Parcel in) {
        strPageType = in.readString();
        strName = in.readString();
        strCategory = in.readString();
        strOrder = in.readString();
        strImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strPageType);
        dest.writeString(strName);
        dest.writeString(strCategory);
        dest.writeString(strOrder);
        dest.writeString(strImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemFrontModel> CREATOR = new Creator<ItemFrontModel>() {
        @Override
        public ItemFrontModel createFromParcel(Parcel in) {
            return new ItemFrontModel(in);
        }

        @Override
        public ItemFrontModel[] newArray(int size) {
            return new ItemFrontModel[size];
        }
    };


    public String getStrPageType() {
        return strPageType;
    }

    public void setStrPageType(String strPageType) {
        this.strPageType = strPageType;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrCategory() {
        return strCategory;
    }

    public void setStrCategory(String strCategory) {
        this.strCategory = strCategory;
    }

    public String getStrOrder() {
        return strOrder;
    }

    public void setStrOrder(String strOrder) {
        this.strOrder = strOrder;
    }

    public String getStrimage() {
        return strImage;
    }

    public void setStrimage(String strimage) {
        this.strImage = strimage;
    }
}
