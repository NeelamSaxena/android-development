package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by shailendra on 9/6/16.
 */

public class SearchCategoryModal implements Parcelable{

    public String id = "";
    public String name = "";
    public String text = "";
    public boolean isSelected;
    public String parent="";

    public ArrayList<SearchCategoryModal> ChildArray=new ArrayList<SearchCategoryModal>();

    public SearchCategoryModal(){

    }

    protected SearchCategoryModal(Parcel in) {
        id = in.readString();
        name = in.readString();
        text = in.readString();
        parent = in.readString();
        isSelected = in.readByte() != 0;
        ChildArray = in.createTypedArrayList(SearchCategoryModal.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(text);
        dest.writeString(parent);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeTypedList(ChildArray);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchCategoryModal> CREATOR = new Creator<SearchCategoryModal>() {
        @Override
        public SearchCategoryModal createFromParcel(Parcel in) {
            return new SearchCategoryModal(in);
        }

        @Override
        public SearchCategoryModal[] newArray(int size) {
            return new SearchCategoryModal[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<SearchCategoryModal> getChildArray() {
        return ChildArray;
    }

    public void setChildArray(ArrayList<SearchCategoryModal> childArray) {
        ChildArray = childArray;
    }


    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}
