package com.shopnearby.modal;

/**
 * Created by ankit on 12/5/16.
 */
public class User {
    public String strUserFName="";
    public String strUserLName="";
    public String strUserEmail="";
    public String strUserId="";
    public String strUserState="";
    public String strUserToken="";
    public String strDoB="";
    public String strGender="";
    public boolean strUserPassword;

    public String getStrUserFName() {
        return strUserFName;
    }

    public void setStrUserFName(String strUserFName) {
        this.strUserFName = strUserFName;
    }

    public String getStrUserLName() {
        return strUserLName;
    }

    public void setStrUserLName(String strUserLName) {
        this.strUserLName = strUserLName;
    }

    public String getStrUserEmail() {
        return strUserEmail;
    }

    public void setStrUserEmail(String strUserEmail) {
        this.strUserEmail = strUserEmail;
    }

    public String getStrUserId() {
        return strUserId;
    }

    public void setStrUserId(String strUserId) {
        this.strUserId = strUserId;
    }

    public String getStrUserState() {
        return strUserState;
    }

    public void setStrUserState(String strUserState) {
        this.strUserState = strUserState;
    }

    public String getStrUserToken() {
        return strUserToken;
    }

    public void setStrUserToken(String strUserToken) {
        this.strUserToken = strUserToken;
    }

    public String getStrDoB() {
        return strDoB;
    }

    public void setStrDoB(String strDoB) {
        this.strDoB = strDoB;
    }

    public String getStrGender() {
        return strGender;
    }

    public void setStrGender(String strGender) {
        this.strGender = strGender;
    }

    public boolean isStrUserPassword() {
        return strUserPassword;
    }

    public void setStrUserPassword(boolean strUserPassword) {
        this.strUserPassword = strUserPassword;
    }
}
