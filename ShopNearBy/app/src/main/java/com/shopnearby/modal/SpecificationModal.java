package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankit on 3/6/16.
 */
public class SpecificationModal implements Parcelable {
    public String name = "";
    public String value = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SpecificationModal() {

    }

    private SpecificationModal(Parcel in) {
        name = in.readString();
        value = in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(value);
    }

    public static final Creator<SpecificationModal> CREATOR = new Creator<SpecificationModal>() {
        @Override
        public SpecificationModal createFromParcel(Parcel in) {
            return new SpecificationModal(in);
        }

        @Override
        public SpecificationModal[] newArray(int size) {
            return new SpecificationModal[size];
        }
    };
}
