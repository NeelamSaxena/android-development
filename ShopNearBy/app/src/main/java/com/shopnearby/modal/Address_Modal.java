package com.shopnearby.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ankit on 11/05/16.
 */
public class Address_Modal implements Parcelable {

    public String str_add_text = "";
    public String str_add_number = "";
    public String str_add_street = "";
    public String str_add_postalcode = "";
    public String str_lat = "";
    public String str_lng = "";
    public String str_length = "";
    public String Str_CountryCode = "";
    public String Str_Msg = "";
    public String str_Street_text = "";
    public  boolean isSelected;

    public String getStr_add_text() {
        return str_add_text;
    }

    public void setStr_add_text(String str_add_text) {
        this.str_add_text = str_add_text;
    }

    public String getStr_add_number() {
        return str_add_number;
    }

    public void setStr_add_number(String str_add_number) {
        this.str_add_number = str_add_number;
    }

    public String getStr_add_street() {
        return str_add_street;
    }

    public void setStr_add_street(String str_add_street) {
        this.str_add_street = str_add_street;
    }

    public String getStr_add_postalcode() {
        return str_add_postalcode;
    }

    public void setStr_add_postalcode(String str_add_postalcode) {
        this.str_add_postalcode = str_add_postalcode;
    }

    public String getStr_lat() {
        return str_lat;
    }

    public void setStr_lat(String str_lat) {
        this.str_lat = str_lat;
    }

    public String getStr_lng() {
        return str_lng;
    }

    public void setStr_lng(String str_lng) {
        this.str_lng = str_lng;
    }

    public String getStr_length() {
        return str_length;
    }

    public void setStr_length(String str_length) {
        this.str_length = str_length;
    }

    public String getStr_CountryCode() {
        return Str_CountryCode;
    }

    public void setStr_CountryCode(String str_CountryCode) {
        Str_CountryCode = str_CountryCode;
    }

    public String getStr_Msg() {
        return Str_Msg;
    }

    public void setStr_Msg(String str_Msg) {
        Str_Msg = str_Msg;
    }

    public String getStr_Street_text() {
        return str_Street_text;
    }

    public void setStr_Street_text(String str_Street_text) {
        this.str_Street_text = str_Street_text;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Address_Modal() {

    }

    private Address_Modal(Parcel in) {
        str_add_text = in.readString();
        str_add_number = in.readString();
        str_add_street = in.readString();
        str_add_postalcode = in.readString();
        str_lat = in.readString();
        str_lng = in.readString();
        str_length = in.readString();
        Str_CountryCode = in.readString();
        Str_Msg = in.readString();
        str_Street_text=in.readString();
        isSelected = in.readByte() != 0;
    }
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(str_add_text);
        parcel.writeString(str_add_number);
        parcel.writeString(str_add_street);
        parcel.writeString(str_add_postalcode);
        parcel.writeString(str_lat);
        parcel.writeString(str_lng);
        parcel.writeString(str_length);
        parcel.writeString(Str_CountryCode);
        parcel.writeString(Str_Msg);
        parcel.writeString(str_Street_text);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }

    public static final Creator<Address_Modal> CREATOR = new Creator<Address_Modal>() {
        public Address_Modal createFromParcel(Parcel in) {
            return new Address_Modal(in);
        }

        public Address_Modal[] newArray(int size) {
            return new Address_Modal[size];
        }
    };
}
