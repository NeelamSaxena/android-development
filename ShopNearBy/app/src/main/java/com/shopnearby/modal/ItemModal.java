package com.shopnearby.modal;

import android.util.Log;

/**
 * Created by ankit on 30/5/16.
 */
public class ItemModal {

    public String strId = "";
    public String strImage = "";
    public String strCustomerReviewCount = "";
    public String strMaxPrice = "";
    public String strMinPrice = "";
    public int mStoreCount;
    public String strTitle = "";
    public String strReviewAverage = "0";
    public boolean InStoreAvailability;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrCustomerReviewCount() {
        return strCustomerReviewCount;
    }

    public void setStrCustomerReviewCount(String strCustomerReviewCount) {
        this.strCustomerReviewCount = strCustomerReviewCount;
    }

    public String getStrMaxPrice() {
        return strMaxPrice;
    }

    public void setStrMaxPrice(String strMaxPrice) {
        this.strMaxPrice = strMaxPrice;
    }

    public String getStrMinPrice() {
        return strMinPrice;
    }

    public void setStrMinPrice(String strMinPrice) {
        this.strMinPrice = strMinPrice;
    }

    public int getmStoreCount() {
        return mStoreCount;
    }

    public void setmStoreCount(int mStoreCount) {
        this.mStoreCount = mStoreCount;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrReviewAverage() {
        return strReviewAverage;
    }

    public void setStrReviewAverage(String strReviewAverage) {
        this.strReviewAverage = strReviewAverage;
    }

    public boolean isInStoreAvailability() {
        return InStoreAvailability;
    }

    public void setInStoreAvailability(boolean inStoreAvailability) {
        InStoreAvailability = inStoreAvailability;
    }

    public String getItemPrice() {

        if (strMaxPrice.equalsIgnoreCase(strMinPrice)) {
            return "$" + strMaxPrice;
        } else {
            return "$" + strMinPrice + " - " + "$" +strMaxPrice;
        }
    }

    public String getStores() {
        if (mStoreCount > 1) {
            return "Sold in " + mStoreCount + " stores";
        } else {
            return "Sold in " + mStoreCount + " store";
        }
    }

    public float getRating() {
//        Log.d("Ankit", "strReviewAverage: " + strReviewAverage);
        if (!strReviewAverage.equalsIgnoreCase("")) {
            return Float.parseFloat(strReviewAverage);
        } else {
            return 0;
        }
    }

}
