package com.shopnearby;

import android.os.Bundle;
import android.widget.Toast;

import com.shopnearby.custom.OnboarderPage;

import java.util.ArrayList;
import java.util.List;

public class TutorialActivity extends OnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnboarderPage onboarderPage1 = new OnboarderPage(0);
        OnboarderPage onboarderPage2 = new OnboarderPage(R.drawable.tutorial_a);
        OnboarderPage onboarderPage3 = new OnboarderPage(R.drawable.tutorial_b);
        OnboarderPage onboarderPage4 = new OnboarderPage(R.drawable.tutorial_c);
        OnboarderPage onboarderPage5 = new OnboarderPage(R.drawable.tutorial_d);

        List<OnboarderPage> pages = new ArrayList<>();

        pages.add(onboarderPage1);
        pages.add(onboarderPage2);
        pages.add(onboarderPage3);
        pages.add(onboarderPage4);
        pages.add(onboarderPage5);

        setOnboardPagesReady(pages);

    }

    @Override
    public void onSkipButtonPressed() {
        super.onSkipButtonPressed();
        Toast.makeText(this, "Skip button was pressed!", Toast.LENGTH_SHORT).show();
    }


}
