package com.shopnearby.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;
import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.location.LocationFragment;
import com.shopnearby.modal.Address_Modal;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.AddressJSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 10/5/16.
 */
public class AddLocationFragment extends LocationFragment {
    OnChangeFragment mChangeFragmentListener;
    private String TAG = "AddLocationFragment";
    private SNB_Application mApplication;
    private GetLatLngFromAddress latlng_task;
    private LocationManager locationManager;
    private Location mLastLocation;
    private int REQUEST_CODE = 100;
    private String strAddress = "";

    @InjectView(R.id.et_location)
    PlacesAutocompleteTextView ET_Location;

    @OnClick(R.id.btn_useCurrentLocation)
    public void goToNextPage() {
        checkForPermission();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    public static AddLocationFragment newInstance() {
        return new AddLocationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (SNB_Application) getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_location, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ET_Location.clearFocus();
        ET_Location.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                ET_Location.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
//                        ET_Location.setSelection(0);
                        mApplication.hideSoftKeyboard(getActivity(), ET_Location);
                        if (latlng_task != null) {
                            latlng_task.cancel(true);
                        }

                        strAddress = details.formatted_address;
                        latlng_task = new GetLatLngFromAddress();
                        latlng_task.execute(details.place_id);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                        mApplication.ShowAlert(getActivity(), getString(R.string.ERROR_InternetProblem),getActivity().getResources().getString(R.string.app_name));
                    }
                });
            }
        });

    }

    private void checkForPermission() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for lollipop and newer versions
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "permissionCheck granted");
                checkForGps();
            } else {
                Log.d(TAG, "permissionCheck denied");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        App_Constant.MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            checkForGps();
        }
    }


    public void checkForGps() {
        Log.d(TAG, "checkForGps");
        locationManager = (LocationManager) getActivity()
                .getSystemService(getActivity().LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGPSEnabled) {
            Log.d(TAG, "GPS IS OFF");
            ShowAlert(getActivity());
        } else {
            requestLocation();
        }
    }

    private void requestLocation() {
        Log.d(TAG, "GPS IS ON");
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                App_Constant.sCurrentLat = mLastLocation.getLatitude();
                App_Constant.sCurrentLng = mLastLocation.getLongitude();
                App_Constant.sCurrentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                Log.d("Ankit", "Latitude== " + String.valueOf(mLastLocation.getLatitude()));
                Log.d("Ankit", "Longitude== " + String.valueOf(mLastLocation.getLongitude()));
                getAddressFromLatLng();
                strAddress="Current Location";
                App_Preference.newInstance(getActivity()).setLocation(String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()), strAddress);
                if (mChangeFragmentListener != null) {
                    mChangeFragmentListener.onReplaceFragment(LoginOptionFragment.newInstance(), TAG);
                }

            } else {

                Log.d("Ankit", "NO location found...............");

            }


        } catch (Exception e) {
            Log.d("Ankit", "Current Location =0,0");
            e.printStackTrace();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private class GetLatLngFromAddress extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";
            Log.d(TAG, "GetLatLngFromAddress doing bg");
            String input = "";
            try {
                input = URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                Log.d(TAG, "error= " + e1.toString());
                e1.printStackTrace();
            }

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + input + "&key=" + App_Constant.API_KEY;

            Log.d(TAG, "url of near= " + url);

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parseAddressResponse(result);
        }
    }

    private void parseAddressResponse(String result) {

        AddressJSONParser addressParser = new AddressJSONParser();
        Address_Modal mAddressModal = addressParser.parse(result);
        if (mAddressModal != null) {

            App_Constant.sCurrentLat = Double.parseDouble(mAddressModal.getStr_lat());
            App_Constant.sCurrentLng = Double.parseDouble(mAddressModal.getStr_lng());
            Log.d(TAG, "Location Lat :" + mAddressModal.getStr_lat());
            Log.d(TAG, "Location Lat :" + mAddressModal.getStr_lng());
            App_Preference.newInstance(getActivity()).setLocation(mAddressModal.getStr_lat(), mAddressModal.getStr_lng(), strAddress);
            if (mChangeFragmentListener != null) {
                mChangeFragmentListener.onReplaceFragment(LoginOptionFragment.newInstance(), TAG);
            }
        } else {
            mApplication.ShowAlert(getActivity(), getResources().getString(R.string.Error_NoAddress),getActivity().getResources().getString(R.string.app_name));
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            Log.d(TAG, "data= " + data);

            br.close();

        } catch (Exception e) {

            Log.d(TAG, e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void getAddressFromLatLng() {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            addresses = geocoder.getFromLocation(App_Constant.sCurrentLat, App_Constant.sCurrentLat, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            Log.d("Ankit", "city: " + city);
            Log.d("Ankit", "state: " + state);
            Log.d("Ankit", "country: " + country);

            if (city == null || city.equalsIgnoreCase("null")) {
                city = "";
            }
            if (state == null || !state.equalsIgnoreCase("null")) {
                state = "";
            } else {
                state = "," + state;
            }
            strAddress = city + state;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ShowAlert(Context mContext) {

        AlertDialog.Builder mAlert = new AlertDialog.Builder(mContext);
        mAlert.setCancelable(false);
        mAlert.setTitle(mContext.getResources().getString(R.string.app_name));
        mAlert.setMessage(mContext.getResources().getString(R.string.GPS_Message));

        mAlert.setPositiveButton(mContext.getResources().getString(R.string.Tag_Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
                dialog.dismiss();
            }
        });
        mAlert.setNegativeButton(mContext.getResources().getString(R.string.Tag_Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mAlert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (REQUEST_CODE == requestCode) {
            requestLocation();
        }
    }

}
