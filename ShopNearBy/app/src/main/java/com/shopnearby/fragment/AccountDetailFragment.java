package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 12/5/16.
 */
public class AccountDetailFragment extends Fragment {

    private String TAG = "AccountDetailFragment";
    OnChangeFragment mChangeFragmentListener;
    private String strFirstName = "";
    private String strLastName = "";
    private String strPassword = "";
    private String strEmail = "";
    private SNB_Application mApplication;

    @InjectView(R.id.tv_pageTitle)
    TextView mPageTitleTextView;

    @InjectView(R.id.input_fname)
    EditText mFNameEditText;
    @InjectView(R.id.input_lname)
    EditText mLNameEditText;
    @InjectView(R.id.input_password)
    EditText mPasswordEditText;

    @InjectView(R.id.btn_next)
    Button mNextButton;


    @OnClick(R.id.img_back)
    public void goBack() {
        mApplication.hideSoftKeyboard(getActivity(), mFNameEditText);
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_next)
    public void onNextClick() {
        mApplication.hideSoftKeyboard(getActivity(), mFNameEditText);
        if (mChangeFragmentListener != null) {
            mChangeFragmentListener.onReplaceFragment(PersonalizeAccFragment.newInstance(strFirstName, strLastName, strPassword, strEmail), TAG);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    public static AccountDetailFragment newInstance(String email) {
        AccountDetailFragment fragment = new AccountDetailFragment();
        Bundle args = new Bundle();
        args.putString("EMAIL", email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplication = (SNB_Application) getActivity().getApplicationContext();

        if (getArguments() != null) {
            strEmail = getArguments().getString("EMAIL");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_account, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPageTitleTextView.setText(getString(R.string.Tag_NewAccount));
        mNextButton.setEnabled(false);
        mFNameEditText.addTextChangedListener(new MyTextWatcher(mFNameEditText));
        mLNameEditText.addTextChangedListener(new MyTextWatcher(mLNameEditText));
        mPasswordEditText.addTextChangedListener(new MyTextWatcher(mPasswordEditText));
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_fname:
                case R.id.input_lname:
                case R.id.input_password:
                    if (validateFields()) {
                        mNextButton.setEnabled(true);
                        mNextButton.setAlpha(1f);
                    } else {
                        mNextButton.setEnabled(false);
                        mNextButton.setAlpha(0.6f);
                    }
                    break;
            }
        }
    }

    private boolean validateFields() {
        strFirstName = mFNameEditText.getText().toString().trim();
        strLastName = mLNameEditText.getText().toString().trim();
        strPassword = mPasswordEditText.getText().toString().trim();
        if (!strFirstName.isEmpty() && strFirstName.length() > 1) {
            if (!strLastName.isEmpty() && strLastName.length() > 1) {
                if (!strPassword.isEmpty() && strPassword.length() > 7) {
                    return true;
                }
            }
        }
        return false;
    }


}
