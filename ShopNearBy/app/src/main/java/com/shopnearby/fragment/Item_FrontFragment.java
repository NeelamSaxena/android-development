package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.adapter.ItemFront_Adapter;
import com.shopnearby.adapter.ItemsAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.custom.RecyclerItemClickListener;
import com.shopnearby.modal.ItemFrontModel;
import com.shopnearby.modal.ItemModal;
import com.shopnearby.modal.User;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.DividerItemDecoration;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.HomeActivity.CATEGORYNAME_LIST;
import static com.shopnearby.HomeActivity.STORE_LIST;
import static com.shopnearby.HomeActivity.sortPosition;
import static com.shopnearby.constant.App_Constant.FilterSort;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentRadius;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;

/**
 * Created by neelam on 16/6/16.
 */

public class Item_FrontFragment extends Fragment implements OnReceiveApiResult, SwipeRefreshLayout.OnRefreshListener{
    private String TAG = "Item_FrontFragment";
    private OnChangeFragment mChangeFragmentListener;
    GridLayoutManager mLayoutManager;
    private ItemFront_Adapter itemfront_adapter;
    private ArrayList<ItemFrontModel> mItemList;
    @InjectView(R.id.swipe_refresh_layout_current)
    SwipeRefreshLayout swipeRefreshLayout;
    private boolean loading = true;


    @InjectView(R.id.tv_noItems)
    TextView noItemsTextView;

    @InjectView(R.id.rv_items_front)
    RecyclerView mRecyclerViewItems;

    @InjectView(R.id.itemsll)
    TextView TxtItm_ll;

    @InjectView(R.id.imgSearch)
    ImageView mImgSearch;

    @InjectView(R.id.imgClear)
    ImageView mImgClear;

    @InjectView(R.id.et_search)
    EditText SearchEdittext;

    @OnClick(R.id.imgClear)
    public void clearSearchField() {
        SearchEdittext.setText("");
        mImgSearch.setVisibility(View.VISIBLE);
        SearchEdittext.setVisibility(View.GONE);
        TxtItm_ll.setVisibility(View.VISIBLE);
        mImgSearch.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.imgSearch)
    public void SearchFieldActive() {
        SearchEdittext.setText("");
        mImgSearch.setVisibility(View.GONE);
        SearchEdittext.setVisibility(View.VISIBLE);
        TxtItm_ll.setVisibility(View.GONE);
        mImgSearch.setVisibility(View.VISIBLE);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static Item_FrontFragment newInstance() {
        return new Item_FrontFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItemList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_front, container, false);
        App_Constant.currentCateogryIDFilter="";
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (App_Preference.newInstance(getActivity()).getUserData() != null) {
            User userModal = App_Preference.newInstance(getActivity()).getUserData();
            App_Constant.sUser=userModal.getStrUserId();
        }

        if (mItemList != null && mItemList.size() > 0) {
            setItemsAdapter();
        } else {
            loading = true;
            callProductFrontApi(true);
        }
    }
    private void callProductFrontApi(boolean show) {
        try {
            String str_request;
            //api/product/page
            Log.d(TAG, "currentBrandsFilter: " + currentBrandsFilter);
            Log.d(TAG, "currentRadius: " + currentRadius);
            Log.d(TAG, "App_Constant.sUser: " + App_Constant.sUser);
//            if (!currentBrandsFilter.equalsIgnoreCase("")) {
//                str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&radius=" + currentRadius + "&brand=" + currentBrandsFilter+App_Constant.currentSortFilter+"&min_price="+App_Constant.min_price+"&max_price"+App_Constant.max_price;
//            } else {
//                str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&radius=" + currentRadius + App_Constant.currentSortFilter+"&min_price="+App_Constant.min_price+"&max_price"+App_Constant.max_price;
//            }
            str_request = App_Constant.URL.PRODUCT_LIST_FrontAPI.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY+"&user="+App_Constant.sUser;
        //    str_request =str_request+"views=1";

            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.CHECK_EMAIL_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setItemsAdapter() {

        itemfront_adapter = new ItemFront_Adapter(mItemList);
        mLayoutManager = new GridLayoutManager(getActivity(),2);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewItems.setLayoutManager(mLayoutManager);
        mRecyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        if (getActivity() != null) {
            mRecyclerViewItems.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            mRecyclerViewItems.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL));
        }
             mRecyclerViewItems.setAdapter(itemfront_adapter);

        mRecyclerViewItems.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                ((HomeActivity)getActivity()).InitializeAllFilter();


                App_Constant.currentCateogryIDFilter=mItemList.get(position).getStrCategory();

                if(mItemList.get(position).getStrPageType().equalsIgnoreCase("Product")
                    ) {
                    App_Constant.currentItemHeader = mItemList.get(position).getStrName();
                    App_Constant.currentCateogryFilter=mItemList.get(position).getStrPageType();

                }else{
                    App_Constant.currentItemHeader="Items";
                }

                FilterSort="Best Rated";
                App_Constant.currentSortFilter = "&ratingSort=desc";
                sortPosition=0;
                App_Preference.newInstance(getActivity()).setSort(App_Constant.currentSortFilter,FilterSort,sortPosition);
                mChangeFragmentListener.onReplaceFragment(ItemFragment.newInstance(), "ItemFragment");
            }
        }));


        mRecyclerViewItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
//                    visibleItemCount = mLayoutManager.getChildCount();
//                    totalItemCount = mLayoutManager.getItemCount();
//                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                    if (totalItemCount > 20) {
//                        totalItemCount = totalItemCount - 10;
//                    }
//                    if (loading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false;
//                            callProductListApi(false);
//                        }
//                    }
                }
            }
        });
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        if (mItemList != null) {
            mItemList.clear();
        }
        callProductFrontApi(true);
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        mItemList = ResponseParser.getFrontItemsList(ApiResult);

        if (mItemList != null && mItemList.size() > 0) {
            setItemsAdapter();
        } else {
            if (mItemList.size() == 0) {
                noItemsTextView.setVisibility(View.VISIBLE);
                mRecyclerViewItems.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }
}
