package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.BrandAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Array_Constant;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentRadius;
import static com.shopnearby.constant.App_Constant.currentSortFilter;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;


/**
 * Created by Ankit on 8/6/16.
 */

public class BrandFilterFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "BrandFilterFragment";
    private SNB_Application mApplication;
    private OnChangeFragment mChangeFragmentListener;
    private List<BrandModal> brandNameList;
    private BrandAdapter brandAdapter;

    @InjectView(R.id.tv_noItems)
    TextView noItems;

    @InjectView(R.id.et_search)
    EditText SearchBrand;

    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        // mApplication.updatetextStatus=false;
        hideKeyboardByForce();
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @InjectView(R.id.lv_brands)
    ListView brandListview;

    @OnClick(R.id.tv_done)
    public void onDoneClick() {
        hideKeyboardByForce();
        Log.d(TAG, "currentCateogryIDFilter in Brand " + currentCateogryIDFilter);
        if (brandAdapter != null) {
            App_Array_Constant.Temp_BRANDNAME_LIST.clear();
            App_Array_Constant.Temp_BRANDNAME_LIST.addAll(brandAdapter.getSelectedBrands());
        }
//
//        App_Constant.currentBrandsFilter = "";
//        for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
//            if (App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected()) {
//                if (App_Constant.currentBrandsFilter.equalsIgnoreCase(""))
//                    App_Constant.currentBrandsFilter = App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
//                else
//                    App_Constant.currentBrandsFilter = App_Constant.currentBrandsFilter + "," + App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
//            }
//        }
//        mApplication.updatetextStatus=true;
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static BrandFilterFragment newInstance() {
        Bundle bundle = new Bundle();
//        bundle.putString("RESULT", total);
        BrandFilterFragment fragment = new BrandFilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        brandNameList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_brand_filter, container, false);
        mApplication=SNB_Application.getApplicationInstance(getActivity());
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (App_Array_Constant.Temp_BRANDNAME_LIST != null && App_Array_Constant.Temp_BRANDNAME_LIST.size() > 0) {
            for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                BrandModal modal = new BrandModal();
                modal.setBrandName(App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName());
                modal.setSelected(App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected());
                brandNameList.add(modal);
            }

            setBrandAdapter();
        } else {
            callBrandApi(true);
        }

        SearchBrand.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int start, int before,
                                      int count) {
             //   if (count > 0) {
                    try {
                        if (brandAdapter != null
                                && brandNameList.size() != 0) {

                            brandAdapter.getFilter().filter(cs);
                            Log.i("Changed", cs.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
             //   }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                try {
//                    if (brandAdapter != null
//                            && brandNameList.size() != 0) {
//
//                        brandAdapter.getFilter().filter(s);
//                        Log.i("Changed", s.toString());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        });
    }

    private void callBrandApi(boolean show) {
        try {
            String str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + App_Constant.currentRadius + App_Constant.currentSortFilter+  "&refine=brand";

            if (!App_Constant.min_price.equalsIgnoreCase("")) {
                str_request=str_request+"&min_price="+App_Constant.min_price.replace(",","").trim();
            }

            if (!App_Constant.max_price.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_price.toString().replace(",","").trim();
            }

            if (!currentCateogryIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&category="+currentCateogryIDFilter;;
            }

            if (!currentStoreIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&store="+currentStoreIDFilter;;
            }

            Log.d(TAG, "currentSortFilter: " +  currentSortFilter);
            Log.d(TAG, "currentBrandsFilter: " + currentBrandsFilter);
            Log.d(TAG, "currentRadius: " + currentRadius);
            Log.d(TAG, "currentStoreIDFilter: " + currentStoreIDFilter);
            Log.d(TAG, "currentCateogryIDFilter: " + currentCateogryIDFilter);
            Log.d(TAG, "App_Constant.max_price. " + App_Constant.max_price.toString());
            Log.d(TAG, "App_Constant.min_price " + App_Constant.min_price);


            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_LIST_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setBrandAdapter() {

        brandAdapter = new BrandAdapter(getActivity(), brandNameList);
        brandListview.setAdapter(brandAdapter);

    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        brandNameList = ResponseParser.getBrandNameList(ApiResult);
        if (brandNameList != null && brandNameList.size() > 0) {
            Collections.sort(brandNameList, new Comparator<BrandModal>() {
                @Override
                public int compare(BrandModal s1, BrandModal s2) {
                    return s1.getBrandName().toLowerCase().compareToIgnoreCase(s2.getBrandName().toLowerCase());
                }
            });
            noItems.setVisibility(View.GONE);
            brandListview.setVisibility(View.VISIBLE);
            setBrandAdapter();

        }else{
            noItems.setVisibility(View.VISIBLE);
            brandListview.setVisibility(View.GONE);
        }

    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }

    private void openKeyBoard() {
        SearchBrand.requestFocus();
        try {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideKeyboardByForce() {
        try {
            mApplication.hideSoftKeyboard(getActivity(), SearchBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
