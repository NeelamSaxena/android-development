package com.shopnearby.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;
import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.adapter.StoreAdapter;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.custom.RecyclerItemClickListener;
import com.shopnearby.modal.ItemDetailsModal;
import com.shopnearby.modal.StoreModal;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ankit on 18/5/16.
 */
public class SelectStoreFragment extends Fragment {

    private String TAG = "SelectStoreFragment";
    OnChangeFragment mChangeFragmentListener;
    LinearLayoutManager mlinearLayoutManager;
    private StoreAdapter storeAdapter;
    private GoogleMap map_view;
    private Handler mHandler;
    private SupportMapFragment MapFragment;
    ItemDetailsModal mDetailsModal;



    @InjectView(R.id.rv_store)
    RecyclerView mRecyclerViewStore;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static SelectStoreFragment newInstance(ItemDetailsModal modal) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("DETAIL", modal);
        SelectStoreFragment mFragment = new SelectStoreFragment();
        mFragment.setArguments(bundle);
        return mFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDetailsModal = getArguments().getParcelable("DETAIL");
        }
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        MapFragment = new SupportMapFragment();
//        FragmentTransaction transaction = getFragmentManager()
//                .beginTransaction();
//        transaction.replace(
//                R.id.map_frame, MapFragment, "MAP");
//        transaction.commitAllowingStateLoss();
//        mHandler.sendEmptyMessage(1);
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_store, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    showMap();
                }
            }
        };

        if (mDetailsModal != null && mDetailsModal.getmStoreList() != null) {
            setStoreAdapter();
        }
    }

    private void setMarkersOnMap() {
        if (map_view != null) {
            Log.d("Ankit", "setMarkersOnMap");
            map_view.clear();
            ArrayList<StoreModal> mStoreList = mDetailsModal.getmStoreList();
            for (int i = 0; i < mStoreList.size(); i++) {
                LatLng ll = new LatLng(mStoreList.get(i).getStoreLat(), mStoreList.get(i).getStoreLng());
//                map_view.addMarker(new MarkerOptions().position(ll).title("Store")
//                        .snippet("miles away").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                double dist = SphericalUtil.computeDistanceBetween(new LatLng(34.0500, -118.2500), ll);
                Log.d("Ankit", "dist: " + dist);


                TextView text = new TextView(getActivity());
                text.setText("Your text here");
                text.setTextColor(Color.GREEN);

                IconGenerator generator = new IconGenerator(getActivity());
                generator.setBackground(null);
                generator.setContentView(text);
                Bitmap icon = generator.makeIcon();

                MarkerOptions tp = new MarkerOptions().position(ll).icon(BitmapDescriptorFactory.fromBitmap(icon));
                map_view.addMarker(tp);

            }

        }

    }

    public void setStoreAdapter() {
        storeAdapter = new StoreAdapter(getActivity(), mDetailsModal.getmStoreList());
        mlinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewStore.setLayoutManager(mlinearLayoutManager);
        mRecyclerViewStore.setAdapter(storeAdapter);
        mRecyclerViewStore.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("Ankit", "On Item Click: " + position);
            }
        }));
        // attach top listener
//        mRecyclerViewStore.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                EventBus.getDefault().post(AttachUtil.isRecyclerViewAttach(recyclerView));
//            }
//        });
    }

    // Handle scroll event from fragments
    public void onEvent(Boolean b) {
        Log.d("Ankit", "onEvent: " + b);
//        dragLayout.setTouchMode(b);
    }

    @Override
    public void onResume() {
        super.onResume();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
//        EventBus.getDefault().unregister(this);
    }

    private void showMap() {

        map_view = MapFragment.getMap();
        if (map_view == null) {
            return;
        }
//        map_view.setMyLocationEnabled(false);
        map_view.getUiSettings().setZoomControlsEnabled(true);
        map_view.getUiSettings().setCompassEnabled(false);
        map_view.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map_view.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(34.0500, -118.2500), 14));
        map_view.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                Log.d("Ankit", "onMapLoaded");
                if (mDetailsModal != null && mDetailsModal.getmStoreList() != null) {
//                    setMarkersOnMap();
                } else {
                    Log.d("Ankit", "mDetailsModal Empty");
                }
            }
        });
    }

}
