package com.shopnearby.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import android.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;
import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.RecentLocationAdapter;
import com.shopnearby.adapter.RecentSearchAdapter;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.custom.RecyclerItemClickListener;
import com.shopnearby.location.LocationFragment;
import com.shopnearby.modal.Address_Modal;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.AddressJSONParser;
import com.shopnearby.utils.DividerItemDecoration;
import com.shopnearby.utils.Progress_HUD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by shailendra on 10/6/16.
 */

public class LocationFilterFragment extends Fragment  implements  LocationListener{



    private String TAG = "LocationFilterFragment";
    private SNB_Application mApplication;
    private OnChangeFragment mChangeFragmentListener;
    private LocationManager locationManager;
    private Handler mHandler;

    private String characterTyped = "";
    private boolean callAutocompleteApi;
    private GetLatLngFromAddress latlng_task;
    private String strAddress = "";
    private List<Address_Modal> searchList;
    private RecentLocationAdapter mRecentSearchAdapter;
    private LinearLayoutManager mLayoutManager;
    private boolean clearList;
    private int mPageCount = 1;
    private int mPageSize = 30;
    private String strSearchQuery = "";
    private int REQUEST_CODE = 100;

   // private android.location.LocationListener.LocationListener locationListener;

    @InjectView(R.id.et_location)
    PlacesAutocompleteTextView ET_Location;

    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        mApplication.updatetextStatus = false;
        hideKeyboardByForce();
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @InjectView(R.id.ll_recentView)
    LinearLayout LL_RecentView;

    @InjectView(R.id.rv_RecentItems)
    RecyclerView mRecyclerViewRecentSearch;

    @InjectView(R.id.imgClear)
    ImageView clearImageView;

    @OnClick(R.id.imgClear)
    public void clearSearchField() {
        ET_Location.setText("");
    }

    @InjectView(R.id.rb_current)
    CheckBox RB_Current;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static LocationFilterFragment newInstance() {
        Bundle bundle = new Bundle();
        LocationFilterFragment fragment = new LocationFilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMembers();

    }

    private void initMembers() {
        mHandler=new Handler();
        searchList = new ArrayList<>();
        if (App_Preference.newInstance(getActivity()).getSaveSearchedLocation() != null) {
            searchList = App_Preference.newInstance(getActivity()).getSaveSearchedLocation();
        }

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location_filter, container, false);
        mApplication = SNB_Application.getApplicationInstance(getActivity());
        ButterKnife.inject(this, view);
        SetFilter();
        if (searchList != null && searchList.size() > 0) {
            Collections.reverse(searchList);
            setRecentSearchAdapter();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ET_Location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && searchList != null && searchList.size() > 0) {

                    setRecentSearchAdapter();
                }
                if (ET_Location.getText().toString().length() > 0) {
                    clearImageView.setVisibility(View.VISIBLE);
                } else {
                    clearImageView.setVisibility(View.GONE);

                }

            }
        });


        ET_Location.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(final Place place) {
                ET_Location.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(final PlaceDetails details) {
//                        ET_Location.setSelection(0);
                        mApplication.hideSoftKeyboard(getActivity(), ET_Location);
                        if (latlng_task != null) {
                            latlng_task.cancel(true);
                        }

                        strAddress = details.formatted_address;
                        latlng_task = new GetLatLngFromAddress();
                        latlng_task.execute(details.place_id);
                    }

                    @Override
                    public void onFailure(final Throwable failure) {
                        Log.d("test", "failure " + failure);
                        mApplication.ShowAlert(getActivity(), getString(R.string.ERROR_InternetProblem),getActivity().getResources().getString(R.string.app_name));
                    }
                });
            }
        });


        RB_Current.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                  @Override
                                                  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                      if (isChecked == true)
                                                          checkForPermission();
                                                  }
                                              }
        );

    }

    private void SetFilter(){

        String LocationName="";
        if (mApplication.TempAddress.toString().length() >0 && !mApplication.TempAddress.equalsIgnoreCase("")) {

            LocationName=mApplication.TempAddress;

        } else if (App_Constant.sCurrentAddress != null) {

            LocationName=App_Constant.sCurrentAddress;
        }
        Log.d(TAG, "LocationName " + LocationName);
        if(LocationName.equalsIgnoreCase(getActivity().getResources().getString(R.string.Tag_CurrentLocation))){
            RB_Current.setChecked(true);
        }else{
            RB_Current.setChecked(false);


        }

        if(searchList!=null && searchList.size()>0){
            for (int i=0;i<searchList.size();i++){
                searchList.get(i).setSelected(false);
            }

        }
        if(RB_Current.isChecked()==false){
            int checkedPos=-1;
            if(searchList!=null && searchList.size()>0){
                for (int i=0;i<searchList.size();i++){
                    if(searchList.get(i).getStr_Street_text().equalsIgnoreCase(LocationName)){
                        checkedPos=i;
                        break;
                    }
                }
                if(checkedPos!=-1){
                    searchList.get(checkedPos).setSelected(true);
                }
            }
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private class GetLatLngFromAddress extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";
            Log.d(TAG, "GetLatLngFromAddress doing bg");
            String input = "";
            try {
                input = URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                Log.d(TAG, "error= " + e1.toString());
                e1.printStackTrace();
            }

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + input + "&key=" + App_Constant.API_KEY;

            Log.d(TAG, "url of near= " + url);

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parseAddressResponse(result);
        }
    }

    private void parseAddressResponse(String result) {

        AddressJSONParser addressParser = new AddressJSONParser();
        Address_Modal mAddressModal = addressParser.parse(result);
        if (mAddressModal != null) {

            //  App_Constant.sCurrentLat = Double.parseDouble(mAddressModal.getStr_lat());
            //  App_Constant.sCurrentLng = Double.parseDouble(mAddressModal.getStr_lng());
//            Log.d(TAG, "Location Lat :" + mAddressModal.getStr_lat());
//            Log.d(TAG, "Location Lat :" + mAddressModal.getStr_lng());
       /*     App_Preference.newInstance(getActivity()).setLocation(mAddressModal.getStr_lat(), mAddressModal.getStr_lng(), strAddress);
            App_Constant.sCurrentLat = Double.parseDouble(App_Preference.newInstance(getActivity()).getLatitude());
            App_Constant.sCurrentLng = Double.parseDouble(App_Preference.newInstance(getActivity()).getLongitude());
            App_Constant.sCurrentAddress = App_Preference.newInstance(getActivity()).getAddress();*/
            mApplication.TempLat = mAddressModal.getStr_lat();
            mApplication.TempLong = mAddressModal.getStr_lng();
            mApplication.TempAddress = strAddress;
            mAddressModal.setStr_Street_text(strAddress);
            if(searchList.size()>0){
                    for (int i=0;i<searchList.size();i++){
                        searchList.get(i).setSelected(false);
                    }


            }

            for (int i=0;i<searchList.size();i++){
                Log.d(TAG, "IS SELECTED:" + searchList.get(i).isSelected());
            }


            mAddressModal.setSelected(true);


            saveQuery(mAddressModal);

            getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {
            mApplication.ShowAlert(getActivity(), getResources().getString(R.string.Error_NoAddress),getActivity().getResources().getString(R.string.app_name));
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            Log.d(TAG, "data= " + data);

            br.close();

        } catch (Exception e) {

            Log.d(TAG, e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void saveQuery(Address_Modal SearchQuery) {
        if (searchList == null)
            searchList = new ArrayList<Address_Modal>();

        for (Address_Modal s : searchList) {

            if (s.getStr_lat().equalsIgnoreCase(SearchQuery.getStr_lat())) { // Or use an accessor function for `nomeParagem` if appropriate
                searchList.remove(s);
                break;
            }

           // searchList.s
        }
        searchList.add(SearchQuery);

        App_Preference.newInstance(getActivity()).saveSearchedLocation(searchList);
    }

    public void setRecentSearchAdapter() {
//        clearImageView.setVisibility(View.VISIBLE);
        LL_RecentView.setVisibility(View.VISIBLE);

        mRecentSearchAdapter = new RecentLocationAdapter(searchList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerViewRecentSearch.setLayoutManager(mLayoutManager);
        mRecyclerViewRecentSearch.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewRecentSearch.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewRecentSearch.setAdapter(mRecentSearchAdapter);
        mRecyclerViewRecentSearch.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Address_Modal mAddressModal = searchList.get(position);
                if (mAddressModal != null) {

                    //  App_Constant.sCurrentLat = Double.parseDouble(mAddressModal.getStr_lat());
                    //  App_Constant.sCurrentLng = Double.parseDouble(mAddressModal.getStr_lng());
                    Log.d(TAG, "Location Lat :" + mAddressModal.getStr_lat());
                    Log.d(TAG, "Location Long :" + mAddressModal.getStr_lng());
                    Log.d(TAG, "Location Street :" + mAddressModal.getStr_Street_text());
       /*     App_Preference.newInstance(getActivity()).setLocation(mAddressModal.getStr_lat(), mAddressModal.getStr_lng(), strAddress);
            App_Constant.sCurrentLat = Double.parseDouble(App_Preference.newInstance(getActivity()).getLatitude());
            App_Constant.sCurrentLng = Double.parseDouble(App_Preference.newInstance(getActivity()).getLongitude());
            App_Constant.sCurrentAddress = App_Preference.newInstance(getActivity()).getAddress();*/
                    mApplication.TempLat = mAddressModal.getStr_lat();
                    mApplication.TempLong = mAddressModal.getStr_lng();
                    mApplication.TempAddress = mAddressModal.getStr_Street_text();

                    //  saveQuery(mAddressModal);

                    getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                }
            }
        }));
    }

    private void openKeyBoard() {
        ET_Location.requestFocus();
        try {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideKeyboardByForce() {
        try {
            mApplication.hideSoftKeyboard(getActivity(), ET_Location);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static Criteria getFineCriteria() {
        Criteria fine = new Criteria();
        fine.setAccuracy(Criteria.ACCURACY_FINE);
        fine.setAltitudeRequired(false);
        fine.setBearingRequired(false);
        fine.setSpeedRequired(true);
        fine.setCostAllowed(true);
        return fine;
    }


    private void checkForPermission() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for lollipop and newer versions
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "permissionCheck granted");
                checkForGps();
            } else {
                Log.d(TAG, "permissionCheck denied");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        App_Constant.MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            checkForGps();
        }
    }


    public void checkForGps() {
        Log.d(TAG, "checkForGps");
        locationManager = (LocationManager) getActivity()
                .getSystemService(getActivity().LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGPSEnabled) {
            Log.d(TAG, "GPS IS OFF");
            RB_Current.setChecked(false);
            ShowAlert(getActivity());
        } else {
            requestLocation();
        }
    }

    public void requestLocation() {
        Log.d(TAG, "GPS IS ON");
        try {
            if (((HomeActivity)getActivity()).mCurrentLocation != null) {
              //LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if( ((HomeActivity)getActivity()).mCurrentLocation != null) {
//                App_Constant.sCurrentLat = mLastLocation.getLatitude();
//                App_Constant.sCurrentLng = mLastLocation.getLongitude();
//                App_Constant.sCurrentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    Log.d("Ankit", "Latitude== " + String.valueOf(((HomeActivity)getActivity()).mCurrentLocation.getLatitude()));
                    Log.d("Ankit", "Longitude== " + String.valueOf(((HomeActivity)getActivity()).mCurrentLocation.getLongitude()));
                    getAddressFromLatLng();

                    mApplication.TempLat = String.valueOf(((HomeActivity)getActivity()).mCurrentLocation.getLatitude());
                    mApplication.TempLong = String.valueOf(((HomeActivity)getActivity()).mCurrentLocation.getLongitude());
                    mApplication.TempAddress = "Current Location";

                    //  saveQuery(mAddressModal);

                    getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);

//                App_Preference.newInstance(getActivity()).setLocation(String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()), strAddress);
//                if (mChangeFragmentListener != null) {
//                    mChangeFragmentListener.onReplaceFragment(LoginOptionFragment.newInstance(), TAG);
//                }

                } else {

                    Log.d("Ankit", "NO location found...............");

                }
            }else{
                Log.d("Ankit", "NO location found. custom..............");
            }

        } catch (Exception e) {
            Log.d("Ankit", "Current Location =0,0");
            e.printStackTrace();
        }

    }


    private void getAddressFromLatLng() {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            addresses = geocoder.getFromLocation(App_Constant.sCurrentLat, App_Constant.sCurrentLat, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            Log.d("Ankit", "city: " + city);
            Log.d("Ankit", "state: " + state);
            Log.d("Ankit", "country: " + country);

            if (city == null || city.equalsIgnoreCase("null")) {
                city = "";
            }
            if (state == null || !state.equalsIgnoreCase("null")) {
                state = "";
            } else {
                state = "," + state;
            }
            strAddress = city + state;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ShowAlert(Context mContext) {

        AlertDialog.Builder mAlert = new AlertDialog.Builder(mContext);
        mAlert.setCancelable(false);
        mAlert.setTitle(mContext.getResources().getString(R.string.app_name));
        mAlert.setMessage(mContext.getResources().getString(R.string.GPS_Message));

        mAlert.setPositiveButton(mContext.getResources().getString(R.string.Tag_Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
                dialog.dismiss();
            }
        });
        mAlert.setNegativeButton(mContext.getResources().getString(R.string.Tag_Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mAlert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (REQUEST_CODE == requestCode) {
            Log.d(TAG, "GPS IS On Now");
            Progress_HUD.show(getActivity(), "Loading...");
            ((HomeActivity)getActivity()).updateLocation();

//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    requestLocation();
//                }
//            }, 10000);

        }
    }




}
