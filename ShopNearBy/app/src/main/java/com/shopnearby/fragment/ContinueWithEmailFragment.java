package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.utils.EmailValidator;
import com.shopnearby.utils.ResponseParser;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 12/5/16.
 */
public class ContinueWithEmailFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "ContinueWithEmailFragment";
    OnChangeFragment mChangeFragmentListener;
    private String strEmail = "";

    @InjectView(R.id.tv_pageTitle)
    TextView mPageTitleTextView;

    @InjectView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;

    @InjectView(R.id.input_email)
    EditText mEmailEditText;

    @InjectView(R.id.btn_next)
    Button mNextButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    @OnClick(R.id.img_back)
    public void goBack() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btn_next)
    public void onNextClick() {
        callCheckEmailApi();

    }

    public static ContinueWithEmailFragment newInstance() {
        return new ContinueWithEmailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_continue_with_email, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPageTitleTextView.setText(getString(R.string.Tag_ContinueEmail));
        mNextButton.setEnabled(false);
        mEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (validateEmail()) {
                    mNextButton.setEnabled(true);
                    mNextButton.setAlpha(1f);
                } else {
                    mNextButton.setEnabled(false);
                    mNextButton.setAlpha(0.6f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private boolean validateEmail() {
        strEmail = mEmailEditText.getText().toString().trim();
        if (strEmail.isEmpty() || !isValidEmail(strEmail)) {
            inputLayoutEmail.setError(getString(R.string.Tag_InvalidEmail));
            requestFocus(mEmailEditText);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }


    private void callCheckEmailApi() {
        try {
            String str_request = App_Constant.URL.CHECK_EMAIL_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&email=" + strEmail;
            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.CHECK_EMAIL_API, "Loading...", str_request, true);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return EmailValidator.ValidateEmail(email);
//        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        if (ResponseParser.getDataStatus(ApiResult)) {
            //user exist
            if (mChangeFragmentListener != null) {
                mChangeFragmentListener.onReplaceFragment(ForgotPasswordFragment.newInstance(strEmail), TAG);
            }
        } else {
            //user does not exist
            if (mChangeFragmentListener != null) {
                mChangeFragmentListener.onReplaceFragment(AccountDetailFragment.newInstance(strEmail), TAG);
            }
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }
}
