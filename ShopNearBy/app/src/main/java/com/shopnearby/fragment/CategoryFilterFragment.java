package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.BrandAdapter;
import com.shopnearby.adapter.CategoryAdapter;
import com.shopnearby.adapter.ExpandableListAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Array_Constant;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.HomeActivity.CATEGORYNAME_LIST;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentRadius;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;

/**
 * Created by shailendra on 8/6/16.
 */

public class CategoryFilterFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "CategoryFilterFragment";
    private OnChangeFragment mChangeFragmentListener;
    private List<SearchCategoryModal> categoryNameList;
    private CategoryAdapter catAdapter;
    private SNB_Application mApplication;


    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        // mApplication.updatetextStatus=false;
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @InjectView(R.id.lv_category)
    ListView CategoryListview;


    @InjectView(R.id.tv_noItems)
    TextView NoItems;

    @OnClick(R.id.tv_done)
    public void onDoneClick() {

        if (catAdapter != null) {
            App_Array_Constant.Temp_Category_LIST.clear();
            Log.d(TAG, "CATEGORY SIZE of ADAPter" + catAdapter.getSelectedCategory());
            App_Array_Constant.Temp_Category_LIST.addAll(catAdapter.getSelectedCategory());
        }
        Log.d(TAG, "CATEGORYNAME_LIST Size" + CATEGORYNAME_LIST.size());
//        App_Constant.currentCateogryFilter = "";
//        for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
//            if (App_Array_Constant.Temp_Category_LIST.get(i).isSelected()) {
//                if (App_Constant.currentCateogryFilter.equalsIgnoreCase("")) {
//                    App_Constant.currentCateogryFilter = App_Array_Constant.Temp_Category_LIST.get(i).getName();
//                    App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
//                }
//                else {
//                    App_Constant.currentCateogryFilter = App_Constant.currentCateogryFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getName();
//                    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
//                }
//              }
//            Log.d(TAG, "Parent " + App_Constant.currentCateogryFilter);
//            //else{
//                for (int j=0;j<App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size();j++){
//                    if (App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).isSelected()) {
//                        if (App_Constant.currentCateogryFilter.equalsIgnoreCase("")) {
//                            App_Constant.currentCateogryFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
//                            App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
//                        } else {
//                            App_Constant.currentCateogryFilter = App_Constant.currentCateogryFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
//                            App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
//                        }
//                    }
//                    Log.d(TAG, "Child " + App_Constant.currentCateogryFilter);
//                }
//            //}
//        }

        Log.d(TAG, "App_Constant.currentCateogryFilter:" + App_Constant.currentCateogryFilter);
        Log.d(TAG, "App_Constant.currentCateogryIDFilter:" + App_Constant.currentCateogryIDFilter);
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static CategoryFilterFragment newInstance() {
        Bundle bundle = new Bundle();
//        bundle.putString("RESULT", total);
        CategoryFilterFragment fragment = new CategoryFilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryNameList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_filter, container, false);
        ButterKnife.inject(this, view);



        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (App_Array_Constant.Temp_Category_LIST != null && App_Array_Constant.Temp_Category_LIST.size() > 0) {
            for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                SearchCategoryModal modal = new SearchCategoryModal();
                modal.setName(App_Array_Constant.Temp_Category_LIST.get(i).getName());
                modal.setId(App_Array_Constant.Temp_Category_LIST.get(i).getId());
                modal.setParent(App_Array_Constant.Temp_Category_LIST.get(i).getParent());
                modal.setSelected(App_Array_Constant.Temp_Category_LIST.get(i).isSelected());



                if(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size()>0){
                    ArrayList<SearchCategoryModal> ChildArray=new ArrayList<SearchCategoryModal>();

                    for (int k=0;k<App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size();k++){
                        SearchCategoryModal Cmodal = new SearchCategoryModal();
                        Cmodal.setName(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).getName());
                        Cmodal.setId(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).getId());
                        Cmodal.setParent(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).getParent());
                        Cmodal.setSelected(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).isSelected());
                        if(App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).isSelected()==true)
                            Log.d(TAG, "Child SELECTED  " + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(k).isSelected());

                        ChildArray.add(Cmodal);
                    }
                    modal.setChildArray(ChildArray);

                }

                categoryNameList.add(modal);
            }

            setCategoryAdapter();
        } else {
            callCategoryApi(true);
        }

    }

    private void callCategoryApi(boolean show) {
        try {
            String str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + currentRadius+ App_Constant.currentSortFilter+"&refine=category";

            if (!App_Constant.min_price.equalsIgnoreCase("")) {
                str_request=str_request+"&min_price="+App_Constant.min_price.replace(",","").trim();
            }

            if (!App_Constant.max_price.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_price.toString().replace(",","").trim();
            }

            if (!currentBrandsFilter.equalsIgnoreCase("")) {
                str_request=str_request+"&brand="+currentBrandsFilter;
            }



            if (!currentStoreIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&store="+currentStoreIDFilter;;
            }


            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_LIST_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setCategoryAdapter() {
        catAdapter = new CategoryAdapter(getActivity(), categoryNameList);
        CategoryListview.setAdapter(catAdapter);

    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        categoryNameList = ResponseParser.getCategoryList(ApiResult);
        if (categoryNameList != null && categoryNameList.size() > 0) {

            Collections.sort(categoryNameList, new Comparator<SearchCategoryModal>() {
                @Override
                public int compare(SearchCategoryModal s1, SearchCategoryModal s2) {
                    return s1.getName().toLowerCase().compareToIgnoreCase(s2.getName().toLowerCase());
                }
            });

            NoItems.setVisibility(View.GONE);
            CategoryListview.setVisibility(View.VISIBLE);
            setCategoryAdapter();
        }else{

            NoItems.setVisibility(View.VISIBLE);
            CategoryListview.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }
}
