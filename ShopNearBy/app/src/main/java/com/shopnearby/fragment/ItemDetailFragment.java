package com.shopnearby.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.ViewPagerAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.databinding.ClosestLayoutBinding;
import com.shopnearby.databinding.FragmentItemDetailsBinding;
import com.shopnearby.modal.ItemDetailsModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.ResponseParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 18/5/16.
 */
public class ItemDetailFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "ItemDetailFragment";
    private String itemName = "";
    private String itemImageUrl = "";
    private String dayOfTheWeek = "";
    private String ProductId = "";
    private int ProductCount = 0;
    private String UserId = "";
    private OnChangeFragment mChangeFragmentListener;
    private SNB_Application mApplication;
    private FragmentItemDetailsBinding binding;
    ItemDetailsModal mDetailModal;
    ArrayList<StoreModal> ClosestStoreList;
    ArrayList<StoreModal> LowestStoreList;
    private BottomSheetDialog BottomDialog;
    static final int MY_PERMISSIONS_REQUEST_CALL = 11;
    private String MobileNumber = "";
    private StoreModal storeModal;
    private LatLng currentLatLng;
    private LayoutInflater inflater;

    @InjectView(R.id.tab_layout)
    TabLayout mTabLayout;

    @InjectView(R.id.viewpager)
    ViewPager mViewPager;

    @InjectView(R.id.ll_tabLayout)
    LinearLayout LL_TabLayouts;

    @InjectView(R.id.ll_SingleStore)
    LinearLayout LL_SingleStore;

    @InjectView(R.id.tv_storeCount)
    TextView StoreCountTextview;

//    @InjectView(R.id.tv_storeName)
//    TextView storeTitleText;
//    @InjectView(R.id.tv_itemPrice)
//    TextView storePriceText;
//    @InjectView(R.id.tv_availability)
//    TextView storeAvailabilityText;
//    @InjectView(R.id.tv_storeTiming)
//    TextView storeTimingText;
//    @InjectView(R.id.tv_storeDistance)
//    TextView storeDistanceText;
//    @InjectView(R.id.tv_updateTime)
//    TextView storeUpdateTimeText;

    //Action Buttons
//    @InjectView(R.id.tv_add)
//    TextView addTextView;
//    @InjectView(R.id.tv_Direction)
//    TextView directionTextView;
//    @InjectView(R.id.tv_Share)
//    TextView shareTextView;

//    @OnClick(R.id.tv_Call)
//    public void onStoreCallClick() {
//        if (storeModal != null && !storeModal.getPhoneNumber().equalsIgnoreCase(""))
//            makeAcall(storeModal.getPhoneNumber());
//    }
//
//    @OnClick(R.id.tv_Share)
//    public void onStoreShareClick() {
//        if (storeModal != null)
//            shareToOtherApps(storeModal);
//    }
//
//    @OnClick(R.id.tv_More)
//    public void onStoreMoreClick() {
//        if (storeModal != null)
//            openBottomSheetDialog(storeModal);
//    }


    @OnClick(R.id.tv_MapView)
    public void goToMapView() {
        if (mViewPager != null && mViewPager.getCurrentItem() == 0)
            mChangeFragmentListener.onReplaceFragment(MapView_Fragment.newInstance(ClosestStoreList), TAG);
        else if (mViewPager != null && mViewPager.getCurrentItem() == 1)
            mChangeFragmentListener.onReplaceFragment(MapView_Fragment.newInstance(LowestStoreList), TAG);
        else
            mChangeFragmentListener.onReplaceFragment(MapView_Fragment.newInstance(ClosestStoreList), TAG);

    }

    @OnClick(R.id.tv_specifications)
    public void goToDetailsPage() {
        if (mDetailModal.getmDetailList() != null)
            mChangeFragmentListener.onReplaceFragment(DetailSpecificationsFragment.newInstance(mDetailModal.getmDetailList()), TAG);
    }

    @OnClick(R.id.rl_reviews)
    public void goToReviewsPage() {
        mChangeFragmentListener.onReplaceFragment(ReviewFragment.newInstance(ProductId), TAG);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static ItemDetailFragment newInstance(String id, int count) {
        Bundle args = new Bundle();
        args.putString("ID", id);
        args.putInt("COUNT", count);
        ItemDetailFragment mFragment = new ItemDetailFragment();
        mFragment.setArguments(args);
        return mFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMembers();
    }

    private void initMembers() {
        if (App_Preference.newInstance(getActivity()).getUserData() != null) {
            UserId = App_Preference.newInstance(getActivity()).getUserData().getStrUserId();
        }

        mApplication = (SNB_Application) getActivity().getApplicationContext();
        if (getArguments() != null) {
            ProductId = getArguments().getString("ID");
            ProductCount = getArguments().getInt("COUNT");
        }

        ClosestStoreList = new ArrayList<>();
        LowestStoreList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_details, container, false);
        ButterKnife.inject(this, binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currentLatLng = new LatLng(App_Constant.sCurrentLat, App_Constant.sCurrentLng);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        if (mDetailModal == null) {
            callProductDetailsApi();
        } else {

            setDataInView(false);
            binding.setItemDetailsVM(mDetailModal);

            Log.d("Ankit", "StoreList size: " + mDetailModal.getmStoreList().size());
        }

    }

    private void callProductDetailsApi() {
        try {
            String str_request;
            if (UserId.equalsIgnoreCase(""))
                str_request = App_Constant.URL.PRODUCT_DETAILS_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&id=" + ProductId + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + App_Constant.currentRadius + "&count=" + ProductCount;
            else
                str_request = App_Constant.URL.PRODUCT_DETAILS_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&id=" + ProductId + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + App_Constant.currentRadius + "&count=" + ProductCount + "&user=" + UserId;

            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_DETAILS_API, "Loading...", str_request, true);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(ClosestFragment.newInstance(ClosestStoreList, mDetailModal.getStrTitle(), mDetailModal.getStrImage()), getString(R.string.Tag_Closest));
        adapter.addFragment(LowestFragment.newInstance(LowestStoreList, mDetailModal.getStrTitle(), mDetailModal.getStrImage()), getString(R.string.Tag_LowestPrice));
        viewPager.setAdapter(adapter);
    }

    private void sortStoreList() {
        for (int i = 0; i < ClosestStoreList.size(); i++) {
            LatLng ll = new LatLng(ClosestStoreList.get(i).getStoreLat(), ClosestStoreList.get(i).getStoreLng());
            double dist = SphericalUtil.computeDistanceBetween(currentLatLng, ll);
            dist = dist * 0.000621371;
            dist = mApplication.round(dist, 2);
            ClosestStoreList.get(i).setDistance(dist);
            LowestStoreList.get(i).setDistance(dist);

        }

        Collections.sort(ClosestStoreList, new Comparator<StoreModal>() {
            public int compare(StoreModal acc1, StoreModal acc2) {
                return Double.compare(acc1.getDistance(), acc2.getDistance());
            }
        });

        Collections.sort(LowestStoreList, new Comparator<StoreModal>() {
            public int compare(StoreModal acc1, StoreModal acc2) {
                return Double.compare(Double.parseDouble(acc1.getPrice()), Double.parseDouble(acc2.getPrice()));
//                return acc2.getPrice().compareTo(acc1.getPrice());
            }
        });
    }


    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        mDetailModal = ResponseParser.getItemDetails(ApiResult, dayOfTheWeek.toLowerCase());
        setDataInView(true);
        binding.setItemDetailsVM(mDetailModal);
    }

    private void setDataInView(boolean doSort) {
        if (mDetailModal != null) {
            itemImageUrl = mDetailModal.getStrImage();
            itemName = mDetailModal.getStrTitle();
            if (doSort) {
                ClosestStoreList = mDetailModal.getmStoreList();
                LowestStoreList = cloneList(ClosestStoreList);
                changeDateFormat();
                sortStoreList();
            }

            Log.d("Ankit", "StoreList size: " + mDetailModal.getmStoreList().size());
            if (compareAllPrice()) {
                Log.d("Ankit", "compareAllPrice: true");
                addChildViews();
            } else {
                Log.d("Ankit", "compareAllPrice: false");
                if (mDetailModal.getmStoreList().size() > 1) {
                    LL_TabLayouts.setVisibility(View.VISIBLE);
                    setupViewPager(mViewPager);
                    mTabLayout.setupWithViewPager(mViewPager);
                    mTabLayout.setTabMode(TabLayout.MODE_FIXED);
                    mTabLayout.setTabGravity(0);
                    StoreCountTextview.setText("Stores (" + mDetailModal.getmStoreList().size() + ")");
                } else if (mDetailModal.getmStoreList().size() == 1) {
                    Log.d("Ankit", "Add stores");
                    addChildViews();
//                    LL_SingleStore.setVisibility(View.VISIBLE);
//                    StoreCountTextview.setText("Store");
//                    storeModal = mDetailModal.getmStoreList().get(0);
//                    storeTitleText.setText(storeModal.getRetailerName() + " " + storeModal.getStoreName());
//                    storePriceText.setText("$" + storeModal.getPrice());
//
//                    Log.d("Ankit", "getPrice: " + storeModal.getPrice());
//                    Log.d("Ankit", "getDistance: " + storeModal.getDistance());
//                    Log.d("Ankit", "getUpdateDate: " + storeModal.getUpdateDate());
//                    Log.d("Ankit", "storeModal: " + storeModal.isInStoreAvailability());
//                    Log.d("Ankit", "getOpenTime: " + storeModal.getOpenTime());
//                    if (storeModal.isInStoreAvailability()) {
//                        storeAvailabilityText.setText("Buy & pickup in store");
//                        addTextView.setVisibility(View.VISIBLE);
//                        shareTextView.setVisibility(View.GONE);
//                    } else {
//                        storeAvailabilityText.setText("In-store only");
//                        addTextView.setVisibility(View.GONE);
//                        shareTextView.setVisibility(View.VISIBLE);
//                    }
//                    storeTimingText.setText("Open, " + storeModal.getOpenTime().toLowerCase() + " - " + storeModal.getCloseTime().toLowerCase());
//                    storeDistanceText.setText(storeModal.getDistance() + " miles away");
//                    storeUpdateTimeText.setText(storeModal.getUpdateDate());
                }
            }


        }
    }

    private void addChildViews() {
        LL_SingleStore.setVisibility(View.VISIBLE);
        LL_SingleStore.removeAllViews();
        if (ClosestStoreList.size() == 1)
            StoreCountTextview.setText("Store");
        else
            StoreCountTextview.setText("Stores (" + ClosestStoreList.size() + ")");

        for (int i = 0; i < ClosestStoreList.size(); i++) {
            StoreModal mModal = ClosestStoreList.get(i);
            inflater = getActivity().getLayoutInflater();

            ClosestLayoutBinding childBinding = DataBindingUtil.inflate(inflater, R.layout.closest_layout, (ViewGroup) binding.getRoot().getParent(), false);

            final View child_view = childBinding.getRoot();
            childBinding.setStoreVM(mModal);

            TextView txtTitle = (TextView) child_view.findViewById(R.id.tv_title);

            //Action Buttons
            TextView txt_Add = (TextView) child_view.findViewById(R.id.tv_add);
            TextView txt_Direction = (TextView) child_view.findViewById(R.id.tv_Direction);
            TextView txt_Call = (TextView) child_view.findViewById(R.id.tv_Call);
            TextView txt_Share = (TextView) child_view.findViewById(R.id.tv_Share);
            TextView txt_More = (TextView) child_view.findViewById(R.id.tv_More);
            txt_More.setTag(mModal);
            txt_Share.setTag(mModal);
            txt_Call.setTag(mModal);
            txt_More.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openBottomSheetDialog((StoreModal) view.getTag());
                }
            });
            txt_Share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareToOtherApps((StoreModal) view.getTag());
                }
            });
            txt_Call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MobileNumber = ((StoreModal) view.getTag()).getPhoneNumber();
                    makeAcall(MobileNumber);
                }
            });

            txtTitle.setText((i + 1) + ". " + mModal.getRetailerName() + " " + mModal.getStoreName());

            LL_SingleStore.addView(child_view);
        }
    }

    private void changeDateFormat() {
        for (int i = 0; i < ClosestStoreList.size(); i++) {
            String updateTime = mApplication.ChangeDateFormat(ClosestStoreList.get(i).getUpdateDate(), "MM/dd/yyyy hh:mm aa");
            String[] splited = updateTime.split("\\s+");
            updateTime = "Updated at " + splited[1] + " " + splited[2].toLowerCase() + ", " + splited[0];
            ClosestStoreList.get(i).setUpdateDate(updateTime);
            LowestStoreList.get(i).setUpdateDate(updateTime);
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }

    private void openBottomSheetDialog(final StoreModal modal) {
        BottomDialog = new BottomSheetDialog(getActivity());
        BottomDialog.setContentView(R.layout.bottom_sheet_layout);
        TextView Txt_shareMenu = (TextView) BottomDialog.findViewById(R.id.tv_ShareBS);
        TextView Txt_mapView = (TextView) BottomDialog.findViewById(R.id.tv_MapViewBS);
        Txt_mapView.setVisibility(View.VISIBLE);
        if (!modal.isInStoreAvailability()) {
            Txt_shareMenu.setVisibility(View.GONE);
        }
        Txt_shareMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareToOtherApps(modal);
            }
        });

        Txt_mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<StoreModal> mlist = new ArrayList<>();
                mlist.add(modal);
                BottomDialog.dismiss();
                mChangeFragmentListener.onReplaceFragment(MapView_Fragment.newInstance(mlist), TAG);
            }
        });

        BottomDialog.show();
    }

    public void makeAcall(String number) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                startActivity(callIntent);
            } else {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL);
            }
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! do the
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + MobileNumber));
                    startActivity(callIntent);
                }
            }
        }
    }

    private void shareToOtherApps(StoreModal modal) {
        String shareText = itemName + "\nIt's available at " + modal.getRetailerName() + " " + modal.getStoreName() + " for $" + modal.getPrice() + ". The store address is " + modal.getAddress() + " " + modal.getCity() + " " + modal.getPostalCode();
        Intent shareIntent = new Intent();
        Uri pictureUri = Uri.parse(itemImageUrl);
        Log.d("Ankit", "ImgUrl: " + pictureUri);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share product"));
    }

    private boolean compareAllPrice() {
        String firstPrice = mDetailModal.getmStoreList().get(0).getPrice();
        for (int i = 0; i < mDetailModal.getmStoreList().size(); i++) {
            if (!firstPrice.equalsIgnoreCase(mDetailModal.getmStoreList().get(i).getPrice())) {
                return false;
            }
        }
        return true;
    }

    public static ArrayList<StoreModal> cloneList(ArrayList<StoreModal> list) {
        ArrayList<StoreModal> clone = new ArrayList<StoreModal>(list.size());
        for(StoreModal item: list) clone.add(item.clone());
        return clone;
    }

}