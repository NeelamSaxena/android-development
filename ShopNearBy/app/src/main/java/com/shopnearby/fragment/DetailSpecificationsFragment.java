package com.shopnearby.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.DetailSpecAdapter;
import com.shopnearby.modal.SpecificationModal;
import com.shopnearby.utils.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ankit on 3/6/16.
 */
public class DetailSpecificationsFragment extends Fragment {

    private String TAG = "ReviewFragment";
    private String productId = "";
    private LinearLayoutManager mLayoutManager;
    private SNB_Application mApplication;
    private ArrayList<SpecificationModal> mDetailsList;
    private DetailSpecAdapter mDetailsAdapter;

    @InjectView(R.id.rv_details)
    RecyclerView mRecyclerViewDetails;


    public static DetailSpecificationsFragment newInstance(ArrayList<SpecificationModal> detailList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("DETAIL_LIST", detailList);
        DetailSpecificationsFragment mFragment = new DetailSpecificationsFragment();
        mFragment.setArguments(bundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (SNB_Application) getActivity().getApplicationContext();
        if (getArguments() != null) {
            mDetailsList = getArguments().getParcelableArrayList("DETAIL_LIST");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_specification, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSpecificationAdapter();
    }

    private void setSpecificationAdapter() {
        mDetailsAdapter = new DetailSpecAdapter(mDetailsList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewDetails.setLayoutManager(mLayoutManager);
        mRecyclerViewDetails.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewDetails.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewDetails.setAdapter(mDetailsAdapter);
    }
}
