package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.distancePosition;
import static com.shopnearby.HomeActivity.sortPosition;

/**
 * Created by shailendra on 10/6/16.
 */

public class DistanceFilterFragment extends Fragment {
    private String TAG = "DistanceFilterFragment";
    private OnChangeFragment mChangeFragmentListener;
    private SNB_Application mApplication;


    @InjectView(R.id.rd__5miles)
    RadioButton RD_5miles;

    @InjectView(R.id.rd_20miles)
    RadioButton RD_20miles;

    @InjectView(R.id.rd_50miles)
    RadioButton RD_50miles;

    @InjectView(R.id.rd_100miles)
    RadioButton RD_100miles;

    @InjectView(R.id.distance_rdg)
    RadioGroup RG_Distance;


    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
       // mApplication.updatetextStatus=false;

        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static DistanceFilterFragment newInstance() {
        Bundle bundle = new Bundle();
        DistanceFilterFragment fragment = new DistanceFilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_distance_filter, container, false);
        ButterKnife.inject(this, view);
        mApplication=SNB_Application.getApplicationInstance(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mApplication.TempdistancePosition!=-1){
            setSelectedDistance(mApplication.TempdistancePosition);
        }else{
            setSelectedDistance(distancePosition);
        }



        RG_Distance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case (R.id.rd__5miles):
                        mApplication.TempdistancePosition = 0;
                        saveDistanceFilter("5mi");
                        break;
                    case (R.id.rd_20miles):
                        mApplication.TempdistancePosition = 1;
                        saveDistanceFilter("20mi");
                        break;
                    case (R.id.rd_50miles):
                        mApplication.TempdistancePosition = 2;
                        saveDistanceFilter("50mi");
                        break;
                    case (R.id.rd_100miles):
                        mApplication.TempdistancePosition = 3;
                        saveDistanceFilter("100mi");
                        break;

                }
            }
        });
    }

    private void saveDistanceFilter(String s) {
      //  App_Constant.currentRadius=s;
        mApplication.TempcurrentD=s;
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void setSelectedDistance(int pos) {
        switch (pos) {
            case 0:
                RD_5miles.setChecked(true);
                break;
            case 1:
                RD_20miles.setChecked(true);
                break;

            case 2:
                RD_50miles.setChecked(true);
                break;
            case 3:
               RD_100miles.setChecked(true);
                break;
        }
    }

}
