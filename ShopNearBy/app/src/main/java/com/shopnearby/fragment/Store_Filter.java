package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.StoreFilter_Adapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Array_Constant;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.ItemModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.HomeActivity.STORE_LIST;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;

/**
 * Created by neelam on 20/6/16.
 */

public class Store_Filter extends Fragment implements OnReceiveApiResult{
    private String TAG = "Store_Filter";
    private SNB_Application mApplication;
    private OnChangeFragment mChangeFragmentListener;
    private ArrayList<StoreModal> mStoreList;
    private StoreFilter_Adapter storeAdapter;
    private boolean loading = true;

    @InjectView(R.id.tv_noItems)
    TextView noItem;

    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        // mApplication.updatetextStatus=false;
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @InjectView(R.id.lv_store)
    ListView storeListview;

    @OnClick(R.id.tv_done)
    public void onDoneClick() {

        if (storeAdapter != null) {
            App_Array_Constant.Temp_STORE_LIST.clear();
            App_Array_Constant.Temp_STORE_LIST.addAll(storeAdapter.getSelectedSrore());
        }

//        App_Constant.currentStoreFilter = "";
//        for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
//            if (App_Array_Constant.Temp_STORE_LIST.get(i).isSelected()) {
//                if (App_Constant.currentStoreFilter.equalsIgnoreCase("")) {
//                    App_Constant.currentStoreFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
//                    App_Constant.currentStoreIDFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreId();
//                }
//                else {
//                    App_Constant.currentStoreFilter = App_Constant.currentStoreFilter + "," + App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
//                    App_Constant.currentStoreIDFilter = App_Constant.currentStoreIDFilter + "," +App_Array_Constant.Temp_STORE_LIST.get(i).getStoreId();
//                }
//            }
//       }
        mApplication.updatetextStatus=true;
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static Store_Filter newInstance() {
        Bundle bundle = new Bundle();
//        bundle.putString("RESULT", total);
        Store_Filter fragment = new Store_Filter();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStoreList=new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.store_filter, container, false);
        mApplication=SNB_Application.getApplicationInstance(getActivity());
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (App_Array_Constant.Temp_STORE_LIST != null && App_Array_Constant.Temp_STORE_LIST.size() > 0) {

            for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                StoreModal modal = new StoreModal();
                modal.setStoreName(App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName());
                modal.setStoreId(App_Array_Constant.Temp_STORE_LIST.get(i).getStoreId());
                modal.setSelected(App_Array_Constant.Temp_STORE_LIST.get(i).isSelected());
                mStoreList.add(modal);
            }

            setItemsAdapter();
        } else {
            loading = true;
            callStoreApi(true);
        }

    }

    private void setItemsAdapter() {
        storeAdapter=new StoreFilter_Adapter(getActivity(),mStoreList);
        storeListview.setAdapter(storeAdapter);
    }

    private void callStoreApi(boolean show) {
        try {
            String str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + App_Constant.currentRadius+ App_Constant.currentSortFilter+ "&refine=store";
            if (!currentBrandsFilter.equalsIgnoreCase("")) {
                str_request=str_request+"&brand="+currentBrandsFilter;
            }

            if (!App_Constant.min_price.equalsIgnoreCase("")) {
                str_request=str_request+"&min_price="+App_Constant.min_price.replace(",","").trim();
            }

            if (!App_Constant.max_price.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_price.toString().replace(",","").trim();
            }

            if (!currentCateogryIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&category="+currentCateogryIDFilter;;
            }


            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_LIST_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {

        mStoreList = ResponseParser.getFilterstoreList(ApiResult);

        if (mStoreList != null && mStoreList.size() > 0) {
            loading = true;

            if (storeAdapter != null) {
                storeAdapter.notifyDataSetChanged();
            } else {
                setItemsAdapter();
            }
            noItem.setVisibility(View.GONE);
            storeListview.setVisibility(View.VISIBLE);
        }else{
            noItem.setVisibility(View.VISIBLE);
            storeListview.setVisibility(View.GONE);

        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }


}
