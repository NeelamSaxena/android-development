package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.BrandAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.utils.ResponseParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.HomeActivity.sortPosition;

/**
 * Created by shailendra on 10/6/16.
 */

public class SortFilterFragment extends Fragment {
    private SNB_Application mApplication;
    private String TAG = "SortFilterFragment";
    private OnChangeFragment mChangeFragmentListener;

    @InjectView(R.id.tv_bestRated)
    TextView bestRatedText;

    @InjectView(R.id.tv_lowestPrice)
    TextView lowestPriceText;

    @InjectView(R.id.tv_highestPrice)
    TextView highestPriceText;

    @InjectView(R.id.tv_asec)
    TextView ascOrderText;

    @InjectView(R.id.tv_desc)
    TextView descOrderText;

    @OnClick(R.id.tv_bestRated)
    public void bestRatedClick() {
      //  sortPosition = 0;
        mApplication.TempsortPosition=0;
        saveSortFilter("priceSort", "desc");
    }

    @OnClick(R.id.tv_lowestPrice)
    public void lowestPriceClick() {
       // sortPosition = 2;
        mApplication.TempsortPosition=2;
        saveSortFilter("price", "asc");
    }

    @OnClick(R.id.tv_highestPrice)
    public void highestPriceClick() {
        //sortPosition = 3;
        mApplication.TempsortPosition=3;
        saveSortFilter("price", "desc");
    }

    @OnClick(R.id.tv_asec)
    public void ascOrderClick() {
        //sortPosition = 4;
        mApplication.TempsortPosition=4;
        saveSortFilter("title", "asc");
    }

    @OnClick(R.id.tv_desc)
    public void descOrderClick() {
        //sortPosition = 5;
        mApplication.TempsortPosition=5;
        saveSortFilter("title", "desc");
    }


    @OnClick(R.id.cancel_btn)
    public void icnBack_Click() {
        mApplication.TempsortPosition=-1;

        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static SortFilterFragment newInstance() {
        Bundle bundle = new Bundle();
        SortFilterFragment fragment = new SortFilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter_sort, container, false);
        mApplication=SNB_Application.getApplicationInstance(getActivity());
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mApplication.TempsortPosition==-1)
           setSelectedSort(sortPosition);
        else
            setSelectedSort(mApplication.TempsortPosition);
    }

    private void setSelectedSort(int pos) {


        switch (pos) {
            case 0:
                bestRatedText.setSelected(true);
                lowestPriceText.setSelected(false);
                highestPriceText.setSelected(false);
                ascOrderText.setSelected(false);
                descOrderText.setSelected(false);
                break;
            case 1:

                break;

            case 2:
                bestRatedText.setSelected(false);
                lowestPriceText.setSelected(true);
                highestPriceText.setSelected(false);
                ascOrderText.setSelected(false);
                descOrderText.setSelected(false);
                break;
            case 3:
                bestRatedText.setSelected(false);
                lowestPriceText.setSelected(false);
                highestPriceText.setSelected(true);
                ascOrderText.setSelected(false);
                descOrderText.setSelected(false);
                break;
            case 4:
                bestRatedText.setSelected(false);
                lowestPriceText.setSelected(false);
                highestPriceText.setSelected(false);
                ascOrderText.setSelected(true);
                descOrderText.setSelected(false);
                break;
            case 5:
                bestRatedText.setSelected(false);
                lowestPriceText.setSelected(false);
                highestPriceText.setSelected(false);
                ascOrderText.setSelected(false);
                descOrderText.setSelected(true);
                break;
        }
    }

    private void saveSortFilter(String key, String value) {
        try {
            JSONObject obj = new JSONObject();
            obj.put(key, value);
           // App_Constant.currentSortFilter = obj.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}