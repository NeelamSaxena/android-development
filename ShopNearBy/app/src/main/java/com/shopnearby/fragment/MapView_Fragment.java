package com.shopnearby.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.preference.App_Preference;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by ankit on 1/6/16.
 */
public class MapView_Fragment extends Fragment {
    private static final String TAG = "MapView_Fragment";
    private GoogleMap map_view;
    private Handler mHandler;
    private SupportMapFragment MapFragment;
    private OnChangeFragment mChangeFragmentListener;
    private BottomSheetDialog BottomDialog;
    static final int MY_PERMISSIONS_REQUEST_CALL = 11;
    ArrayList<StoreModal> StoreList;
    private SNB_Application mApplication;
    List<Marker> markersList = new ArrayList<>();
    private int width = 0;
    private String itemName = "";
    private String ImgUrl = "";
    private String MobileNumber = "";
    private LatLng currentLatLng;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static MapView_Fragment newInstance(ArrayList<StoreModal> storeList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("STORE_LIST", storeList);
        MapView_Fragment mFragment = new MapView_Fragment();
        mFragment.setArguments(bundle);
        return mFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (SNB_Application) getActivity().getApplicationContext();
        if (getArguments() != null) {
            StoreList = getArguments().getParcelableArrayList("STORE_LIST");
            Log.d(TAG, "StoreList "+StoreList.size() );
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_view_stores, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    showMap();
                }
            }
        };
        width = (int) ((mApplication.Screen_Width * 85) / 100);
        currentLatLng = new LatLng(App_Constant.sCurrentLat,App_Constant.sCurrentLng);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MapFragment = new SupportMapFragment();
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.replace(
                R.id.StoreMapFrame, MapFragment, "MAP");
        transaction.commitAllowingStateLoss();
        mHandler.sendEmptyMessage(1);
    }

    private void showMap() {

        map_view = MapFragment.getMap();
        if (map_view == null) {
            return;
        }
//        map_view.setMyLocationEnabled(false);

        map_view.getUiSettings().setZoomControlsEnabled(true);
        map_view.getUiSettings().setCompassEnabled(false);
        map_view.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map_view.moveCamera(CameraUpdateFactory.newLatLngZoom(
                currentLatLng, 14));
        map_view.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (StoreList != null && StoreList.size() > 0) {
                    setMarkersOnMap();
                }
            }
        });
        map_view.addMarker(new MarkerOptions().position(currentLatLng).icon(BitmapDescriptorFactory
                .fromResource(R.drawable.ic_place_black_24dp)));

        map_view.setInfoWindowAdapter(new CustomInfoWindowAdapter());
    }

    private void setMarkersOnMap() {
        if (map_view != null) {
            map_view.clear();
            map_view.addMarker(new MarkerOptions().position(currentLatLng).icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.ic_place_black_24dp)));

//            map_view.setInfoWindowAdapter(new CustomInfoWindowAdapter());
            for (int i = 0; i < StoreList.size(); i++) {
                LatLng ll = new LatLng(StoreList.get(i).getStoreLat(), StoreList.get(i).getStoreLng());

                TextView mGreenMarkerView = new TextView(getActivity());
                mGreenMarkerView.setTag(StoreList.get(i));
                mGreenMarkerView.setTextColor(Color.WHITE);
                mGreenMarkerView.setBackgroundResource(R.drawable.ic_pin_green);
                if (StoreList.size() > 1) {
                    mGreenMarkerView.setText("" + (i + 1));
                }
                mGreenMarkerView.setGravity(Gravity.CENTER);
                IconGenerator generator = new IconGenerator(getActivity());
                generator.setBackground(null);
                generator.setContentView(mGreenMarkerView);
                Bitmap icon = generator.makeIcon();
                MarkerOptions tp = new MarkerOptions().position(ll).snippet("" + i).icon(BitmapDescriptorFactory.fromBitmap(icon));
                Marker pinMarker = map_view.addMarker(tp);
                markersList.add(pinMarker);
                map_view.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        Log.d("Ankit", "onMarkerClick: " + marker.getSnippet());
                        if (marker.getSnippet() != null) {
                            marker.showInfoWindow();
                        }
                        return false;
                    }
                });

                int padding = 100; // offset from edges of the map in pixels

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(currentLatLng);
                for (Marker marker : markersList) {
                    builder.include(marker.getPosition());
                }
                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map_view.animateCamera(cu);
                if (StoreList.size() == 1) {
                    pinMarker.showInfoWindow();
                }
            }
        }
    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {


        public CustomInfoWindowAdapter() {

        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (marker.getSnippet() != null) {
                final StoreModal mModal = StoreList.get(Integer.parseInt(marker.getSnippet()));
                View view = getActivity().getLayoutInflater().inflate(R.layout.info_window_layout,
                        null);
                TextView txtTitle = (TextView) view.findViewById(R.id.tv_title);
                TextView txtPrice = (TextView) view.findViewById(R.id.tv_price);
                TextView txt_Availability = (TextView) view.findViewById(R.id.tv_availability);
                TextView txtTiming = (TextView) view.findViewById(R.id.tv_timing);
                TextView txtDistance = (TextView) view.findViewById(R.id.tv_distance);
                TextView txtUpdateTime = (TextView) view.findViewById(R.id.tv_UpdateTime);

                //Action Buttons
 //              TextView txtCall =(TextView)view.findViewById(R.id.tv_Call);
//                TextView txt_More = (TextView) view.findViewById(R.id.tv_More);
//                TextView txt_Share = (TextView) view.findViewById(R.id.tv_Share);
//                txt_More.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        openBottomSheetDialog(mModal);
//                    }
//                });
//                txt_Share.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        shareToOtherApps(mModal);
//                    }
//                });
//                txtCall.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        MobileNumber = mModal.getPhoneNumber();
//                        makeAcall(MobileNumber);
//                    }
//                });

                TextView txtAdd =(TextView)view.findViewById(R.id.tv_add);

                txtTitle.setText(mModal.getRetailerName() + " " + mModal.getStoreName());
                txtPrice.setText("$" + mModal.getPrice());
                if (mModal.isInStoreAvailability()) {
                    txt_Availability.setText("Buy & pickup in store");
                    txtAdd.setVisibility(View.VISIBLE);
                }
                else {
                    txt_Availability.setText("In-store only");
                    txtAdd.setVisibility(View.GONE);
                }
                txtTiming.setText("Open, " + mModal.getOpenTime() + " - " + mModal.getCloseTime());
                txtDistance.setText(mModal.getDistance() + " miles away");
                txtUpdateTime.setText(mModal.getUpdateDate());

                LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                txtTitle.setLayoutParams(Params1);
                return view;
            }
            return null;
        }
    }

    private void openBottomSheetDialog(final StoreModal modal) {
        BottomDialog = new BottomSheetDialog(getActivity());
        BottomDialog.setContentView(R.layout.bottom_sheet_layout);
        TextView Txt_shareMenu = (TextView) BottomDialog.findViewById(R.id.tv_ShareBS);
        if (!modal.isInStoreAvailability()) {
            Txt_shareMenu.setVisibility(View.GONE);
        }
        Txt_shareMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareToOtherApps(modal);
            }
        });

        BottomDialog.show();
    }

    public void makeAcall(String number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                startActivity(callIntent);
            } else {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL);
            }
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + MobileNumber));
                    startActivity(callIntent);
                }
            }
        }
    }

    private void shareToOtherApps(StoreModal modal) {
        String shareText = itemName + "\nIt's available at " + modal.getRetailerName() + " " + modal.getStoreName() + " for $" + modal.getPrice() + ". The store address is " + modal.getAddress() + " " + modal.getCity() + " " + modal.getPostalCode();
        Intent shareIntent = new Intent();
        Uri pictureUri = Uri.parse(ImgUrl);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share product"));
    }
//    private void addIcon(IconGenerator iconFactory, String text, LatLng position) {
//        MarkerOptions markerOptions = new MarkerOptions().
//                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
//                position(position).
//                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
//
//        map_view.addMarker(markerOptions);
//    }
}
