package com.shopnearby.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.adapter.ItemsAdapter;
import com.shopnearby.adapter.RecentSearchAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.custom.RecyclerItemClickListener;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.ItemModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.DividerItemDecoration;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static android.databinding.DataBindingUtil.inflate;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentSearchBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentSearchCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentSearchStoreIDFilter;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;

/**
 * Created by ankit on 3/6/16.
 */
public class SearchFragment extends Fragment implements OnReceiveApiResult {
    OnChangeFragment mChangeFragmentListener;
    private String TAG = "SearchFragment";
    private String UserId = "";
    private String strSearchQuery = "";
    private String categoryId = "";
    private String characterTyped = "";
    private SNB_Application mApplication;
    private ArrayList<ItemModal> mItemList;
    private boolean loading = true;
    public int mPageCount = 1;
    private int mPageSize = 30;
    private int SelectedView=2;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;
    private ItemsAdapter mItemAdapter;
    private RecentSearchAdapter mRecentSearchAdapter;
    private List<String> searchList;
    private List<String> searchedTerms;
    private List<SearchCategoryModal> originalCatList;
    private List<SearchCategoryModal> categoryList;
    public boolean clearList;



    private LayoutInflater inflater;
    private boolean callAutocompleteApi;
    @InjectView(R.id.rv_SearchItems)
    RecyclerView mRecyclerViewItems;

    @InjectView(R.id.rv_RecentItems)
    RecyclerView mRecyclerViewRecentSearch;

    @InjectView(R.id.et_search)
    EditText SearchEdittext;

    @InjectView(R.id.tv_searchTerms)
    LinearLayout txtSearchTerms;

    @InjectView(R.id.ll_noResultView)
    LinearLayout LL_noResultFound;
    @InjectView(R.id.ll_recentView)
    LinearLayout LL_RecentView;
    @InjectView(R.id.rl_searchResultView)
    RelativeLayout RL_SearchResult;

    @InjectView(R.id.sv_autocomplete)
    ScrollView autoCompleteScrollView;

    @InjectView(R.id.ll_categorySearch)
    LinearLayout LL_CategorySearchParent;
    @InjectView(R.id.ll_searchTerms)
    LinearLayout LL_SearchTermsParent;

    @InjectView(R.id.imgClear)
    ImageView clearImageView;

    @OnClick(R.id.imgClear)
    public void clearSearchField() {
        SearchEdittext.setText("");
    }

    @OnClick(R.id.btn_searchAgain)
    public void searchAgainClick() {
        openKeyBoard();
    }




    @OnClick(R.id.imgBack)
    public void goBack() {
        hideKeyboardByForce();
        RL_SearchResult.requestFocus();
        SearchEdittext.clearFocus();
        if (LL_RecentView.getVisibility() == View.VISIBLE && mItemList != null && mItemList.size() > 0) {
            RL_SearchResult.setVisibility(View.VISIBLE);
            LL_RecentView.setVisibility(View.GONE);
            LL_noResultFound.setVisibility(View.GONE);
            autoCompleteScrollView.setVisibility(View.GONE);
        } else {
            getActivity().onBackPressed();
        }
    }

    @InjectView(R.id.tv_showMore)
    TextView showMoreTextview;

    @OnClick(R.id.tv_showMore)
    public void showAllResults() {
        if (originalCatList != null) {
            categoryList.clear();
            categoryList.addAll(originalCatList);
            setSearchCategoryAdapter();
            showMoreTextview.setVisibility(View.GONE);
        }
    }

    @InjectView(R.id.tab_View)
    LinearLayout TabViewLayout;


    @InjectView(R.id.allcategory)
    RelativeLayout AllCategory;

    @InjectView(R.id.productcategory)
    RelativeLayout ProductCategory;


    @InjectView(R.id.allcategoryText)
    TextView AllcategoryText;

    @InjectView(R.id.productcategoryText)
    TextView ProductcategoryText;


    @OnClick(R.id.allcategory)
    public void onAllCategoryClick() {
        SetTabView(1);
    }

    @OnClick(R.id.productcategory)
    public void onProductCategoryClick() {
        SetTabView(2);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initMembers();
    }

    private void initMembers() {



        searchList = new ArrayList<>();
        searchedTerms = new ArrayList<>();
        mItemList = new ArrayList<>();
        originalCatList = new ArrayList<>();
        categoryList = new ArrayList<>();

        if (App_Preference.newInstance(getActivity()).getUserData() != null) {
            UserId = App_Preference.newInstance(getActivity()).getUserData().getStrUserId();
        }
        mApplication = (SNB_Application) getActivity().getApplicationContext();

        if (App_Preference.newInstance(getActivity()).getSaveSearchedItems() != null) {
            searchList = App_Preference.newInstance(getActivity()).getSaveSearchedItems();
        }

        callAutocompleteApi = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mItemList != null && mItemList.size() > 0) {
            setItemsAdapter();
        } else if (searchList != null && searchList.size() > 0) {
            Collections.reverse(searchList);
            setRecentSearchAdapter();
            openKeyBoard();
        } else {
            openKeyBoard();
        }
        SearchEdittext.requestFocus();
        SearchEdittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    strSearchQuery = SearchEdittext.getText().toString().trim();
                    if (!strSearchQuery.equalsIgnoreCase(""))
                        saveQuery(strSearchQuery);
                    clearList = true;
                    mPageCount = 1;
                    callProductSearchApi(strSearchQuery, true);
                    hideKeyboardByForce();
                    return true;
                }
                return false;
            }
        });

        SearchEdittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && searchList != null && searchList.size() > 0) {
                    setRecentSearchAdapter();
                } else {
                    clearImageView.setVisibility(View.GONE);
                }
            }
        });

        SearchEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                characterTyped = charSequence.toString();
                Log.d("Ankit", "callAutocompleteApi: " + callAutocompleteApi);
                if (characterTyped.length() > 0 && callAutocompleteApi) {
                    clearImageView.setVisibility(View.VISIBLE);
                    callAutoCompleteApi(characterTyped);
                } else {
                    clearImageView.setVisibility(View.GONE);
                    callAutocompleteApi = true;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if(!App_Constant.currentCateogryIDFilter.equalsIgnoreCase(""))
        {
            TabViewLayout.setVisibility(View.VISIBLE);
            AllcategoryText.setText(getActivity().getResources().getString(R.string.Tag_AllCategories));
            ProductcategoryText.setText(App_Constant.currentItemHeader);
            SetTabView(2);
        }else {
            TabViewLayout.setVisibility(View.GONE);
        }
    }

    private void callAutoCompleteApi(String charSequence) {
        try {
            String str_request = App_Constant.URL.AUTOCOMPLETE_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&radius=" + App_Constant.currentRadius + "&q=" + charSequence;




            if(SelectedView==2){
                if (!currentCateogryIDFilter.equalsIgnoreCase("")) {
                    str_request =str_request+ "&category="+currentCateogryIDFilter;;
                }
            }

            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.AUTOCOMPLETE_API, "Loading...", str_request, false);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void saveQuery(String SearchQuery) {
        if (searchList == null)
            searchList = new ArrayList<>();

        for (String s : searchList) {
            if (s.equalsIgnoreCase(SearchQuery)) { // Or use an accessor function for `nomeParagem` if appropriate
                searchList.remove(s);
                break;
            }
        }
        searchList.add(SearchQuery);

        App_Preference.newInstance(getActivity()).saveSearchedItems(searchList);
    }

    public void callProductSearchApi(String query, boolean show) {
        try {
            String str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&radius=" + App_Constant.currentRadius + "&q=" + query;
            if (!currentSearchBrandsFilter.equalsIgnoreCase("")) {
                str_request=str_request+"&brand="+currentSearchBrandsFilter;
            }

            if (!App_Constant.min_priceSearch.equalsIgnoreCase("")) {
                str_request=str_request+"&min_priceSearch="+App_Constant.min_priceSearch.replace(",","").trim();
            }

            if (!App_Constant.max_priceSearch.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_priceSearch.toString().replace(",","").trim();
            }



            if (!currentSearchStoreIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&store="+currentSearchStoreIDFilter;;
            }


            if(SelectedView==2){
                if (!currentSearchCateogryIDFilter.equalsIgnoreCase("")) {
                    str_request =str_request+ "&category="+currentSearchCateogryIDFilter;;
                }
            }
            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_LIST_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callProductSearchByCategoryApi(boolean show) {
        try {
            String str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&radius=" + App_Constant.currentRadius;

            if (!currentSearchBrandsFilter.equalsIgnoreCase("")) {
                str_request=str_request+"&brand="+currentSearchBrandsFilter;
            }

            if (!App_Constant.min_priceSearch.equalsIgnoreCase("")) {
                str_request=str_request+"&min_price="+App_Constant.min_priceSearch.replace(",","").trim();
            }

            if (!App_Constant.max_priceSearch.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_priceSearch.toString().replace(",","").trim();
            }



            if (!currentSearchStoreIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&store="+currentSearchStoreIDFilter;;
            }


//            if(SelectedView==2){
//                if(!categoryId.equalsIgnoreCase("")){
//                    str_request =str_request+ "&category="+categoryId;;
//                }else if (!currentSearchCateogryIDFilter.equalsIgnoreCase("")) {
//                    str_request =str_request+ "&category="+currentSearchCateogryIDFilter;;
//                }
//            }else{
//                if(!currentSearchCateogryIDFilter.equalsIgnoreCase("")){
//                    str_request =str_request+ "&category="+currentSearchCateogryIDFilter;;
//                }
//            }
            if(!currentSearchCateogryIDFilter.equalsIgnoreCase("")){
                str_request =str_request+ "&category="+currentSearchCateogryIDFilter;;
            }



            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.PRODUCT_LIST_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setRecentSearchAdapter() {
//        clearImageView.setVisibility(View.VISIBLE);
        LL_RecentView.setVisibility(View.VISIBLE);
        RL_SearchResult.setVisibility(View.GONE);
        LL_noResultFound.setVisibility(View.GONE);
        autoCompleteScrollView.setVisibility(View.GONE);
        mRecentSearchAdapter = new RecentSearchAdapter(searchList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerViewRecentSearch.setLayoutManager(mLayoutManager);
        mRecyclerViewRecentSearch.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewRecentSearch.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewRecentSearch.setAdapter(mRecentSearchAdapter);
        mRecyclerViewRecentSearch.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                clearList = true;
                mPageCount = 1;
                strSearchQuery = searchList.get(position);
                callAutocompleteApi = false;
                SearchEdittext.setText(strSearchQuery);
                callProductSearchApi(strSearchQuery, true);
                hideKeyboardByForce();
            }
        }));
    }

    public void setItemsAdapter() {
        RL_SearchResult.setVisibility(View.VISIBLE);
        LL_RecentView.setVisibility(View.GONE);
        LL_noResultFound.setVisibility(View.GONE);
        autoCompleteScrollView.setVisibility(View.GONE);
        mItemAdapter = new ItemsAdapter(mItemList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewItems.setLayoutManager(mLayoutManager);
        mRecyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewItems.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewItems.setAdapter(mItemAdapter);
        mRecyclerViewItems.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                callAutocompleteApi=false;
                mChangeFragmentListener.onReplaceFragment(ItemDetailFragment.newInstance(mItemList.get(position).getStrId(), mItemList.get(position).getmStoreCount()), TAG);
            }
        }));

        mRecyclerViewItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (totalItemCount > 20) {
                        totalItemCount = totalItemCount - 10;
                    }

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            clearList = false;
                            callProductSearchApi(strSearchQuery, false);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {

        if (URL_Type.equals(App_Constant.URL.PRODUCT_LIST_API)) {
            ArrayList<ItemModal> mList;
            mList = ResponseParser.getItemsList(ApiResult);
            if (clearList) {
                mPageCount = 1;
                mItemList.clear();
            }

            if (mList != null && mList.size() > 0) {

                RL_SearchResult.setVisibility(View.VISIBLE);
                LL_RecentView.setVisibility(View.GONE);
                autoCompleteScrollView.setVisibility(View.GONE);
                LL_noResultFound.setVisibility(View.GONE);

                loading = true;
                mPageCount++;
                mPageSize = 20;
                mItemList.addAll(mList);
                if (mItemAdapter != null) {
                    mItemAdapter.notifyDataSetChanged();
                } else {
                    setItemsAdapter();
                }

            } else {
                loading = false;
                if (mPageCount == 1) {
                    LL_noResultFound.setVisibility(View.VISIBLE);
                    LL_RecentView.setVisibility(View.GONE);
                    RL_SearchResult.setVisibility(View.GONE);
                    autoCompleteScrollView.setVisibility(View.GONE);
                }
            }

            RL_SearchResult.requestFocus();
            SearchEdittext.clearFocus();
            TabViewLayout.setVisibility(View.GONE);
            ((HomeActivity)getActivity()).updateFilterTextSearch(false);
            ((HomeActivity)getActivity()).LL_FilterBar.setVisibility(View.VISIBLE);
        } else if (URL_Type.equals(App_Constant.URL.AUTOCOMPLETE_API)) {
            searchedTerms = ResponseParser.getSearchedTermList(ApiResult);
            originalCatList = ResponseParser.getSearchCategoryList(ApiResult);
            if (originalCatList != null) {
                categoryList.clear();
                if (originalCatList.size() > 4) {
                    for (int i = 0; i < 4; i++) {
                        categoryList.add(originalCatList.get(i));
                    }
                    showMoreTextview.setVisibility(View.VISIBLE);
                } else {
                    categoryList.addAll(originalCatList);
                    showMoreTextview.setVisibility(View.GONE);
                }
                setSearchCategoryAdapter();
            }

        }

    }

    private void setSearchCategoryAdapter() {
        LL_CategorySearchParent.removeAllViews();
        LL_SearchTermsParent.removeAllViews();
        Log.d("Ankit", "categoryList: " + categoryList.size());
        LL_RecentView.setVisibility(View.GONE);
        RL_SearchResult.setVisibility(View.GONE);
        LL_noResultFound.setVisibility(View.GONE);
        autoCompleteScrollView.setVisibility(View.VISIBLE);
        for (int i = 0; i < categoryList.size(); i++) {

            SearchCategoryModal modal = categoryList.get(i);
            inflater = getActivity().getLayoutInflater();
            final View childView = inflater.inflate(R.layout.item_recent_search, null);
            TextView catText = (TextView) childView.findViewById(R.id.tv_searchText);
            catText.setTag(modal);
            String sourceString = "<b>" + modal.getText() + "</b> " + " in " + modal.getName();
            catText.setText(Html.fromHtml(sourceString));

            catText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearList = true;
                    mPageCount = 1;
                    strSearchQuery = ((SearchCategoryModal) view.getTag()).getText();
                  //  categoryId = ((SearchCategoryModal) view.getTag()).getId();
                    currentSearchCateogryIDFilter = ((SearchCategoryModal) view.getTag()).getId();
                    saveQuery(strSearchQuery);
                    callAutocompleteApi = false;
                    SearchEdittext.setText(strSearchQuery);
            //        callProductSearchByCategoryApi(categoryId, true);
                    callProductSearchByCategoryApi(true);
                    hideKeyboardByForce();
                }
            });
            LL_CategorySearchParent.addView(childView);
        }
        if (searchedTerms != null) {
            txtSearchTerms.setVisibility(View.VISIBLE);
            LL_SearchTermsParent.setVisibility(View.VISIBLE);
            for (int i = 0; i < searchedTerms.size(); i++) {
                inflater = getActivity().getLayoutInflater();
                final View childView = inflater.inflate(R.layout.item_recent_search, null);
                TextView catText = (TextView) childView.findViewById(R.id.tv_searchText);

                String s = searchedTerms.get(i);
                String first = s.substring(0, characterTyped.length());  // gives "How ar"
                String second = s.substring(characterTyped.length());
                catText.setTag(s);

                String sourceString = "<b>" + first + "</b>" + second;
                catText.setText(Html.fromHtml(sourceString));

                catText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clearList = true;
                        mPageCount = 1;
                        strSearchQuery = view.getTag().toString();
                        saveQuery(strSearchQuery);
                        callAutocompleteApi = false;
                        SearchEdittext.setText(strSearchQuery);
                        callProductSearchApi(strSearchQuery, true);
                        hideKeyboardByForce();
                    }
                });
                LL_SearchTermsParent.addView(childView);
            }
        } else {
            LL_SearchTermsParent.setVisibility(View.GONE);
            txtSearchTerms.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }

    private void openKeyBoard() {
        SearchEdittext.requestFocus();
        try {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_UNCHANGED_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideKeyboardByForce() {
        try {
            mApplication.hideSoftKeyboard(getActivity(), SearchEdittext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetTabView(int View){
        if(View==1){
            SelectedView=1;
            AllCategory.setBackgroundResource(R.drawable.selected_tab);
            ProductCategory.setBackgroundResource(R.drawable.unselected_tab);
            AllcategoryText.setTextColor(Color.parseColor("#32c700"));
            ProductcategoryText.setTextColor(Color.parseColor("#788587"));

        }else{
            SelectedView=2;
            AllCategory.setBackgroundResource(R.drawable.unselected_tab);
            ProductCategory.setBackgroundResource(R.drawable.selected_tab);
            AllcategoryText.setTextColor(Color.parseColor("#788587"));
            ProductcategoryText.setTextColor(Color.parseColor("#32c700"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mItemList.size()>0){
            TabViewLayout.setVisibility(View.GONE);
            ((HomeActivity)getActivity()).updateFilterTextSearch(false);
            ((HomeActivity)getActivity()).LL_FilterBar.setVisibility(View.VISIBLE);
        }
    }
}
