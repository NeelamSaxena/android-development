package com.shopnearby.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.async.PostParamAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.User;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.ResponseParser;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 12/5/16.
 */
public class PersonalizeAccFragment extends Fragment implements OnReceiveApiResult {

    private String TAG = "PersonalizeAccFragment";
    OnChangeFragment mChangeFragmentListener;
    private String strFirstName = "";
    private String strLastName = "";
    private String strEmail = "";
    private String strPassword = "";
    private String strGender = "";
    private String strAge = "";
    String[] ageArray;

    @InjectView(R.id.tv_pageTitle)
    TextView pageTitleTextView;

    @InjectView(R.id.age_spinner)
    Spinner ageSpinner;

    @InjectView(R.id.rg_gender)
    RadioGroup genderRadioGroup;

    @InjectView(R.id.btn_CreateAccount)
    Button mCreateAccountButton;

    @OnClick(R.id.btn_CreateAccount)
    public void createAccountClick() {
            callRegisterApi();
    }

    @InjectView(R.id.tv_skip)
    TextView mSkipTextView;

    @OnClick(R.id.tv_skip)
    public void onSkipClick() {
        strAge = "";
        strGender = "";
        callRegisterApi();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    public static PersonalizeAccFragment newInstance() {
        return new PersonalizeAccFragment();
    }

    public static PersonalizeAccFragment newInstance(String name, String lastname, String password, String email) {
        PersonalizeAccFragment fragment = new PersonalizeAccFragment();
        Bundle bundle = new Bundle();
        bundle.putString("NAME", name);
        bundle.putString("LAST", lastname);
        bundle.putString("EMAIL", email);
        bundle.putString("PASSWORD", password);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            strFirstName = getArguments().getString("NAME");
            strLastName = getArguments().getString("LAST");
            strPassword = getArguments().getString("PASSWORD");
            strEmail = getArguments().getString("EMAIL");
        }
        ageArray = new String[83];
        for (int i = 0; i <= 82; i++) {
            ageArray[i] = "" + (i + 18);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personalize_account, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pageTitleTextView.setText(getString(R.string.Tag_PersonalizeAcc));
        mSkipTextView.setVisibility(View.VISIBLE);
        mCreateAccountButton.setEnabled(false);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, ageArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(adapter);

        ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                strAge = ageArray[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ageSpinner.setSelection(7);

        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.rb_male)
                {
                    strGender="M";
                }
                if(checkedId == R.id.rb_female)
                {
                    strGender="F";
                }

                checkCredentials();
            }
        });
    }

    private void checkCredentials() {
        if (!strGender.equalsIgnoreCase("")) {
            mCreateAccountButton.setEnabled(true);
            mCreateAccountButton.setAlpha(1f);
        } else {
            mCreateAccountButton.setEnabled(false);
            mCreateAccountButton.setAlpha(0.6f);
        }
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
//        {"success":true,"data":{"__v":0,"updatedAt":"2016-05-17T08:38:04.000Z","createdAt":"2016-05-17T08:38:04.000Z","password":true,"email":"y@mailinator.com","fname":"Hddh","lname":"B hd","gender":"M","dob":"1990-05-12","referral":"YMAI5320","_id":"573ad86c8a81ffec3b4eca50","loggedinAt":null,"state":1,"user_token":"p_hgOCfDF2MHlxeUZVwo5tuWy1fwhtiqrT"}}
        User user = ResponseParser.getUserData(ApiResult);
        if (user != null) {
            App_Preference.newInstance(getActivity()).setUserData(user);
            Intent mHomeIntent = new Intent(getActivity(), HomeActivity.class);
            startActivity(mHomeIntent);
            getActivity().finish();
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }

    private void callRegisterApi() {
        try {
            JSONObject mRequestObj = new JSONObject();
            mRequestObj.put("api_secure_key", App_Constant.API_SECURE_KEY);
            mRequestObj.put("email", strEmail);
            mRequestObj.put("fname", strFirstName);
            mRequestObj.put("lname", strLastName);
            mRequestObj.put("gender", strGender);
            mRequestObj.put("password", strPassword);
            mRequestObj.put("dob", strAge);
            String str_request = mRequestObj.toString();
            OnReceiveApiResult onReceive = this;
            PostParamAsync mProductAsync = new PostParamAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.REGISTER_API, "Loading...", str_request);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void ShowDatePicker() {
//        // Show a datepicker when the dateButton is clicked
//        Calendar now = Calendar.getInstance();
//        DatePickerDialog dpd = DatePickerDialog.newInstance(
//                this,
//                now.get(Calendar.YEAR),
//                now.get(Calendar.MONTH),
//                now.get(Calendar.DAY_OF_MONTH)
//        );
//        dpd.setThemeDark(true);
//        dpd.vibrate(true);
//        dpd.setMaxDate(now);
//        dpd.show(getFragmentManager(), "Datepickerdialog");
//
//    }

//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String date;
//        if (dayOfMonth < 10) {
//            date = (++monthOfYear) + "/0" + dayOfMonth + "/" + year;
//        } else {
//            date = (++monthOfYear) + "/" + dayOfMonth + "/" + year;
//        }
//        strBirthday = date;
//        mBirthdayTextView.setText(date);
//        checkCredentials();
//    }

}
