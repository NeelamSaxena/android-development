package com.shopnearby.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.databinding.ClosestLayoutBinding;
import com.shopnearby.modal.ItemDetailsModal;
import com.shopnearby.modal.StoreModal;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ankit on 31/5/16.
 */
public class LowestFragment extends Fragment {
    private String TAG = "LowestFragment";
    OnChangeFragment mChangeFragmentListener;
    private LayoutInflater inflater;
    ItemDetailsModal mDetailsModal;
    static final int MY_PERMISSIONS_REQUEST_CALL = 11;
    private String MobileNumber = "";
    private ArrayList<StoreModal> mStoreList;
    private SNB_Application mApplication;
    private String itemName = "";
    private String ImgUrl = "";
    private BottomSheetDialog BottomDialog;
    private TextView Txt_shareMenu;
    private TextView Txt_mapView;

    @InjectView(R.id.ll_parent_view)
    LinearLayout LL_ParentContainer;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static LowestFragment newInstance(ArrayList<StoreModal> list, String name, String url) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("DETAIL", list);
        bundle.putString("NAME", name);
        bundle.putString("ImgUrl", url);
        LowestFragment mFragment = new LowestFragment();
        mFragment.setArguments(bundle);
        return mFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStoreList = new ArrayList<>();

        if (getArguments() != null) {
            mStoreList = getArguments().getParcelableArrayList("DETAIL");
            itemName = getArguments().getString("NAME");
            ImgUrl = getArguments().getString("ImgUrl");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_closest, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LL_ParentContainer.removeAllViews();
        for (int i = 0; i < mStoreList.size(); i++) {
            StoreModal mModal = mStoreList.get(i);
            inflater = getActivity().getLayoutInflater();

            ClosestLayoutBinding childBinding = DataBindingUtil.inflate(inflater, R.layout.closest_layout, (ViewGroup) view.getParent(), false);
            final View child_view = childBinding.getRoot();
            childBinding.setStoreVM(mModal);

            TextView txtTitle = (TextView) child_view.findViewById(R.id.tv_title);

            //Action Buttons
            TextView txt_Add = (TextView) child_view.findViewById(R.id.tv_add);
            TextView txt_Direction = (TextView) child_view.findViewById(R.id.tv_Direction);
            TextView txt_Call = (TextView) child_view.findViewById(R.id.tv_Call);
            TextView txt_Share = (TextView) child_view.findViewById(R.id.tv_Share);
            TextView txt_More = (TextView) child_view.findViewById(R.id.tv_More);
            txt_More.setTag(mModal);
            txt_Share.setTag(mModal);
            txt_Call.setTag(mModal);
            txt_More.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openBottomSheetDialog((StoreModal) view.getTag());
                }
            });
            txt_Share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareToOtherApps((StoreModal) view.getTag());
                }
            });
            txt_Call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Ankit", "Call clicked");
                    MobileNumber = ((StoreModal) view.getTag()).getPhoneNumber();
                    makeAcall(MobileNumber);
                }
            });

            txtTitle.setText((i + 1) + ". " + mModal.getRetailerName() + " " + mModal.getStoreName());
            LL_ParentContainer.addView(child_view);
        }
    }

    private void openBottomSheetDialog(final StoreModal modal) {
        BottomDialog = new BottomSheetDialog(getActivity());
        BottomDialog.setContentView(R.layout.bottom_sheet_layout);
        Txt_shareMenu = (TextView) BottomDialog.findViewById(R.id.tv_ShareBS);
        Txt_mapView = (TextView) BottomDialog.findViewById(R.id.tv_MapViewBS);
        Txt_mapView.setVisibility(View.VISIBLE);
        if (!modal.isInStoreAvailability()) {
            Txt_shareMenu.setVisibility(View.GONE);
        }
        Txt_shareMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareToOtherApps(modal);
            }
        });
        Txt_mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<StoreModal> mlist = new ArrayList<>();
                mlist.add(modal);
                BottomDialog.dismiss();
                mChangeFragmentListener.onReplaceFragment(MapView_Fragment.newInstance(mlist), TAG);
            }
        });
        BottomDialog.show();
    }

    public void makeAcall(String number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                startActivity(callIntent);
            } else {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL);
            }
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + MobileNumber));
                    startActivity(callIntent);
                }
            }
        }
    }

    private void shareToOtherApps(StoreModal modal) {
        String shareText = itemName + "\nIt's available at " + modal.getRetailerName() + " " + modal.getStoreName() + " for $" + modal.getPrice() + ". The store address is " + modal.getAddress() + " " + modal.getCity() + " " + modal.getPostalCode();
        Intent shareIntent = new Intent();
        Uri pictureUri = Uri.parse(ImgUrl);
        Log.d("Ankit", "ImgUrl: " + pictureUri);
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share product"));
    }


}