package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopnearby.R;
import com.shopnearby.custom.OnboarderPage;

public class OnboarderFragment extends Fragment {

    private static final String ONBOARDER_PAGE_IMAGE_RES_ID = "onboarder_page_iamge_res_id";

    @DrawableRes
    private int onboarderImageResId;

    private View onboarderView;
    private ImageView ivOnboarderImage;
    private ImageView Img_topLogo;
    private TextView txt_Center;

    public OnboarderFragment() {
    }

    public static OnboarderFragment newInstance(OnboarderPage page) {
        Bundle args = new Bundle();
        args.putInt(ONBOARDER_PAGE_IMAGE_RES_ID, page.getImageResourceId());
        OnboarderFragment fragment = new OnboarderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getArguments();

        onboarderImageResId = bundle.getInt(ONBOARDER_PAGE_IMAGE_RES_ID, 0);

        onboarderView = inflater.inflate(R.layout.fragment_onboarder, container, false);
        ivOnboarderImage = (ImageView) onboarderView.findViewById(R.id.iv_onboarder_image);
        Img_topLogo = (ImageView) onboarderView.findViewById(R.id.img_logo);
        txt_Center = (TextView) onboarderView.findViewById(R.id.tv_center);


        if (onboarderImageResId != 0) {
            Img_topLogo.setVisibility(View.GONE);
            txt_Center.setVisibility(View.GONE);
            ivOnboarderImage.setVisibility(View.VISIBLE);
            ivOnboarderImage.setImageDrawable(ContextCompat.getDrawable(getActivity(), onboarderImageResId));
        } else {
            ivOnboarderImage.setVisibility(View.GONE);
            Img_topLogo.setVisibility(View.VISIBLE);
            txt_Center.setVisibility(View.VISIBLE);
        }

        return onboarderView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
