package com.shopnearby.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.shopnearby.HomeActivity;
import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.async.PostParamAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.User;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.ResponseParser;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * Created by ankit on 10/5/16.
 */
public class LoginOptionFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, OnReceiveApiResult {
    OnChangeFragment mChangeFragmentListener;
    private String TAG = "LoginOptionFragment";
    private SNB_Application mApplication;
    GoogleApiClient mGoogleApiClient;
    GoogleApiAvailability google_api_availability;
    private static final int SIGN_IN_CODE = 0;
    private ConnectionResult connection_result;
    private boolean is_intent_inprogress;
    private int request_code;
    private CallbackManager callbackManager;
    App_Preference mPreference;

    @InjectView(R.id.tv_policy)
    TextView txt_Policy;

    @OnClick(R.id.ll_fbLogin)
    public void onFbLoginClick() {
        loginWithFacebook();
    }

    @OnClick(R.id.ll_gPlusLogin)
    public void onGPlusLoginClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, SIGN_IN_CODE);
    }

    @OnClick(R.id.ll_emailLogin)
    public void onLoginViaEmailClick() {
        if (mChangeFragmentListener != null) {
            mChangeFragmentListener.onReplaceFragment(ContinueWithEmailFragment.newInstance(), TAG);
        }
    }

    @OnClick(R.id.tv_skip)
    public void onSkipAndBrowse() {
        Intent mHomeIntent = new Intent(getActivity(), HomeActivity.class);
        startActivity(mHomeIntent);
        getActivity().finish();
    }

    public static LoginOptionFragment newInstance() {
        return new LoginOptionFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (SNB_Application) getActivity().getApplicationContext();
        buidNewGoogleApiClient();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_option, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        App_Preference.newInstance(getActivity()).setShowLocation();

        callbackManager = CallbackManager.Factory.create();
        SpannableString spanString = new SpannableString(getResources().getString(R.string.Tag_TermsAndPolicyText));
        ClickableSpan clickableSpanRegister = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
            }
        };
        spanString.setSpan(clickableSpanRegister, 41, 57, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ClickableSpan clickableSpanLogin = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
            }
        };

        spanString.setSpan(clickableSpanLogin, 62, 76, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), 15, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_Policy.setText(spanString);
    }

    private void loginWithFacebook() {

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("Facebook", "onSuccess : " + loginResult.getRecentlyGrantedPermissions());
                        Log.d("Facebook", "onSuccess Access Token : " + loginResult.getAccessToken());
                        retrieveFacebookUserData(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("Facebook", "onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("Facebook", "FacebookException: " + exception.getMessage());
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void retrieveFacebookUserData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        Log.d("Facebook", "JSONObject: " + object.toString());
                        Log.d("Facebook", "GraphResponse: " + response.toString());
                        parseFacebookUserResponse(object);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void parseFacebookUserResponse(JSONObject object) {
        try {

            String firstName = "";
            String lastName = "";

            String[] splited = object.optString("name").split("\\s+");
            if (splited.length > 1) {
                for (int i = 0; i < splited.length; i++) {
                    if (i == splited.length - 1) {
                        lastName = splited[i];
                    } else {
                        firstName = firstName + splited[i];
                    }
                }
            } else {
                firstName = object.optString("name");
            }

            callLoginApi("fbid", object.optString("id"), firstName, lastName, object.optString("email"));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void buidNewGoogleApiClient() {
// Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            google_api_availability.getErrorDialog(getActivity(), result.getErrorCode(), request_code).show();
            return;
        }

        if (!is_intent_inprogress) {
            connection_result = result;
            resolveSignInError();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /*
      Method to resolve any signin errors
     */

    private void resolveSignInError() {
        if (connection_result.hasResolution()) {
            try {
                is_intent_inprogress = true;
                connection_result.startResolutionForResult(getActivity(), SIGN_IN_CODE);
                Log.d("Google", "sign in error resolved");
            } catch (IntentSender.SendIntentException e) {
                is_intent_inprogress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Google", "onActivityResult");
//         Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.d("FACEBOOK", "onActivityResult");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
         get user's information name, email
         */
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("GOOGLE", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            Log.d("GOOGLE", "successfully:");
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("GOOGLE", "DisplayID(): " + acct.getId());
            Log.d("GOOGLE", "DisplayEmail(): " + acct.getEmail());
            Log.d("GOOGLE", "DisplayName(): " + acct.getDisplayName());

            String firstName = "";
            String lastName = "";

            String[] splited = acct.getDisplayName().split("\\s+");
            if (splited.length > 1) {
                for (int i = 0; i < splited.length; i++) {
                    if (i == splited.length - 1) {
                        lastName = splited[i];
                    } else {
                        firstName = firstName + splited[i];
                    }
                }
            } else {
                firstName = acct.getDisplayName();
            }
            callLoginApi("googleId", acct.getId(), firstName, lastName, acct.getEmail());

        } else {
            Log.d("GOOGLE", "unauthenticated:");
            // Signed out, show unauthenticated UI.
        }
    }

    private void callLoginApi(String id_type, String id, String name, String lname, String email) {
        try {
            JSONObject mRequestObj = new JSONObject();
            mRequestObj.put("api_secure_key", App_Constant.API_SECURE_KEY);
            mRequestObj.put(id_type, id);
            mRequestObj.put("email", email);
            mRequestObj.put("fname", name);
            mRequestObj.put("lname", lname);
            String str_request = mRequestObj.toString();
            OnReceiveApiResult onReceive = this;
            PostParamAsync mProductAsync = new PostParamAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.USER_LOGIN_API, "Loading...", str_request);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        User user = ResponseParser.getUserData(ApiResult);
        if (user != null) {
            App_Preference.newInstance(getActivity()).setUserData(user);

            Intent mHomeIntent = new Intent(getActivity(), HomeActivity.class);
            startActivity(mHomeIntent);
            getActivity().finish();
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }


}
