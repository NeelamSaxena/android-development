package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.adapter.ItemsAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.custom.RecyclerItemClickListener;
import com.shopnearby.modal.ItemModal;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.DividerItemDecoration;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.shopnearby.HomeActivity.mPageCount;
import static com.shopnearby.HomeActivity.mPageSize;
import static com.shopnearby.HomeActivity.sortPosition;
import static com.shopnearby.constant.App_Constant.TotalResults;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentCategory;
import static com.shopnearby.constant.App_Constant.currentCateogryIDFilter;
import static com.shopnearby.constant.App_Constant.currentRadius;
import static com.shopnearby.constant.App_Constant.currentSortFilter;
import static com.shopnearby.constant.App_Constant.currentStoreIDFilter;
import static com.shopnearby.constant.App_Constant.min_price;

/**
 * Created by ankit on 19/5/16.
 */
public class ItemFragment extends Fragment implements OnReceiveApiResult, SwipeRefreshLayout.OnRefreshListener {

    private String TAG = "ItemFragment";

    private ArrayList<ItemModal> mItemList;
    private OnChangeFragment mChangeFragmentListener;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    private ItemsAdapter mItemAdapter;

    @InjectView(R.id.swipe_refresh_layout_current)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectView(R.id.tv_noItems)
    TextView noItemsTextView;

    @InjectView(R.id.rv_items)
    RecyclerView mRecyclerViewItems;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static ItemFragment newInstance() {

//        Bundle args = new Bundle();
//        args.putString("Cid", Cid);
//
//        ItemFragment mFragment = new ItemFragment();
//        mFragment.setArguments(args);
        return new ItemFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItemList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_items, container, false);
        ButterKnife.inject(this, view);

//        if (getArguments() != null) {
//            App_Constant.currentCateogryIDFilter = getArguments().getString("Cid");
//        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (mItemList != null && mItemList.size() > 0) {
            setItemsAdapter();
        } else {
            loading = true;
            callProductListApi(true);
        }
    }

    public void callProductListApi(boolean show) {
        try {
            String str_request;
            Log.d(TAG, "currentSortFilter: " +  currentSortFilter);
            Log.d(TAG, "currentBrandsFilter: " + currentBrandsFilter);
            Log.d(TAG, "currentRadius: " + currentRadius);
            Log.d(TAG, "currentStoreIDFilter: " + currentStoreIDFilter);
            Log.d(TAG, "currentCateogryIDFilter: " + currentCateogryIDFilter);
            Log.d(TAG, "App_Constant.max_price. " + App_Constant.max_price.toString());
            Log.d(TAG, "App_Constant.min_price " + App_Constant.min_price);
            str_request = App_Constant.URL.PRODUCT_LIST_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&lat=" + App_Constant.sCurrentLat + "&long=" + App_Constant.sCurrentLng + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&radius=" + currentRadius+ currentSortFilter;

            if (!currentBrandsFilter.equalsIgnoreCase("")) {
                str_request=str_request+"&brand="+currentBrandsFilter;
            }

            if (!App_Constant.min_price.equalsIgnoreCase("")) {
                str_request=str_request+"&min_price="+App_Constant.min_price.replace(",","").trim();
            }

            if (!App_Constant.max_price.equalsIgnoreCase("")) {
                str_request=str_request+"&max_price="+App_Constant.max_price.toString().replace(",","").trim();
            }

            if (!currentCateogryIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&category="+currentCateogryIDFilter;;
            }



            if (!currentStoreIDFilter.equalsIgnoreCase("")) {
                str_request =str_request+ "&store="+currentStoreIDFilter;;
            }


            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.CHECK_EMAIL_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setItemsAdapter() {

        mItemAdapter = new ItemsAdapter(mItemList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewItems.setLayoutManager(mLayoutManager);
        mRecyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        if (getActivity() != null)
            mRecyclerViewItems.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewItems.setAdapter(mItemAdapter);

        mRecyclerViewItems.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mChangeFragmentListener.onReplaceFragment(ItemDetailFragment.newInstance(mItemList.get(position).getStrId(), mItemList.get(position).getmStoreCount()), TAG);
            }
        }));


        mRecyclerViewItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (totalItemCount > 20) {
                        totalItemCount = totalItemCount - 10;
                    }
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            callProductListApi(false);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {


        ArrayList<ItemModal> mList;
        mList = ResponseParser.getItemsList(ApiResult);

        if (mList != null && mList.size() > 0) {
            Log.d(TAG, "mList Size" +  mList.size());
            loading = true;

            noItemsTextView.setVisibility(View.GONE);
            mRecyclerViewItems.setVisibility(View.VISIBLE);
            if(mPageCount==1){
                mItemList.clear();
                mRecyclerViewItems.invalidate();
            }
            mItemList.addAll(mList);
            mPageCount++;
            mPageSize = 20;
            if (mItemAdapter != null) {
                mItemAdapter.notifyDataSetChanged();
                mRecyclerViewItems.invalidate();
            } else {
                setItemsAdapter();
            }

        } else {
            loading = false;
            Log.d(TAG, "mList Size else part " +  mPageCount);
            if(mPageCount==1){
                mItemList.clear();
                mRecyclerViewItems.invalidate();
            }

            if (mItemList.size() == 0) {
                noItemsTextView.setVisibility(View.VISIBLE);
                mRecyclerViewItems.setVisibility(View.GONE);
            }
        }

        TotalResults = ResponseParser.getTotalResults(ApiResult);

        ((HomeActivity)getActivity()).updateFilterText(false);

    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        mPageCount = 1;
        mPageSize = 40;
        if (mItemList != null) {
            mItemList.clear();
        }
        callProductListApi(true);
    }


}
