package com.shopnearby.fragment;


import static com.shopnearby.HomeActivity.BRANDNAME_LISTSearch;
import static com.shopnearby.HomeActivity.CATEGORYNAME_LISTSearch;
import static com.shopnearby.HomeActivity.FromSearch;
import static com.shopnearby.HomeActivity.STORE_LISTSearch;
import static com.shopnearby.HomeActivity.mPageCount;
import static com.shopnearby.HomeActivity.mPageSize;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Array_Constant;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.modal.StoreModal;
import com.shopnearby.preference.App_Preference;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static com.shopnearby.HomeActivity.BRANDNAME_LIST;
import static com.shopnearby.HomeActivity.CATEGORYNAME_LIST;
import static com.shopnearby.HomeActivity.STORE_LIST;
import static com.shopnearby.HomeActivity.distancePosition;
import static com.shopnearby.HomeActivity.sortPosition;
import static com.shopnearby.HomeActivity.sortPositionSearch;
import static com.shopnearby.constant.App_Constant.FilterRadius;
import static com.shopnearby.constant.App_Constant.FilterSearchSort;
import static com.shopnearby.constant.App_Constant.FilterSort;
import static com.shopnearby.constant.App_Constant.TotalResults;
import static com.shopnearby.constant.App_Constant.currentBrandsFilter;
import static com.shopnearby.constant.App_Constant.currentSearchBrandsFilter;

/**
 * Created by ankit on 4/6/16.
 */
public class FilterFragment extends Fragment {
    private SNB_Application mApplication;
    private String TAG = "FilterFragment";
    private String strFilterText = "";
    private OnChangeFragment mChangeFragmentListener;
    private String currentBrandTempFilter;
    private String currentCategoryTempFilter;
    private String currentStoreTempFilter;

    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        // mApplication.updatetextStatus=false;
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.tv_apply)
    public void onApplyClick() {

        if(FromSearch==1){
            FilterClickFor_MainItem();
        }else if (FromSearch==2){
            FilterClickFor_Search();
        }

        //   reloadItemFragment();

    }


    public void FilterClickFor_MainItem(){
        mPageCount = 1;
        mPageSize = 40;

        if (sortBrandNameText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectBrand))) {
            if (BRANDNAME_LIST != null) {
                for (int i = 0; i < BRANDNAME_LIST.size(); i++) {
                    BRANDNAME_LIST.get(i).setSelected(false);
                }
            }
            currentBrandsFilter = "";
        } else {
            if (App_Array_Constant.Temp_BRANDNAME_LIST != null) {
                BRANDNAME_LIST.clear();
                BRANDNAME_LIST.addAll(App_Array_Constant.Temp_BRANDNAME_LIST);
            }
            App_Constant.currentBrandsFilter = "";
            for (int i = 0; i < BRANDNAME_LIST.size(); i++) {
                if (BRANDNAME_LIST.get(i).isSelected()) {
                    if (App_Constant.currentBrandsFilter.equalsIgnoreCase(""))
                        App_Constant.currentBrandsFilter = BRANDNAME_LIST.get(i).getBrandName();
                    else
                        App_Constant.currentBrandsFilter = App_Constant.currentBrandsFilter + " , " + BRANDNAME_LIST.get(i).getBrandName();
                }
            }
        }
        // When Category Row is Visible Then only We check for Selected Category
        if(llCategoryParent.getVisibility()==View.VISIBLE){

            if (sortCategoryNameText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectCategory))) {

                if (CATEGORYNAME_LIST != null) {
                    for (int i = 0; i < CATEGORYNAME_LIST.size(); i++) {
                        CATEGORYNAME_LIST.get(i).setSelected(false);
                        for (int j = 0; j < CATEGORYNAME_LIST.get(i).getChildArray().size(); j++) {
                            CATEGORYNAME_LIST.get(i).getChildArray().get(j).setSelected(false);
                        }
                    }
                }
                App_Constant.currentCateogryFilter = "";
                App_Constant.currentCateogryIDFilter = "";

            } else if (App_Array_Constant.Temp_Category_LIST != null) {
                CATEGORYNAME_LIST.clear();
                CATEGORYNAME_LIST.addAll(App_Array_Constant.Temp_Category_LIST);
            }

            App_Constant.currentCateogryFilter = "";
            for (int i = 0; i < CATEGORYNAME_LIST.size(); i++) {
                if (CATEGORYNAME_LIST.get(i).isSelected()) {
                    if (App_Constant.currentCateogryFilter.equalsIgnoreCase("")) {
                        App_Constant.currentCateogryFilter = CATEGORYNAME_LIST.get(i).getName();
                        App_Constant.currentCateogryIDFilter = CATEGORYNAME_LIST.get(i).getId();
                    } else {
                        App_Constant.currentCateogryFilter = App_Constant.currentCateogryFilter + " , " + CATEGORYNAME_LIST.get(i).getName();
                        App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + CATEGORYNAME_LIST.get(i).getId();
                    }
                }
                Log.d(TAG, "Parent " + App_Constant.currentCateogryFilter);
                //else{
                for (int j = 0; j < CATEGORYNAME_LIST.get(i).getChildArray().size(); j++) {
                    if (CATEGORYNAME_LIST.get(i).getChildArray().get(j).isSelected()) {
                        if (App_Constant.currentCateogryFilter.equalsIgnoreCase("")) {
                            App_Constant.currentCateogryFilter = CATEGORYNAME_LIST.get(i).getChildArray().get(j).getName();
                            App_Constant.currentCateogryIDFilter = CATEGORYNAME_LIST.get(i).getChildArray().get(j).getId();
                        } else {
                            App_Constant.currentCateogryFilter = App_Constant.currentCateogryFilter + "," + CATEGORYNAME_LIST.get(i).getChildArray().get(j).getName();
                            App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + CATEGORYNAME_LIST.get(i).getChildArray().get(j).getId();
                        }
                    }

                }
                //}
                Log.d(TAG, "Child " + App_Constant.currentCateogryIDFilter);
            }


        }



        if (tvStoreName.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectStore))) {
            Log.d(TAG, "When store is not selected " );
            App_Constant.currentStoreFilter = "";
            App_Constant.currentStoreIDFilter = "";
            if (STORE_LIST != null) {
                for (int i = 0; i < STORE_LIST.size(); i++) {
                    STORE_LIST.get(i).setSelected(false);
                }
            }
        } else if (App_Array_Constant.Temp_STORE_LIST != null &&App_Array_Constant.Temp_STORE_LIST.size()>0) {
            Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST " + App_Array_Constant.Temp_STORE_LIST.size());
            STORE_LIST.clear();
            STORE_LIST.addAll(App_Array_Constant.Temp_STORE_LIST);

            App_Constant.currentStoreFilter = "";
            App_Constant.currentStoreIDFilter="";
            for (int i = 0; i < STORE_LIST.size(); i++) {
                if (STORE_LIST.get(i).isSelected()) {
                    if (App_Constant.currentStoreFilter.equalsIgnoreCase("")) {
                        App_Constant.currentStoreFilter = STORE_LIST.get(i).getStoreName();
                        App_Constant.currentStoreIDFilter = STORE_LIST.get(i).getStoreId();
                    } else {
                        App_Constant.currentStoreFilter = App_Constant.currentStoreFilter + ", " + STORE_LIST.get(i).getStoreName();
                        App_Constant.currentStoreIDFilter = App_Constant.currentStoreIDFilter + "," + STORE_LIST.get(i).getStoreId();
                    }
                }
            }
        }else{
            Log.d(TAG, "In last step of Store " );
        }

        Log.d(TAG, "App_Constant.currentStoreFilter " +App_Constant.currentStoreFilter);

        Log.d(TAG, "App_Constant.currentStoreIDFilter " +App_Constant.currentStoreIDFilter);
        // if(tvPricevalue.getText().toString().equalsIgnoreCase(getResources().getString(R.string.TAG_Se)))

        if (mApplication.TempLat != null && !mApplication.TempLat.equalsIgnoreCase("")) {
            App_Constant.sCurrentLat = Double.valueOf(mApplication.TempLat);
            App_Constant.sCurrentLng = Double.valueOf(mApplication.TempLong);
            App_Constant.sCurrentAddress = mApplication.TempAddress;
            App_Preference.newInstance(getActivity()).setLocation(String.valueOf(App_Constant.sCurrentLat), String.valueOf(App_Constant.sCurrentLng), App_Constant.sCurrentAddress);
            mApplication.TempLat = "";
            mApplication.TempLong = "";
            mApplication.TempAddress = "";


        }
        if (mApplication.TempsortPosition != -1) {

            switch (mApplication.TempsortPosition) {
                case 0:
                    FilterSort = "Best Rated";
                    App_Constant.currentSortFilter = "&ratingSort=desc";
                    break;
                case 1:
                    FilterSort = "Lowest Price";
                    App_Constant.currentSortFilter = "&priceSort=asc";
                    break;
                case 2:
                    FilterSort = "Highest Price";
                    App_Constant.currentSortFilter = "&priceSort=desc";
                    break;
                case 3:
                    FilterSort = "A to Z";
                    App_Constant.currentSortFilter = "&titleSort=asc";
                    break;
                case 4:
                    FilterSort = "Z to A";
                    App_Constant.currentSortFilter = "&titleSort=desc";
                    break;
            }


            sortPosition = mApplication.TempsortPosition;
            mApplication.TempsortPosition = -1;
        }
//            else  if(App_Constant.currentSortFilter.equalsIgnoreCase("")){
//                sortPosition = 0;
//                App_Constant.currentSortFilter = "&ratingSort=desc";
//            }

        App_Preference.newInstance(getActivity()).setSort(App_Constant.currentSortFilter, FilterSort, sortPosition);
        if (mApplication.TempdistancePosition != -1) {


            switch (mApplication.TempdistancePosition) {
                case 0:
                    distancePosition = 0;
                    FilterRadius = getResources().getString(R.string.Tag_5miles);
                    App_Constant.currentRadius = "5mi";
                    break;
                case 1:
                    distancePosition = 1;
                    FilterRadius = getResources().getString(R.string.Tag_20miles);
                    App_Constant.currentRadius = "20mi";
                    break;
                case 2:
                    distancePosition = 2;
                    FilterRadius = getResources().getString(R.string.Tag_50miles);
                    App_Constant.currentRadius = "50mi";
                    break;
                case 3:
                    distancePosition = 3;
                    FilterRadius = getResources().getString(R.string.Tag_100miles);
                    App_Constant.currentRadius = "100mi";
                    break;
            }

            mApplication.TempdistancePosition = -1;
            mApplication.TempcurrentD = "";
        }


        App_Preference.newInstance(getActivity()).setDistanceSort(App_Constant.currentRadius, FilterRadius, distancePosition);

        if (mApplication.TempMaxPrice != null && !mApplication.TempMaxPrice.equalsIgnoreCase("")) {
            App_Constant.max_price = mApplication.TempMaxPrice;
            mApplication.TempMaxPrice = "";
        }
        if (mApplication.TempMinPrice != null && !mApplication.TempMinPrice.equalsIgnoreCase("")) {
            App_Constant.min_price = mApplication.TempMinPrice;
            mApplication.TempMinPrice = "";
        } else {
            App_Constant.min_price = "";
        }

        ((HomeActivity) getActivity()).updateFilterText(true);

    }

    public void FilterClickFor_Search(){

       // mPageCount = 1;
       // mPageSize = 40;

        if (sortBrandNameText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectBrand))) {
            if (BRANDNAME_LISTSearch != null) {
                for (int i = 0; i < BRANDNAME_LISTSearch.size(); i++) {
                    BRANDNAME_LISTSearch.get(i).setSelected(false);
                }
            }
            currentSearchBrandsFilter = "";
        } else {
            if (App_Array_Constant.Temp_BRANDNAME_LIST != null) {
                BRANDNAME_LISTSearch.clear();
                BRANDNAME_LISTSearch.addAll(App_Array_Constant.Temp_BRANDNAME_LIST);
            }
            App_Constant.currentSearchBrandsFilter = "";
            for (int i = 0; i < BRANDNAME_LISTSearch.size(); i++) {
                if (BRANDNAME_LIST.get(i).isSelected()) {
                    if (App_Constant.currentSearchBrandsFilter.equalsIgnoreCase(""))
                        App_Constant.currentSearchBrandsFilter = BRANDNAME_LISTSearch.get(i).getBrandName();
                    else
                        App_Constant.currentSearchBrandsFilter = App_Constant.currentSearchBrandsFilter + " , " + BRANDNAME_LISTSearch.get(i).getBrandName();
                }
            }
        }
        // When Category Row is Visible Then only We check for Selected Category
        if(llCategoryParent.getVisibility()==View.VISIBLE){

            if (sortCategoryNameText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectCategory))) {

                if (CATEGORYNAME_LISTSearch != null) {
                    for (int i = 0; i < CATEGORYNAME_LISTSearch.size(); i++) {
                        CATEGORYNAME_LISTSearch.get(i).setSelected(false);
                        for (int j = 0; j < CATEGORYNAME_LISTSearch.get(i).getChildArray().size(); j++) {
                            CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).setSelected(false);
                        }
                    }
                }
                App_Constant.currentSearchCateogryFilter = "";
                App_Constant.currentSearchCateogryIDFilter = "";

            } else if (App_Array_Constant.Temp_Category_LIST != null) {
                CATEGORYNAME_LISTSearch.clear();
                CATEGORYNAME_LISTSearch.addAll(App_Array_Constant.Temp_Category_LIST);
            }

            App_Constant.currentSearchCateogryFilter = "";
            for (int i = 0; i < CATEGORYNAME_LISTSearch.size(); i++) {
                if (CATEGORYNAME_LISTSearch.get(i).isSelected()) {
                    if (App_Constant.currentSearchCateogryFilter.equalsIgnoreCase("")) {
                        App_Constant.currentSearchCateogryFilter = CATEGORYNAME_LISTSearch.get(i).getName();
                        App_Constant.currentSearchCateogryFilter = CATEGORYNAME_LISTSearch.get(i).getId();
                    } else {
                        App_Constant.currentSearchCateogryFilter = App_Constant.currentSearchCateogryFilter + " , " + CATEGORYNAME_LISTSearch.get(i).getName();
                        App_Constant.currentSearchCateogryIDFilter = App_Constant.currentSearchCateogryIDFilter + "," + CATEGORYNAME_LISTSearch.get(i).getId();
                    }
                }
                Log.d(TAG, "Parent " + App_Constant.currentSearchCateogryFilter);
                //else{
                for (int j = 0; j < CATEGORYNAME_LISTSearch.get(i).getChildArray().size(); j++) {
                    if (CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).isSelected()) {
                        if (App_Constant.currentSearchCateogryFilter.equalsIgnoreCase("")) {
                            App_Constant.currentSearchCateogryFilter = CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).getName();
                            App_Constant.currentSearchCateogryIDFilter = CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).getId();
                        } else {
                            App_Constant.currentSearchCateogryFilter = App_Constant.currentSearchCateogryFilter + "," + CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).getName();
                            App_Constant.currentSearchCateogryIDFilter = App_Constant.currentSearchCateogryIDFilter + "," + CATEGORYNAME_LISTSearch.get(i).getChildArray().get(j).getId();
                        }
                    }

                }
                //}
                Log.d(TAG, "Child " + App_Constant.currentSearchCateogryIDFilter);
            }


        }



        if (tvStoreName.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Tag_SelectStore))) {
            Log.d(TAG, "When store is not selected " );
            App_Constant.currentSearchStoreFilter = "";
            App_Constant.currentSearchStoreIDFilter = "";
            if (STORE_LISTSearch != null) {
                for (int i = 0; i < STORE_LISTSearch.size(); i++) {
                    STORE_LISTSearch.get(i).setSelected(false);
                }
            }
        } else if (App_Array_Constant.Temp_STORE_LIST != null &&App_Array_Constant.Temp_STORE_LIST.size()>0) {
            Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST " + App_Array_Constant.Temp_STORE_LIST.size());
            STORE_LISTSearch.clear();
            STORE_LISTSearch.addAll(App_Array_Constant.Temp_STORE_LIST);

            App_Constant.currentSearchStoreFilter = "";
            App_Constant.currentSearchStoreIDFilter="";
            for (int i = 0; i < STORE_LISTSearch.size(); i++) {
                if (STORE_LISTSearch.get(i).isSelected()) {
                    if (App_Constant.currentSearchStoreFilter.equalsIgnoreCase("")) {
                        App_Constant.currentSearchStoreFilter = STORE_LISTSearch.get(i).getStoreName();
                        App_Constant.currentSearchStoreIDFilter = STORE_LISTSearch.get(i).getStoreId();
                    } else {
                        App_Constant.currentSearchStoreFilter = App_Constant.currentSearchStoreFilter + ", " + STORE_LISTSearch.get(i).getStoreName();
                        App_Constant.currentSearchStoreIDFilter = App_Constant.currentSearchStoreIDFilter + "," + STORE_LISTSearch.get(i).getStoreId();
                    }
                }
            }
        }else{
            Log.d(TAG, "In last step of Store " );
        }

        Log.d(TAG, "App_Constant.currentStoreFilter " +App_Constant.currentSearchStoreFilter);

        Log.d(TAG, "App_Constant.currentStoreIDFilter " +App_Constant.currentSearchStoreIDFilter);
        // if(tvPricevalue.getText().toString().equalsIgnoreCase(getResources().getString(R.string.TAG_Se)))

        if (mApplication.TempLat != null && !mApplication.TempLat.equalsIgnoreCase("")) {
            App_Constant.sCurrentLat = Double.valueOf(mApplication.TempLat);
            App_Constant.sCurrentLng = Double.valueOf(mApplication.TempLong);
            App_Constant.sCurrentAddress = mApplication.TempAddress;
       //     App_Preference.newInstance(getActivity()).setLocation(String.valueOf(App_Constant.sCurrentLat), String.valueOf(App_Constant.sCurrentLng), App_Constant.sCurrentAddress);
            mApplication.TempLat = "";
            mApplication.TempLong = "";
            mApplication.TempAddress = "";


        }
        if (mApplication.TempsortPosition != -1) {

            switch (mApplication.TempsortPosition) {
                case 0:
                    FilterSearchSort = "Best Rated";
                    App_Constant.currentSearchSortFilter = "&ratingSort=desc";
                    break;

                case 1:
                    FilterSearchSort = "Best Matched";
                 //   App_Constant.currentSearchSortFilter = "&ratingSort=desc";
                    break;
                case 2:
                    FilterSearchSort = "Lowest Price";
                    App_Constant.currentSearchSortFilter = "&priceSort=asc";
                    break;
                case 3:
                    FilterSearchSort = "Highest Price";
                    App_Constant.currentSearchSortFilter = "&priceSort=desc";
                    break;
                case 4:
                    FilterSearchSort = "A to Z";
                    App_Constant.currentSearchSortFilter = "&titleSort=asc";
                    break;
                case 5:
                    FilterSearchSort = "Z to A";
                    App_Constant.currentSearchSortFilter = "&titleSort=desc";
                    break;
            }


            sortPositionSearch = mApplication.TempsortPosition;
            mApplication.TempsortPosition = -1;
        }
//            else  if(App_Constant.currentSortFilter.equalsIgnoreCase("")){
//                sortPosition = 0;
//                App_Constant.currentSortFilter = "&ratingSort=desc";
//            }

     //   App_Preference.newInstance(getActivity()).setSort(App_Constant.currentSortFilter, FilterSort, sortPosition);
        if (mApplication.TempdistancePosition != -1) {


            switch (mApplication.TempdistancePosition) {
                case 0:
                    distancePosition = 0;
                    FilterRadius = getResources().getString(R.string.Tag_5miles);
                    App_Constant.currentRadius = "5mi";
                    break;
                case 1:
                    distancePosition = 1;
                    FilterRadius = getResources().getString(R.string.Tag_20miles);
                    App_Constant.currentRadius = "20mi";
                    break;
                case 2:
                    distancePosition = 2;
                    FilterRadius = getResources().getString(R.string.Tag_50miles);
                    App_Constant.currentRadius = "50mi";
                    break;
                case 3:
                    distancePosition = 3;
                    FilterRadius = getResources().getString(R.string.Tag_100miles);
                    App_Constant.currentRadius = "100mi";
                    break;
            }

            mApplication.TempdistancePosition = -1;
            mApplication.TempcurrentD = "";
        }


        App_Preference.newInstance(getActivity()).setDistanceSort(App_Constant.currentRadius, FilterRadius, distancePosition);

        if (mApplication.TempMaxPrice != null && !mApplication.TempMaxPrice.equalsIgnoreCase("")) {
            App_Constant.max_priceSearch = mApplication.TempMaxPrice;
            mApplication.TempMaxPrice = "";
        }
        if (mApplication.TempMinPrice != null && !mApplication.TempMinPrice.equalsIgnoreCase("")) {
            App_Constant.min_priceSearch = mApplication.TempMinPrice;
            mApplication.TempMinPrice = "";
        } else {
            App_Constant.min_price = "";
        }

        ((HomeActivity) getActivity()).updateFilterTextSearch(true);

    }

    @InjectView(R.id.categoryView)
    View CategoryDivider;


    @InjectView(R.id.llcategoryParent)
    RelativeLayout llCategoryParent;


    @InjectView(R.id.tv_storeNames)
    TextView tvStoreName;

    @InjectView(R.id.clearStores)
    TextView clearStores;

    @InjectView(R.id.tv_locationName)
    TextView tvLocationName;

//    @InjectView(R.id.tv_totalItemCount)
//    TextView totalResultsTextview;

    @InjectView(R.id.price_value)
    TextView tvPricevalue;

    @InjectView(R.id.tv_distanceName)
    TextView distanceFilterTextview;

//    @InjectView(R.id.tv_sortName)
//    TextView sortFilterNameText;

    @InjectView(R.id.tv_brandNames)
    TextView sortBrandNameText;

    @InjectView(R.id.tv_categoryNames)
    TextView sortCategoryNameText;

    @InjectView(R.id.clearBrand)
    TextView clearBrands;

    @InjectView(R.id.clearCategory)
    TextView clearCategory;

    @InjectView(R.id.clearPrice)
    TextView clearPrices;

    @InjectView(R.id.rd_br)
    RadioButton RD_Br;

    @InjectView(R.id.rd_bm)
    RadioButton RD_BM;

    @InjectView(R.id.rd_lp)
    RadioButton RD_LP;

    @InjectView(R.id.rd_hp)
    RadioButton RD_HP;

    @InjectView(R.id.rd_az)
    RadioButton RD_AZ;

    @InjectView(R.id.rd_za)
    RadioButton RD_ZA;


    @InjectView(R.id.sort_rdg)
    RadioGroup RG_Sort;


    @OnClick(R.id.clearBrand)
    public void clearBrandFilters() {

        sortBrandNameText.setText(getResources().getString(R.string.Tag_SelectBrand));
        if (App_Array_Constant.Temp_BRANDNAME_LIST != null) {
            for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                App_Array_Constant.Temp_BRANDNAME_LIST.get(i).setSelected(false);
            }
        }
        //currentBrandsFilter = "";
        clearBrands.setVisibility(View.GONE);
//        if (getActivity() != null)
//            ((HomeActivity) getActivity()).updateFilterText(true);
    }

    @OnClick(R.id.clearCategory)
    public void clearCategoryFilters() {

        sortCategoryNameText.setText(getResources().getString(R.string.Tag_SelectCategory));
        if (App_Array_Constant.Temp_Category_LIST != null) {
            for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                App_Array_Constant.Temp_Category_LIST.get(i).setSelected(false);
                for (int j = 0; j < App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size(); j++) {
                    App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).setSelected(false);
                }
            }
        }
        // App_Constant.currentCateogryFilter = "";
        //    App_Constant.currentCateogryIDFilter = "";
        clearCategory.setVisibility(View.GONE);
//        if (getActivity() != null)
//            ((HomeActivity) getActivity()).updateFilterText(true);
    }

    @OnClick(R.id.clearStores)
    public void clearStoreFilters() {
//        App_Constant.currentStoreFilter = "";
//        App_Constant.currentStoreIDFilter = "";
        if (App_Array_Constant.Temp_STORE_LIST != null) {

            for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                App_Array_Constant.Temp_STORE_LIST.get(i).setSelected(false);
            }
        }
        tvStoreName.setText(getResources().getString(R.string.Tag_SelectStore));
        clearStores.setVisibility(View.GONE);
//        if (getActivity() != null)
//            ((HomeActivity) getActivity()).updateFilterText(true);
    }


    @OnClick(R.id.clearPrice)
    public void clearPriceFilters() {


    }

    @OnClick(R.id.rl_location)
    public void goToLocationFilter() {
        //   mChangeFragmentListener.onReplaceFilterFragment(LocationFilterFragment.newInstance(), "SUB_FILTER");
        mChangeFragmentListener.onReplaceFragment(LocationFilterFragment.newInstance(), "SUB_FILTER");
    }

    @OnClick(R.id.rl_distance)
    public void goToDistanceFilter() {
        mChangeFragmentListener.onReplaceFragment(DistanceFilterFragment.newInstance(), "SUB_FILTER");
    }

    @OnClick(R.id.ll_brand)
    public void goToBrandFilter() {
        mChangeFragmentListener.onReplaceFragment(BrandFilterFragment.newInstance(), "SUB_FILTER");
    }

    @OnClick(R.id.ll_category)
    public void goToCategoryFilter() {
        mChangeFragmentListener.onReplaceFragment(CategoryFilterFragment.newInstance(), "SUB_FILTER");
    }

    @OnClick(R.id.ll_price)
    public void goToPriceFilter() {
        mChangeFragmentListener.onReplaceFragment(PriceFragment.newInstance(), "SUB_FILTER");
    }

    @OnClick(R.id.ll_stores)
    public void goToStoreFilter() {
        mChangeFragmentListener.onReplaceFragment(Store_Filter.newInstance(), "SUB_FILTER");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
    }

    public static FilterFragment newInstance() {
        Bundle bundle = new Bundle();
//        bundle.putString("RESULT", total);
        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_view, container, false);
        mApplication = SNB_Application.getApplicationInstance(getContext());
        ButterKnife.inject(this, view);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(FromSearch==1){
            RD_BM.setVisibility(View.GONE);
            setFilterName();
        }else if(FromSearch==2){
            RD_BM.setVisibility(View.VISIBLE);
            setFilterSearchName();
        }


        RG_Sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {


                    case (R.id.rd_br):
                        mApplication.TempsortPosition = 0;
                        break;
                    case (R.id.rd_bm):
                        mApplication.TempsortPosition = 1;
                        break;

                    case (R.id.rd_lp):
                        mApplication.TempsortPosition = 2;
                        break;
                    case (R.id.rd_hp):
                        mApplication.TempsortPosition = 3;
                        break;
                    case (R.id.rd_az):
                        mApplication.TempsortPosition = 4;
                        break;
                    case (R.id.rd_za):
                        mApplication.TempsortPosition = 5;
                        break;
                }
            }
        });
    }

    private void setFilterName() {
        int sPost = -1;
        if (mApplication.TempsortPosition == -1) {
            sPost = sortPosition;
        } else {
            sPost = mApplication.TempsortPosition;
        }
        switch (sPost) {
            case 0:
                RD_Br.setChecked(true);
                FilterSort = getResources().getString(R.string.Tag_BestRated);
                break;
            case 1:
                RD_LP.setChecked(true);
                FilterSort = getResources().getString(R.string.Tag_LowestPriceSmall);
                break;
            case 2:
                RD_HP.setChecked(true);
                FilterSort = getResources().getString(R.string.Tag_HighestPrice);
                break;
            case 3:
                RD_AZ.setChecked(true);
                FilterSort = getResources().getString(R.string.Tag_AtoZ);
                break;
            case 4:
                RD_ZA.setChecked(true);
                FilterSort = getResources().getString(R.string.Tag_ZtoA);
                break;
        }

        int dPost = -1;
        if (mApplication.TempdistancePosition == -1) {
            dPost = distancePosition;
        } else {
            dPost = mApplication.TempdistancePosition;
        }

        switch (dPost) {
            case 0:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_5miles));
                //  FilterRadius = getResources().getString(R.string.Tag_5miles);
                break;
            case 1:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_20miles));
                //     FilterRadius = getResources().getString(R.string.Tag_20miles);
                break;
            case 2:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_50miles));
                //   FilterRadius = getResources().getString(R.string.Tag_50miles);
                break;
            case 3:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_100miles));
                //     FilterRadius = getResources().getString(R.string.Tag_100miles);
                break;
        }


        currentBrandTempFilter = "";
        if (BRANDNAME_LIST != null && BRANDNAME_LIST.size() == 0) {
            //  sortBrandNameText.setText(currentBrandTempFilter);
            if (App_Array_Constant.Temp_BRANDNAME_LIST.size() != 0) {
                for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected()) {
                        if (currentBrandTempFilter.equalsIgnoreCase(""))
                            currentBrandTempFilter = App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                        else
                            currentBrandTempFilter = currentBrandTempFilter + ", " + App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                    }
                }
            } else {
                clearBrands.setVisibility(View.GONE);
            }


        } else {
            if (App_Array_Constant.Temp_BRANDNAME_LIST.size() == 0) {
                App_Array_Constant.Temp_BRANDNAME_LIST.clear();

                if (BRANDNAME_LIST != null && BRANDNAME_LIST.size() > 0) {
                    for (int i = 0; i < BRANDNAME_LIST.size(); i++) {
                        BrandModal modal = new BrandModal();
                        modal.setBrandName(BRANDNAME_LIST.get(i).getBrandName());
                        modal.setSelected(BRANDNAME_LIST.get(i).isSelected());
                        App_Array_Constant.Temp_BRANDNAME_LIST.add(modal);

                        if (BRANDNAME_LIST.get(i).isSelected()) {
                            if (currentBrandTempFilter.equalsIgnoreCase(""))
                                currentBrandTempFilter = BRANDNAME_LIST.get(i).getBrandName();
                            else
                                currentBrandTempFilter = currentBrandTempFilter + ", " + BRANDNAME_LIST.get(i).getBrandName();
                        }
                    }
                    clearBrands.setVisibility(View.VISIBLE);
                }
            } else {

                for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected()) {
                        if (currentBrandTempFilter.equalsIgnoreCase(""))
                            currentBrandTempFilter = App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                        else
                            currentBrandTempFilter = currentBrandTempFilter + ", " + App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                    }
                }
            }
        }

        if (currentBrandTempFilter.equalsIgnoreCase("")) {
            currentBrandTempFilter = getActivity().getResources().getString(R.string.Tag_SelectBrand);
            clearBrands.setVisibility(View.GONE);
        } else {
            clearBrands.setVisibility(View.VISIBLE);
        }
        sortBrandNameText.setText(currentBrandTempFilter);

        currentCategoryTempFilter = "";

        if (CATEGORYNAME_LIST != null && CATEGORYNAME_LIST.size() == 0) {
            if (App_Array_Constant.Temp_Category_LIST.size() != 0) {
                for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_Category_LIST.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }
                    Log.d(TAG, "Parent " + App_Constant.currentCateogryFilter);
                    //else{
                    for (int j = 0; j < App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size(); j++) {
                        if (App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).isSelected()) {
                            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            } else {
                                currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            }
                        }
                        Log.d(TAG, "Child " + App_Constant.currentCateogryFilter);
                    }
                    //}
                }
            }else{

                clearCategory.setVisibility(View.GONE);
            }


        } else {
            if (App_Array_Constant.Temp_Category_LIST != null && App_Array_Constant.Temp_Category_LIST.size() == 0) {

                App_Array_Constant.Temp_Category_LIST.clear();


                for (int i = 0; i < CATEGORYNAME_LIST.size(); i++) {
                    SearchCategoryModal modal = new SearchCategoryModal();
                    modal.setName(CATEGORYNAME_LIST.get(i).getName());
                    modal.setId(CATEGORYNAME_LIST.get(i).getId());
                    modal.setParent(CATEGORYNAME_LIST.get(i).getParent());
                    modal.setSelected(CATEGORYNAME_LIST.get(i).isSelected());

                    if (CATEGORYNAME_LIST.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = CATEGORYNAME_LIST.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + CATEGORYNAME_LIST.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }

                    if (CATEGORYNAME_LIST.get(i).getChildArray().size() > 0) {
                        ArrayList<SearchCategoryModal> ChildArray = new ArrayList<SearchCategoryModal>();

                        for (int k = 0; k < CATEGORYNAME_LIST.get(i).getChildArray().size(); k++) {
                            SearchCategoryModal Cmodal = new SearchCategoryModal();
                            Cmodal.setName(CATEGORYNAME_LIST.get(i).getChildArray().get(k).getName());
                            Cmodal.setId(CATEGORYNAME_LIST.get(i).getChildArray().get(k).getId());
                            Cmodal.setParent(CATEGORYNAME_LIST.get(i).getChildArray().get(k).getParent());
                            Cmodal.setSelected(CATEGORYNAME_LIST.get(i).getChildArray().get(k).isSelected());
                            ChildArray.add(Cmodal);

                            if (CATEGORYNAME_LIST.get(i).getChildArray().get(k).isSelected()) {
                                if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                    currentCategoryTempFilter = CATEGORYNAME_LIST.get(i).getChildArray().get(k).getName();
                                    //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                                } else {
                                    currentCategoryTempFilter = currentCategoryTempFilter + ", " + CATEGORYNAME_LIST.get(i).getChildArray().get(k).getName();
                                    //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                                }
                            }

                        }
                        modal.setChildArray(ChildArray);

                    }

                    App_Array_Constant.Temp_Category_LIST.add(modal);
                }

            } else {
                for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_Category_LIST.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }
                    Log.d(TAG, "Parent " + App_Constant.currentCateogryFilter);
                    //else{
                    for (int j = 0; j < App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size(); j++) {
                        if (App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).isSelected()) {
                            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            } else {
                                currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            }
                        }
                        Log.d(TAG, "Child " + App_Constant.currentCateogryFilter);
                    }
                    //}
                }
            }
        }


        Log.d(TAG, "Child " + App_Constant.currentCateogryFilter);
        if (!App_Constant.currentItemHeader.equalsIgnoreCase("Items")) {
            llCategoryParent.setVisibility(View.GONE);
            CategoryDivider.setVisibility(View.GONE);
        } else {
            CategoryDivider.setVisibility(View.VISIBLE);
            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                currentCategoryTempFilter = getActivity().getResources().getString(R.string.Tag_SelectCategory);
                clearCategory.setVisibility(View.GONE);
            } else {
                clearCategory.setVisibility(View.VISIBLE);
            }
            sortCategoryNameText.setText(currentCategoryTempFilter);
        }


        if (mApplication.TempAddress != null && !mApplication.TempAddress.equalsIgnoreCase("")) {
            tvLocationName.setText(mApplication.TempAddress);
        } else if (App_Constant.sCurrentAddress != null) {
            tvLocationName.setText(App_Constant.sCurrentAddress);
        }

        String PriceText = "";
        if (mApplication.TempMinPrice != null && mApplication.TempMinPrice.length() > 0) {
            PriceText = "$" + mApplication.TempMinPrice;
        } else if (!App_Constant.min_price.equalsIgnoreCase("")) {
            PriceText = "$" + App_Constant.min_price;
        }

        if (mApplication.TempMaxPrice != null && mApplication.TempMaxPrice.length() > 0) {
            if (PriceText.length() > 0) {
                PriceText = PriceText + " - $" + mApplication.TempMaxPrice;
            } else {
                PriceText = " 0$ - $" + mApplication.TempMaxPrice;
            }
        } else if (!App_Constant.max_price.equalsIgnoreCase("")) {
            if (PriceText.length() > 0) {
                PriceText = PriceText + " - $" + App_Constant.max_price;
            } else {
                PriceText = " 0$ - $" + App_Constant.max_price;
            }
        } else {
            PriceText = "Select Price ";
        }

        tvPricevalue.setText(PriceText);


        currentStoreTempFilter = "";
        Log.d(TAG, "STORE_LIST.SIZE" + STORE_LIST.size());
        if (STORE_LIST != null && STORE_LIST.size() == 0) {

            if(App_Array_Constant.Temp_STORE_LIST.size()!=0){
                for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_STORE_LIST.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        }
                    }
                    Log.d(TAG, "currentStoreTempFilter is Selected " + App_Array_Constant.Temp_STORE_LIST.get(i).isSelected());
                }
            }else{
                clearStores.setVisibility(View.GONE);
            }

            Log.d(TAG, "STORE_LIST.SIZE When 0 ");
        } else {
            Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST SIZE  " + App_Array_Constant.Temp_STORE_LIST.size());
            if (App_Array_Constant.Temp_STORE_LIST != null && App_Array_Constant.Temp_STORE_LIST.size() == 0) {
                Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST SIZE  when 0 ");
                for (int i = 0; i < STORE_LIST.size(); i++) {
                    StoreModal modal = new StoreModal();
                    modal.setStoreName(STORE_LIST.get(i).getStoreName());
                    modal.setSelected(STORE_LIST.get(i).isSelected());
                    modal.setStoreId(STORE_LIST.get(i).getStoreId());
                    App_Array_Constant.Temp_STORE_LIST.add(modal);

                    if (STORE_LIST.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = STORE_LIST.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + STORE_LIST.get(i).getStoreName();
                        }
                    }
                }
            } else {
                Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST.SIZE" + App_Array_Constant.Temp_STORE_LIST.size());
                for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_STORE_LIST.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        }
                    }
                    Log.d(TAG, "currentStoreTempFilter is Selected " + App_Array_Constant.Temp_STORE_LIST.get(i).isSelected());
                }
            }

        }
        Log.d(TAG, "currentStoreTempFilter " + currentStoreTempFilter);
        if (currentStoreTempFilter.equalsIgnoreCase("")) {
            currentStoreTempFilter = getActivity().getResources().getString(R.string.Tag_SelectStore);
            clearStores.setVisibility(View.GONE);
        } else {
            clearStores.setVisibility(View.VISIBLE);
        }

        tvStoreName.setText(currentStoreTempFilter);
        // clearStores.setVisibility(View.VISIBLE);
    }


    private void setFilterSearchName() {
        int sPost = -1;
        if (mApplication.TempsortPosition == -1) {
            sPost = sortPositionSearch;
        } else {
            sPost = mApplication.TempsortPosition;
        }
        Log.d(TAG, "mApplication.TempsortPosition " + mApplication.TempsortPosition );
        Log.d(TAG, "sortPositionSearch " +sortPositionSearch);
        Log.d(TAG, "sPost" + sPost);
        switch (sPost) {
            case 0:
                RD_Br.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_BestRated);
                break;
            case 1:
                RD_BM.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_BestMatched);
                break;

            case 2:
                RD_LP.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_LowestPriceSmall);
                break;
            case 3:
                RD_HP.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_HighestPrice);
                break;
            case 4:
                RD_AZ.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_AtoZ);
                break;
            case 5:
                RD_ZA.setChecked(true);
                FilterSearchSort = getResources().getString(R.string.Tag_ZtoA);
                break;
        }

        int dPost = -1;
        if (mApplication.TempdistancePosition == -1) {
            dPost = distancePosition;
        } else {
            dPost = mApplication.TempdistancePosition;
        }

        switch (dPost) {
            case 0:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_5miles));
                //  FilterRadius = getResources().getString(R.string.Tag_5miles);
                break;
            case 1:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_20miles));
                //     FilterRadius = getResources().getString(R.string.Tag_20miles);
                break;
            case 2:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_50miles));
                //   FilterRadius = getResources().getString(R.string.Tag_50miles);
                break;
            case 3:
                distanceFilterTextview.setText(getResources().getString(R.string.Tag_100miles));
                //     FilterRadius = getResources().getString(R.string.Tag_100miles);
                break;
        }


        currentBrandTempFilter = "";
        if (BRANDNAME_LISTSearch != null && BRANDNAME_LISTSearch.size() == 0) {
            //  sortBrandNameText.setText(currentBrandTempFilter);
            if (App_Array_Constant.Temp_BRANDNAME_LIST.size() != 0) {
                for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected()) {
                        if (currentBrandTempFilter.equalsIgnoreCase(""))
                            currentBrandTempFilter = App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                        else
                            currentBrandTempFilter = currentBrandTempFilter + ", " + App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                    }
                }
            } else {
                clearBrands.setVisibility(View.GONE);
            }


        } else {
            if (App_Array_Constant.Temp_BRANDNAME_LIST.size() == 0) {
                App_Array_Constant.Temp_BRANDNAME_LIST.clear();

                if (BRANDNAME_LISTSearch != null && BRANDNAME_LISTSearch.size() > 0) {
                    for (int i = 0; i < BRANDNAME_LISTSearch.size(); i++) {
                        BrandModal modal = new BrandModal();
                        modal.setBrandName(BRANDNAME_LISTSearch.get(i).getBrandName());
                        modal.setSelected(BRANDNAME_LISTSearch.get(i).isSelected());
                        App_Array_Constant.Temp_BRANDNAME_LIST.add(modal);

                        if (BRANDNAME_LISTSearch.get(i).isSelected()) {
                            if (currentBrandTempFilter.equalsIgnoreCase(""))
                                currentBrandTempFilter = BRANDNAME_LISTSearch.get(i).getBrandName();
                            else
                                currentBrandTempFilter = currentBrandTempFilter + ", " + BRANDNAME_LISTSearch.get(i).getBrandName();
                        }
                    }
                    clearBrands.setVisibility(View.VISIBLE);
                }
            } else {

                for (int i = 0; i < App_Array_Constant.Temp_BRANDNAME_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_BRANDNAME_LIST.get(i).isSelected()) {
                        if (currentBrandTempFilter.equalsIgnoreCase(""))
                            currentBrandTempFilter = App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                        else
                            currentBrandTempFilter = currentBrandTempFilter + ", " + App_Array_Constant.Temp_BRANDNAME_LIST.get(i).getBrandName();
                    }
                }
            }
        }

        if (currentBrandTempFilter.equalsIgnoreCase("")) {
            currentBrandTempFilter = getActivity().getResources().getString(R.string.Tag_SelectBrand);
            clearBrands.setVisibility(View.GONE);
        } else {
            clearBrands.setVisibility(View.VISIBLE);
        }
        sortBrandNameText.setText(currentBrandTempFilter);

        currentCategoryTempFilter = "";

        if (CATEGORYNAME_LISTSearch != null && CATEGORYNAME_LISTSearch.size() == 0) {
            if (App_Array_Constant.Temp_Category_LIST.size() != 0) {
                for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_Category_LIST.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }
                    Log.d(TAG, "Parent " + App_Constant.currentSearchCateogryFilter);
                    //else{
                    for (int j = 0; j < App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size(); j++) {
                        if (App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).isSelected()) {
                            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            } else {
                                currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            }
                        }
                        Log.d(TAG, "Child " + App_Constant.currentCateogryFilter);
                    }
                    //}
                }
            }else{

                clearCategory.setVisibility(View.GONE);
            }


        } else {
            if (App_Array_Constant.Temp_Category_LIST != null && App_Array_Constant.Temp_Category_LIST.size() == 0) {

                App_Array_Constant.Temp_Category_LIST.clear();


                for (int i = 0; i < CATEGORYNAME_LISTSearch.size(); i++) {
                    SearchCategoryModal modal = new SearchCategoryModal();
                    modal.setName(CATEGORYNAME_LISTSearch.get(i).getName());
                    modal.setId(CATEGORYNAME_LISTSearch.get(i).getId());
                    modal.setParent(CATEGORYNAME_LISTSearch.get(i).getParent());
                    modal.setSelected(CATEGORYNAME_LISTSearch.get(i).isSelected());

                    if (CATEGORYNAME_LISTSearch.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = CATEGORYNAME_LISTSearch.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + CATEGORYNAME_LISTSearch.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }

                    if (CATEGORYNAME_LISTSearch.get(i).getChildArray().size() > 0) {
                        ArrayList<SearchCategoryModal> ChildArray = new ArrayList<SearchCategoryModal>();

                        for (int k = 0; k < CATEGORYNAME_LISTSearch.get(i).getChildArray().size(); k++) {
                            SearchCategoryModal Cmodal = new SearchCategoryModal();
                            Cmodal.setName(CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).getName());
                            Cmodal.setId(CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).getId());
                            Cmodal.setParent(CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).getParent());
                            Cmodal.setSelected(CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).isSelected());
                            ChildArray.add(Cmodal);

                            if (CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).isSelected()) {
                                if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                    currentCategoryTempFilter = CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).getName();
                                    //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                                } else {
                                    currentCategoryTempFilter = currentCategoryTempFilter + ", " + CATEGORYNAME_LISTSearch.get(i).getChildArray().get(k).getName();
                                    //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                                }
                            }

                        }
                        modal.setChildArray(ChildArray);

                    }

                    App_Array_Constant.Temp_Category_LIST.add(modal);
                }

            } else {
                for (int i = 0; i < App_Array_Constant.Temp_Category_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_Category_LIST.get(i).isSelected()) {
                        if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                            currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        } else {
                            currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getName();
                            //  App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getId();
                        }
                    }
                    Log.d(TAG, "Parent " + App_Constant.currentCateogryFilter);
                    //else{
                    for (int j = 0; j < App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().size(); j++) {
                        if (App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).isSelected()) {
                            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                                currentCategoryTempFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //   App_Constant.currentCateogryIDFilter = App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            } else {
                                currentCategoryTempFilter = currentCategoryTempFilter + ", " + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getName();
                                //    App_Constant.currentCateogryIDFilter = App_Constant.currentCateogryIDFilter + "," + App_Array_Constant.Temp_Category_LIST.get(i).getChildArray().get(j).getId();
                            }
                        }
                        Log.d(TAG, "Child " + App_Constant.currentSearchCateogryFilter);
                    }
                    //}
                }
            }
        }


        Log.d(TAG, "Child " + App_Constant.currentSearchCateogryFilter);
        if (!App_Constant.currentItemHeader.equalsIgnoreCase("Items")) {
            llCategoryParent.setVisibility(View.GONE);
            CategoryDivider.setVisibility(View.GONE);
        } else {
            CategoryDivider.setVisibility(View.VISIBLE);
            if (currentCategoryTempFilter.equalsIgnoreCase("")) {
                currentCategoryTempFilter = getActivity().getResources().getString(R.string.Tag_SelectCategory);
                clearCategory.setVisibility(View.GONE);
            } else {
                clearCategory.setVisibility(View.VISIBLE);
            }
            sortCategoryNameText.setText(currentCategoryTempFilter);
        }


        if (mApplication.TempAddress != null && !mApplication.TempAddress.equalsIgnoreCase("")) {
            tvLocationName.setText(mApplication.TempAddress);
        } else if (App_Constant.sCurrentAddress != null) {
            tvLocationName.setText(App_Constant.sCurrentAddress);
        }

        String PriceText = "";
        if (mApplication.TempMinPrice != null && mApplication.TempMinPrice.length() > 0) {
            PriceText = "$" + mApplication.TempMinPrice;
        } else if (!App_Constant.min_price.equalsIgnoreCase("")) {
            PriceText = "$" + App_Constant.min_price;
        }

        if (mApplication.TempMaxPrice != null && mApplication.TempMaxPrice.length() > 0) {
            if (PriceText.length() > 0) {
                PriceText = PriceText + " - $" + mApplication.TempMaxPrice;
            } else {
                PriceText = " 0$ - $" + mApplication.TempMaxPrice;
            }
        } else if (!App_Constant.max_price.equalsIgnoreCase("")) {
            if (PriceText.length() > 0) {
                PriceText = PriceText + " - $" + App_Constant.max_price;
            } else {
                PriceText = " 0$ - $" + App_Constant.max_price;
            }
        } else {
            PriceText = "Select Price ";
        }

        tvPricevalue.setText(PriceText);


        currentStoreTempFilter = "";
        Log.d(TAG, "STORE_LIST.SIZE" + STORE_LISTSearch.size());
        if (STORE_LISTSearch != null && STORE_LISTSearch.size() == 0) {

            if(App_Array_Constant.Temp_STORE_LIST.size()!=0){
                for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_STORE_LIST.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        }
                    }
                    Log.d(TAG, "currentStoreTempFilter is Selected " + App_Array_Constant.Temp_STORE_LIST.get(i).isSelected());
                }
            }else{
                clearStores.setVisibility(View.GONE);
            }

            Log.d(TAG, "STORE_LIST.SIZE When 0 ");
        } else {
            Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST SIZE  " + App_Array_Constant.Temp_STORE_LIST.size());
            if (App_Array_Constant.Temp_STORE_LIST != null && App_Array_Constant.Temp_STORE_LIST.size() == 0) {
                Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST SIZE  when 0 ");
                for (int i = 0; i < STORE_LISTSearch.size(); i++) {
                    StoreModal modal = new StoreModal();
                    modal.setStoreName(STORE_LISTSearch.get(i).getStoreName());
                    modal.setSelected(STORE_LISTSearch.get(i).isSelected());
                    modal.setStoreId(STORE_LISTSearch.get(i).getStoreId());
                    App_Array_Constant.Temp_STORE_LIST.add(modal);

                    if (STORE_LISTSearch.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = STORE_LISTSearch.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + STORE_LISTSearch.get(i).getStoreName();
                        }
                    }
                }
            } else {
                Log.d(TAG, "App_Array_Constant.Temp_STORE_LIST.SIZE" + App_Array_Constant.Temp_STORE_LIST.size());
                for (int i = 0; i < App_Array_Constant.Temp_STORE_LIST.size(); i++) {
                    if (App_Array_Constant.Temp_STORE_LIST.get(i).isSelected()) {
                        if (currentStoreTempFilter.equalsIgnoreCase("")) {
                            currentStoreTempFilter = App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        } else {
                            currentStoreTempFilter = currentStoreTempFilter + ", " + App_Array_Constant.Temp_STORE_LIST.get(i).getStoreName();
                        }
                    }
                    Log.d(TAG, "currentStoreTempFilter is Selected " + App_Array_Constant.Temp_STORE_LIST.get(i).isSelected());
                }
            }

        }
        Log.d(TAG, "currentStoreTempFilter " + currentStoreTempFilter);
        if (currentStoreTempFilter.equalsIgnoreCase("")) {
            currentStoreTempFilter = getActivity().getResources().getString(R.string.Tag_SelectStore);
            clearStores.setVisibility(View.GONE);
        } else {
            clearStores.setVisibility(View.VISIBLE);
        }

        tvStoreName.setText(currentStoreTempFilter);
        // clearStores.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void updateResultCount() {
        //totalResultsTextview.setText(TotalResults + " results");
    }
}
