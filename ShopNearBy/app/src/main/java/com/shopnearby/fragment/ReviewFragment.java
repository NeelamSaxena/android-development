package com.shopnearby.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shopnearby.R;
import com.shopnearby.adapter.ReviewsAdapter;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.ReviewModal;
import com.shopnearby.utils.DividerItemDecoration;
import com.shopnearby.utils.ResponseParser;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ankit on 2/6/16.
 */
public class ReviewFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "ReviewFragment";
    private String productId = "";
    LinearLayoutManager mLayoutManager;
    private int mPageCount = 1;
    private int mPageSize = 10;
    private ReviewsAdapter mReviewAdapter;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ArrayList<ReviewModal> reviewList;

    @InjectView(R.id.rv_reviews)
    RecyclerView mRecyclerReviews;

    public static ReviewFragment newInstance(String id) {
        Bundle bundle = new Bundle();
        bundle.putString("P_ID", id);
        ReviewFragment mFragment = new ReviewFragment();
        mFragment.setArguments(bundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productId = getArguments().getString("P_ID");
        }
        reviewList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (reviewList != null && reviewList.size() > 0) {
            setReviewsAdapter();
        } else {
            callGetReviewListApi(true);
        }

    }

    private void callGetReviewListApi(boolean show) {
        try {
            String str_request = App_Constant.URL.REVIEWS_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&page=" + mPageCount + "&pageSize=" + mPageSize + "&product=097855086600";
            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.REVIEWS_API, "Loading...", str_request, show);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setReviewsAdapter() {
        mReviewAdapter = new ReviewsAdapter(reviewList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerReviews.setLayoutManager(mLayoutManager);
        mRecyclerReviews.setItemAnimator(new DefaultItemAnimator());
        mRecyclerReviews.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerReviews.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL));
        mRecyclerReviews.setAdapter(mReviewAdapter);

        mRecyclerReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("Ankit", "Last Item Wow !");
                            callGetReviewListApi(false);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        ArrayList<ReviewModal> mList;
        mList = ResponseParser.getReviewsList(ApiResult);
        if (mList != null && mList.size() > 0) {
            loading = true;
            mPageCount++;
            reviewList.addAll(mList);
            if (mReviewAdapter != null) {
                mReviewAdapter.notifyDataSetChanged();
            } else {
                setReviewsAdapter();
            }
        } else {
            loading = false;
        }

    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {

    }
}
