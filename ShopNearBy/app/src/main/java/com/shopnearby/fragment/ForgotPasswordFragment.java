package com.shopnearby.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shopnearby.HomeActivity;
import com.shopnearby.LoginActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.async.GetParamsAsync;
import com.shopnearby.async.PostParamAsync;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.modal.User;
import com.shopnearby.preference.App_Preference;
import com.shopnearby.utils.ResponseParser;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by ankit on 14/5/16.
 */
public class ForgotPasswordFragment extends Fragment implements OnReceiveApiResult {
    private String TAG = "ForgotPasswordFragment";
    private boolean isPasswordVisible = false;
    OnChangeFragment mChangeFragmentListener;
    private String strPassword = "";
    private String strEmail = "";
    private SNB_Application mApplication;

    @OnClick(R.id.img_back)
    public void goBack() {
        getActivity().onBackPressed();
    }

    @InjectView(R.id.tv_show)
    TextView mShowTextView;

    @InjectView(R.id.input_password)
    EditText mPasswordEditText;

    @InjectView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;


//    @OnClick(R.id.tv_show)
//    public void showHidePassword() {
//        if (!isPasswordVisible) {
//            isPasswordVisible = true;
//            mShowTextView.setText("hide");
//            mPasswordEditText.setTransformationMethod(new HideReturnsTransformationMethod());
//        } else {
//            isPasswordVisible = false;
//            mShowTextView.setText("show");
//            mPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());
//        }
//    }

    @InjectView(R.id.ll_email)
    LinearLayout LL_Email;

    @OnClick(R.id.tv_forgot_pass)
    public void onForgotPasswordClick() {
        ForgotPasswordAlert(getActivity());
    }

    @OnClick(R.id.btn_login)
    public void onLoginButtonClick() {
        if (validatePassword()) {
            callLoginApi();
        }
    }

    @InjectView(R.id.tv_pageTitle)
    TextView mPageTitleTextView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (LoginActivity) context;
    }

    public static ForgotPasswordFragment newInstance(String email) {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        Bundle args = new Bundle();
        args.putString("EMAIL", email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (SNB_Application) getActivity().getApplicationContext();
        if (getArguments() != null) {
            strEmail = getArguments().getString("EMAIL");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPageTitleTextView.setText(getString(R.string.Tag_Login));

        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                inputLayoutPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validatePassword() {
        strPassword = mPasswordEditText.getText().toString().trim();
        Log.d(TAG, "Password Length= " + strPassword.length());
        if (strPassword.length() == 0) {
            inputLayoutPassword.setError(getString(R.string.Tag_PleaseEnterPass));
            requestFocus(mPasswordEditText);
            return false;
        } else if (strPassword.length() < 8) {
            inputLayoutPassword.setError(getString(R.string.Tag_PasswordHelp));
            requestFocus(mPasswordEditText);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }


    private void ForgotPasswordAlert(Context mContext) {

        AlertDialog.Builder mAlert = new AlertDialog.Builder(mContext);
        mAlert.setCancelable(false);
        mAlert.setTitle("Forgot Password");
        mAlert.setMessage("Are you sure you want to reset your password?");

        mAlert.setPositiveButton(mContext.getResources().getString(R.string.Tag_Yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CallForgotPasswordApi();
            }
        });

        mAlert.setNegativeButton(mContext.getResources().getString(R.string.Tag_Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mAlert.show();
    }

    private void CallForgotPasswordApi() {
        try {
            String str_request = App_Constant.URL.CHECK_EMAIL_API.toString() + "?api_secure_key=" + App_Constant.API_SECURE_KEY + "&email=" + strEmail;
            OnReceiveApiResult onReceive = this;
            GetParamsAsync mProductAsync = new GetParamsAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.FORGOT_API, "Loading...", str_request,true);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callLoginApi() {
        try {
            JSONObject mRequestObj = new JSONObject();
            mRequestObj.put("api_secure_key", App_Constant.API_SECURE_KEY);
            mRequestObj.put("email", strEmail);
            mRequestObj.put("password", strPassword);
            mRequestObj.put("type", "2");
            String str_request = mRequestObj.toString();
            OnReceiveApiResult onReceive = this;
            PostParamAsync mProductAsync = new PostParamAsync(getActivity(), getActivity(), onReceive, App_Constant.URL.LOGIN_API, "Loading...", str_request);
            mProductAsync.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveResult(App_Constant.URL URL_Type, String ApiResult) {
        User user = ResponseParser.getUserData(ApiResult);
        if (user != null) {
            App_Preference.newInstance(getActivity()).setUserData(user);

            Intent mHomeIntent = new Intent(getActivity(), HomeActivity.class);
            startActivity(mHomeIntent);
            getActivity().finish();
        }
    }

    @Override
    public void onErrorResult(App_Constant.URL URL_Type, String Req_Params, String ApiResult) {
        //        {"success":false,"error_code":2,"msg":"Invalid password"}
//        SNB_Application.ShowAlert(getActivity(), ResponseParser.getResponseMessage(ApiResult));
//        Snackbar.make(LL_Email, "This is a snack bar", Snackbar.LENGTH_LONG)
//                .show();
    }
}
