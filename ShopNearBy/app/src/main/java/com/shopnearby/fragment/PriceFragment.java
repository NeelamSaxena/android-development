package com.shopnearby.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shopnearby.HomeActivity;
import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnChangeFragment;
import com.shopnearby.constant.App_Constant;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by shailendra on 13/6/16.
 */

public class PriceFragment extends Fragment{

    private String TAG = "PriceFragment";
    private OnChangeFragment mChangeFragmentListener;
    private SNB_Application mApplication;

    private DecimalFormat df;
    private DecimalFormat dfnd;
    private boolean hasFractionalPart;

    @InjectView(R.id.tv_Done)
    TextView TvDone;

    @InjectView(R.id.Max_Edit)
    EditText MaxEdit;

    @InjectView(R.id.Min_Edit)
    EditText MinEdit;


    @InjectView(R.id.input_layout_min)
    TextInputLayout inputLayoutMin;

    @InjectView(R.id.input_layout_max)
    TextInputLayout inputLayoutMax;




    @OnClick(R.id.imgBack)
    public void icnBack_Click() {
        // mApplication.updatetextStatus=false;
        hideKeyboardByForce();
        getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @OnClick(R.id.tv_Done)
    public void Done_Click() {
        // mApplication.updatetextStatus=false;
        hideKeyboardByForce();
        if(!(MaxEdit.getText().toString().equalsIgnoreCase(""))){
            if(!(MinEdit.getText().toString().equalsIgnoreCase(""))){
                int minVal=Integer.valueOf(MinEdit.getText().toString().replace(",","").trim());
                int maxVal=Integer.valueOf(MaxEdit.getText().toString().replace(",","").trim());
                if(minVal>maxVal){
                    mApplication.ShowAlert(getActivity(),getActivity().getResources().getString(R.string.Header_minprice_greater_then_max_Body),getActivity().getResources().getString(R.string.Header_minprice_greater_then_max));
                }else if(!(minVal<100000&&minVal>0)){
                    mApplication.ShowAlert(getActivity(), getActivity().getResources().getString(R.string.Header_minprice_range_between0_and_99999_Body),getActivity().getResources().getString(R.string.Header_minprice_range_between0_and_99999));
                }else if(!(maxVal<100001&&maxVal>0)){
                    mApplication.ShowAlert(getActivity(), getActivity().getResources().getString(R.string.Header_maxnprice_range_between1_and_100000_Body),getActivity().getResources().getString(R.string.Header_maxnprice_range_between1_and_100000));
                }else{
                    mApplication.TempMinPrice=MinEdit.getText().toString();
                    mApplication.TempMaxPrice=MaxEdit.getText().toString();
                    getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }else {

                int maxVal=Integer.valueOf(MaxEdit.getText().toString().replace(",","").trim());
                if(!(maxVal<100001&&maxVal>0)){
                    mApplication.ShowAlert(getActivity(), getActivity().getResources().getString(R.string.Header_maxnprice_range_between1_and_100000_Body),getActivity().getResources().getString(R.string.Header_maxnprice_range_between1_and_100000));
                }else{

                    mApplication.TempMaxPrice=MaxEdit.getText().toString();
                    mApplication.TempMinPrice="";
                    getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }


        }else{
            mApplication.ShowAlert(getActivity(), getActivity().getResources().getString(R.string.Header_Please_Enter_Max_Value_of_Price_Body),getActivity().getResources().getString(R.string.Header_Please_Enter_Max_Value_of_Price));
        }


      //  getFragmentManager().popBackStack("SUB_FILTER", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mChangeFragmentListener = (HomeActivity) context;
        df = new DecimalFormat("##,###");
        df.setDecimalSeparatorAlwaysShown(true);
        dfnd = new DecimalFormat("##,###");
        hasFractionalPart = false;
    }


    public static PriceFragment newInstance() {
        Bundle bundle = new Bundle();
        PriceFragment fragment = new PriceFragment();
        fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter_price, container, false);
        ButterKnife.inject(this, view);
        mApplication=SNB_Application.getApplicationInstance(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSelectedPrice();


        MinEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator())))
                {
                    hasFractionalPart = true;
                } else {
                    hasFractionalPart = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                inputLayoutMin.setErrorEnabled(false);
                inputLayoutMin.setError("");

                MinEdit.removeTextChangedListener(this);


                try {
                    int inilen, endlen;
                    inilen = MinEdit.getText().length();

                    String v = editable.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                    Number n = df.parse(v);
                    int cp = MinEdit.getSelectionStart();
                    if (hasFractionalPart) {
                        MinEdit.setText(df.format(n));
                    } else {
                        MinEdit.setText(dfnd.format(n));
                    }
                    endlen = MinEdit.getText().length();
                    int sel = (cp + (endlen - inilen));
                    if (sel > 0 && sel <= MinEdit.getText().length()) {
                        MinEdit.setSelection(sel);
                    } else {
                        // place cursor at the end?
                        MinEdit.setSelection(MinEdit.getText().length() - 1);
                    }
                } catch (NumberFormatException nfe) {
                    // do nothing?
                } catch (ParseException e) {
                    // do nothing?
                }

                MinEdit.addTextChangedListener(this);

//                if(!MinEdit.getText().toString().equalsIgnoreCase("")){
//
//                    validateMinPrice();
//
//
//                }
            }
        });

        MaxEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator())))
                {
                    hasFractionalPart = true;
                } else {
                    hasFractionalPart = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutMax.setErrorEnabled(false);
                inputLayoutMax.setError("");
                MaxEdit.removeTextChangedListener(this);


                try {
                    int inilen, endlen;
                    inilen = MaxEdit.getText().length();

                    String v = editable.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                    Number n = df.parse(v);
                    int cp = MaxEdit.getSelectionStart();
                    if (hasFractionalPart) {
                        MaxEdit.setText(df.format(n));
                    } else {
                        MaxEdit.setText(dfnd.format(n));
                    }
                    endlen = MaxEdit.getText().length();
                    int sel = (cp + (endlen - inilen));
                    if (sel > 0 && sel <= MaxEdit.getText().length()) {
                        MaxEdit.setSelection(sel);
                    } else {
                        // place cursor at the end?
                        MaxEdit.setSelection(MaxEdit.getText().length() - 1);
                    }
                } catch (NumberFormatException nfe) {
                    // do nothing?
                } catch (ParseException e) {
                    // do nothing?
                }

                MaxEdit.addTextChangedListener(this);

              //  validateMaxPrice();
            }
        });

    }

    private void setSelectedPrice() {

        if(mApplication.TempMinPrice.length()>0){
            MinEdit.setText(mApplication.TempMinPrice);
        }else{
            MinEdit.setText(App_Constant.min_price);
        }


        if(mApplication.TempMaxPrice.length()>0){
            MaxEdit.setText(mApplication.TempMaxPrice);
        }else{
            MaxEdit.setText(App_Constant.max_price);
        }



    }

    private boolean validateMaxPrice() {
//         int value = Integer.valueOf(MaxEdit.getText().toString());
//        if (value!="") {
//            inputLayoutEmail.setError(getString(R.string.Tag_InvalidEmail));
//            requestFocus(mEmailEditText);
//            return false;
//        } else {
//           // inputLayoutEmail.setErrorEnabled(false);
//        }

        if(!(MaxEdit.getText().toString().equalsIgnoreCase(""))){
            Log.d(TAG, "Max Edit Text :" + MaxEdit.getText().toString());
            String maxValue=MaxEdit.getText().toString().replace(",","").trim();
            Log.d(TAG, "Max Edit Text After trim :" + maxValue);
            DecimalFormat formatter = new DecimalFormat("##,###");
            String yourFormattedString = formatter.format(Long.valueOf(maxValue));
            Log.d(TAG, "Max Edit Text After yourFormattedString :" + yourFormattedString);
            MaxEdit.setText(yourFormattedString);

            if(!(MinEdit.getText().toString().equalsIgnoreCase(""))){
                int minVal=Integer.valueOf(MinEdit.getText().toString().replace(",","").trim());
                int maxVal=Integer.valueOf(MaxEdit.getText().toString().replace(",","").trim());
                 if(!(maxVal<100001&&maxVal>0)){
                     //inputLayoutEmail.setErrorEnabled(false);
                     inputLayoutMax.setError("Maximum Price range between 1 and 100000");
                     inputLayoutMax.setErrorEnabled(false);
                  //  mApplication.ShowAlert(getActivity(), "Maximum Price range between 1 and 100000");
                }
            }else {

                int maxVal=Integer.valueOf(MaxEdit.getText().toString().replace(",","").trim());
                if(!(maxVal<100001&&maxVal>0)){
                    //inputLayoutEmail.setErrorEnabled(false);
                  //  mApplication.ShowAlert(getActivity(), "Maximum Price range between 1 and 100000");
                    inputLayoutMax.setError("Maximum Price range between 1 and 100000");
                    inputLayoutMax.setErrorEnabled(false);
                }
            }


        }

        return true;
    }

    private boolean validateMinPrice() {


        if(!(MinEdit.getText().toString().equalsIgnoreCase(""))){
            int minVal=Integer.valueOf(MinEdit.getText().toString().replace(",","").trim());

            if(!MaxEdit.getText().toString().equalsIgnoreCase("")){
                int maxVal=Integer.valueOf(MaxEdit.getText().toString().replace(",","").trim());
                if(minVal>maxVal){
                  //  mApplication.ShowAlert(getActivity(), "Minimum Price cannot be greater then Maximum Price");
                    inputLayoutMin.setError(getActivity().getResources().getString(R.string.Header_minprice_greater_then_max_Body));
                    inputLayoutMin.setErrorEnabled(false);
                }else if(!(minVal<99999&&minVal>0)){
                  //  mApplication.ShowAlert(getActivity(), "Minimum Price range between 0 and 99999");
                    inputLayoutMin.setError(getActivity().getResources().getString(R.string.Header_minprice_range_between0_and_99999_Body));
                    inputLayoutMin.setErrorEnabled(false);
                }
            }else {
                if(!(minVal<99999&&minVal>0)){
                 //   mApplication.ShowAlert(getActivity(), "Minimum Price range between 0 and 99999");
                    inputLayoutMin.setError(getActivity().getResources().getString(R.string.Header_minprice_range_between0_and_99999_Body));
                    inputLayoutMin.setErrorEnabled(false);
                }
            }

        }

        return true;
    }

    private void hideKeyboardByForce() {
        try {
            mApplication.hideSoftKeyboard(getActivity(), MaxEdit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
