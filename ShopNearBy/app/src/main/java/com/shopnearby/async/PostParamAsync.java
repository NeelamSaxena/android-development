package com.shopnearby.async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.shopnearby.R;
import com.shopnearby.SNB_Application;
import com.shopnearby.callbacks.OnReceiveApiResult;
import com.shopnearby.constant.App_Constant;
import com.shopnearby.utils.CheckInternet;
import com.shopnearby.utils.Progress_HUD;
import com.shopnearby.utils.ResponseParser;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.util.concurrent.TimeUnit;

/**
 * Created by ankit on 12/5/16.
 */
public class PostParamAsync extends AsyncTask<String, Void, String> {

    /**
     * POST ASYNC Members Declarations
     */
    private final SNB_Application mApplication;
    private Context mContext;
    private Activity mActivity;
    private OnReceiveApiResult mOnReciveServerResponse;
    private String Str_Msg = "Loading...";
    private App_Constant.URL URL_Type;
    //    private ArrayList<SNB_PostModal> arr_PostModels;
    private String Str_RequestParams = "";
    private MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    /**
     * Constructor Implementations
     */
    public PostParamAsync(Context context, Activity activity,
                          OnReceiveApiResult onReciveServerResponse, App_Constant.URL URL_Type, String Msg, String Str_Params) {
        this.mContext = context;
        this.mOnReciveServerResponse = onReciveServerResponse;
        this.mActivity = activity;
        this.URL_Type = URL_Type;
        this.Str_Msg = Msg;
        this.Str_RequestParams = Str_Params;
        mApplication = SNB_Application.getApplicationInstance(mContext);
    }

    /**
     * This Method is called before the execution start
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Progress_HUD.show(mActivity,"Loading...");
//
//        mMaterialDialog = new MaterialDialog.Builder(mContext)
//                .title(Str_Msg)
//                .content("Please Wait...")
//                .progress(true, 0)
//                .cancelable(false)
//                .show();
    }

    /**
     * This Method is called during the execution
     */
    @Override
    protected String doInBackground(String... params) {
        try {

            if (CheckInternet.hasConnection(mActivity)) {
                /**
                 * Establishing Connection with Server
                 */
                OkHttpClient client = new OkHttpClient();
                client.setReadTimeout(30, TimeUnit.SECONDS);
                client.setConnectTimeout(30, TimeUnit.SECONDS);
                /**
                 * Setting up Parameters with the request
                 */
                Log.d("PostAsync", "API URL : " + URL_Type.toString());
                Log.d("PostAsync", "API Request : " + Str_RequestParams);
                RequestBody body = RequestBody.create(JSON, Str_RequestParams);
                Request request = new Request.Builder()
                        .url(URL_Type.toString())
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("PostAsync", "Api Response : " + result);
                return result;
            } else {
                return "No Internet";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Network Error";
        }
    }

    /**
     * This Method is called after the execution finish
     */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.d("PostAsync", "onPostExecute : " + result);
        Progress_HUD.hideProgressDialog(mActivity);
        if (result.equalsIgnoreCase("No Internet")) {
            mApplication.ShowAlert(mContext, "No Internet",mContext.getResources().getString(R.string.app_name));
        } else if (result.equalsIgnoreCase("Network Error")) {
            mApplication.ShowAlert(mContext, "Network Error",mContext.getResources().getString(R.string.app_name));
        } else if (ResponseParser.getResponseStatus(result)) {
            this.mOnReciveServerResponse.onReceiveResult(URL_Type, result);
        } else {
            this.mOnReciveServerResponse.onErrorResult(URL_Type, Str_RequestParams, result);
        }
    }


}
