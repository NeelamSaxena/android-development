package com.shopnearby;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ankit on 11/5/16.
 */
public class SNB_Application extends Application {

    public static SNB_Application mApplicationInstance = null;
    public float Screen_Height;
    public float Screen_Width;
    public String deviceId;
    private String device_lang = "";
    private static Activity mCurrentAcitivity = null;
    public boolean updatetextStatus=true;
    public String TempLat="";
    public String TempLong="";
    public String TempAddress="";
    public int TempsortPosition = -1;
    public int TempdistancePosition = -1;
    public String TempcurrentD = "";
    public String TempMinPrice = "";
    public String TempMaxPrice = "";
    public String TempCategory="";
    public String TempCategoryID="";
    public String TempBrand="";

    @Override
    public void onCreate() {
        MultiDex.install(getApplicationContext());
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        getScreenResolution(getApplicationContext());
        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        device_lang = Locale.getDefault().getDisplayLanguage();

    }

    private void getScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        Screen_Width = display.getWidth();
        Screen_Height = display.getHeight();
    }

    public static void setCurrentActivity(Activity mActivity) {
        mCurrentAcitivity = mActivity;
    }

    public static Activity getCurrentActivity() {

        return mCurrentAcitivity;
    }

    /**
     * Below method is used to instantiate application class
     */
    public static SNB_Application getApplicationInstance(Context mContext) {

        if (mApplicationInstance == null) {
            mApplicationInstance = (SNB_Application) mContext
                    .getApplicationContext();
        }

        return mApplicationInstance;

    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard(Context context, EditText view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ShowAlert(Context mContext, String mMessage,String mTitle) {

        AlertDialog.Builder mAlert = new AlertDialog.Builder(mContext);
        mAlert.setCancelable(false);
        mAlert.setTitle(mTitle);
        mAlert.setMessage(mMessage);

        mAlert.setPositiveButton(mContext.getResources().getString(R.string.Tag_Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        mAlert.show();
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public String ChangeDateFormat(String apiDate, String format) {
        String ConvertedDate = "";
        try {
            apiDate = apiDate.replace("T", " ");

            SimpleDateFormat sourceFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date parsed = sourceFormat.parse(apiDate);
            sourceFormat.setTimeZone(TimeZone.getDefault());

            ConvertedDate = sourceFormat.format(parsed);

            SimpleDateFormat destFormat = new SimpleDateFormat(format);
            ConvertedDate = destFormat.format(parsed);

        } catch (Exception e) {
            e.printStackTrace();
            return apiDate;
        }
        return ConvertedDate;
    }

}
