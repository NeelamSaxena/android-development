package com.shopnearby;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shopnearby.adapter.OnboarderAdapter;
import com.shopnearby.callbacks.SimpleGestureListener;
import com.shopnearby.custom.CircleIndicatorView;
import com.shopnearby.custom.OnboarderPage;
import com.shopnearby.custom.SimpleGestureFilter;

import java.util.List;

public class OnboarderActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, SimpleGestureListener {
    private CircleIndicatorView circleIndicatorView;
    private ViewPager vpOnboarderPager;
    private OnboarderAdapter onboarderAdapter;
    private Button BtnGetStarted;
    private TextView txt_Description;
    private ImageView ImgBack;
    private ImageView ImgNext;
    private SimpleGestureFilter detector;
    private int lastPagePosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarder);
        // Detect touched area
        detector = new SimpleGestureFilter(this, this);
        BtnGetStarted = (Button) findViewById(R.id.btn_getStarted);
        txt_Description = (TextView) findViewById(R.id.tv_description);
        ImgBack = (ImageView) findViewById(R.id.imgBack);
        ImgNext = (ImageView) findViewById(R.id.imgNext);
        circleIndicatorView = (CircleIndicatorView) findViewById(R.id.circle_indicator_view);
        vpOnboarderPager = (ViewPager) findViewById(R.id.vp_onboarder_pager);
        vpOnboarderPager.addOnPageChangeListener(this);
        ImgBack.setOnClickListener(this);
        ImgNext.setOnClickListener(this);
        BtnGetStarted.setOnClickListener(this);
    }


    public void setOnboardPagesReady(List<OnboarderPage> pages) {
        onboarderAdapter = new OnboarderAdapter(pages, getSupportFragmentManager());
        vpOnboarderPager.setAdapter(onboarderAdapter);
        circleIndicatorView.setPageIndicators(pages.size());
        circleIndicatorView.setActiveIndicatorColor(R.color.green_theme);
    }

    @Override
    public void onClick(View v) {
        boolean isInLastPage = vpOnboarderPager.getCurrentItem() == onboarderAdapter.getCount() - 1;
        switch (v.getId()) {
            case R.id.imgBack:
                if (vpOnboarderPager.getCurrentItem() > 0)
                    vpOnboarderPager.setCurrentItem(vpOnboarderPager.getCurrentItem() - 1);
                break;
            case R.id.imgNext:
                if (!isInLastPage)
                    vpOnboarderPager.setCurrentItem(vpOnboarderPager.getCurrentItem() + 1);
                else {
                    startActivity(new Intent(OnboarderActivity.this, LoginActivity.class));
                    finish();
                }
                break;

            case R.id.btn_getStarted:
                startActivity(new Intent(OnboarderActivity.this, LoginActivity.class));
                finish();
                break;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Ankit", "onPageSelected position : " + position);
        lastPagePosition = position;
        circleIndicatorView.setCurrentPage(position);
        BtnGetStarted.setVisibility(position > 0 ? View.GONE : View.VISIBLE);
        ImgBack.setVisibility(position > 0 ? View.VISIBLE : View.GONE);
        txt_Description.setVisibility(position > 0 ? View.VISIBLE : View.GONE);

        switch (position) {
            case 1:
                txt_Description.setText(R.string.Tag_text1);
                break;
            case 2:
                txt_Description.setText(R.string.Tag_text2);
                break;
            case 3:
                txt_Description.setText(R.string.Tag_text3);
                break;
            case 4:
                txt_Description.setText(R.string.Tag_text4);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("Ankit", "onPageScrollStateChanged: " + state);
    }

    protected void onSkipButtonPressed() {
        vpOnboarderPager.setCurrentItem(onboarderAdapter.getCount());
    }

    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                str = "Swipe Right";
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                str = "Swipe Left";
                if (lastPagePosition == 4) {
                    startActivity(new Intent(OnboarderActivity.this, LoginActivity.class));
                    finish();
                }
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                str = "Swipe Up";
                break;

        }

        Log.d("Ankit", "Direction : " + str);
//        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {
//        Log.d("Ankit", "Double Tap");
//        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

}
