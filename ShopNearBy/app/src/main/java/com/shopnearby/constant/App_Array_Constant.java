package com.shopnearby.constant;

import com.shopnearby.modal.BrandModal;
import com.shopnearby.modal.SearchCategoryModal;
import com.shopnearby.modal.StoreModal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neelam on 22/6/16.
 */

public class App_Array_Constant {

    public static List<BrandModal> Temp_BRANDNAME_LIST=new ArrayList<>();
    public static List<SearchCategoryModal> Temp_Category_LIST=new ArrayList<>();
    public static List<StoreModal> Temp_STORE_LIST=new ArrayList<>();
}
