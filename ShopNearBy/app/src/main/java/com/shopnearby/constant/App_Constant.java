package com.shopnearby.constant;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ankit on 11/5/16.
 */
public class App_Constant {

    public static String API_KEY = "AIzaSyDYy9TNF0BvH6tUNqNnLFR2RaR5vOw7wmo";
    public static String API_SECURE_KEY = "4zTywu0qyld2Fva1zx20b9bAsD2kLnCU";

    public static String sCurrentAddress = "";
//    public static double sCurrentLat = 34.0500;
//    public static double sCurrentLng = -118.2500;
    public static String sUser = "";
    public static double sCurrentLat = 0.0;
    public static double sCurrentLng = 0.0;
    public static LatLng sCurrentLocation;

    public static String currentCategory = "";
    public static String currentRadius = "20mi";
    public static String currentSortFilter = "";
    public static String currentBrandsFilter = "";
    public static String currentCateogryFilter = "";
    public static String currentCateogryIDFilter = "";
    public static String currentStoreFilter = "";
    public static String currentStoreIDFilter = "";
    public static String FilterRadius = "20 miles or less";
    public static String FilterSort = "Best Rated";
    public static String TotalResults = "";
    public static String min_price = "";
    public static String max_price = "";

    public static String currentItemHeader = "Items";


    // Constant For Search Filter

    public static String sCurrentSearchAddress = "";
    //    public static double sCurrentLat = 34.0500;
//    public static double sCurrentLng = -118.2500;
    public static double sCurrentSearchLat = 0.0;
    public static double sCurrentSearchLng = 0.0;
    public static LatLng sCurrentSearhLocation;

    public static String currentSearchCategory = "";
    public static String currentSearchRadius = "20mi";
    public static String currentSearchSortFilter = "";
    public static String currentSearchBrandsFilter = "";
    public static String currentSearchCateogryFilter = "";
    public static String currentSearchCateogryIDFilter = "";
    public static String currentSearchStoreFilter = "";
    public static String currentSearchStoreIDFilter = "";
    public static String FilterSearchRadius = "20 miles or less";
    public static String FilterSearchSort = "Best Match";
    public static String TotalSearchResults = "";
    public static String min_priceSearch = "";
    public static String max_priceSearch = "";








    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 22;

    public static String USER_DOMAIN = "http://dev.shopnearbyapp.com/api/user/";
    public static String PRODUCT_DOMAIN = "http://dev.shopnearbyapp.com/api/product/";

    public enum URL {
        //User api list
        USER_LOGIN_API(USER_DOMAIN + "integratedLogin"),
        CHECK_EMAIL_API(USER_DOMAIN + "checkEmail"),
        FORGOT_API(USER_DOMAIN + "forgot"),
        LOGIN_API(USER_DOMAIN + "login"),
        REGISTER_API(USER_DOMAIN + "register"),

        //Product Apis list
        PRODUCT_LIST_FrontAPI(PRODUCT_DOMAIN + "page"),
        PRODUCT_LIST_API(PRODUCT_DOMAIN + "list"),
        PRODUCT_DETAILS_API(PRODUCT_DOMAIN + "detail"),
        REVIEWS_API(PRODUCT_DOMAIN + "reviews"),
        AUTOCOMPLETE_API(PRODUCT_DOMAIN + "autocomplete"),
        asd(USER_DOMAIN + "forgot");

        private final String name;

        URL(String s) {
            name = s;
        }

        public String toString() {
            return name;
        }

    }
}
