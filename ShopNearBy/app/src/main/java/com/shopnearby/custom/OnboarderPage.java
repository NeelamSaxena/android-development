package com.shopnearby.custom;

import android.support.annotation.DrawableRes;

public class OnboarderPage {

    @DrawableRes public int imageResourceId;

    public OnboarderPage(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }


    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

}
